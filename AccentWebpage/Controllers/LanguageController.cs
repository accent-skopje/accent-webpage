﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace AccentWebpage.Controllers
{
    public class LanguageController : Controller
    {
        // GET: Language
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Change(String LanguageName)
        {
            if (LanguageName != null)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(LanguageName);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(LanguageName);
            }

            HttpCookie cookie = new HttpCookie("Language");
            cookie.Value = LanguageName;
            Response.Cookies.Add(cookie);

            return View("Index");
        }
    }
}