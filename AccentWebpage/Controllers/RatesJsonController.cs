﻿using AccentWebpage.Models;
using AccentWebpage.ViewModels;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace AccentWebpage.Controllers
{
    public class RatesJsonController : Controller
    {
        private ApplicationDbContext db;

        public RatesJsonController()
        {
            db = new ApplicationDbContext();
            ViewData["navigationPartialData"] = new AdminViewModel(db);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
        }

        [HttpGet]
        // GET: RatesJson
        public JsonResult Index()
        {
            var rates = db.Rates.Include(i => i.SourceLanguage).Include(i => i.DestinationLanguage).ToList();
            var ratePricelists = db.RatePricelists.Include(p => p.Pricelist).ToList();

            var viewModel = new RateViewModel()
            {
                Rates = rates,
                RatePricelists = ratePricelists
            };

            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }
    }
}