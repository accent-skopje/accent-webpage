﻿using AccentWebpage.Models;
using AccentWebpage.ViewModels;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System;
using PagedList;

namespace AccentWebpage.Controllers
{
    [Authorize]
    public class NewsController : Controller
    {
        private ApplicationDbContext db;
        private NewsViewModel viewModel;

        public NewsController()
        {
            db = new ApplicationDbContext();
            viewModel = new NewsViewModel();
            ViewData["navigationPartialData"] = new AdminViewModel(db);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
        }

        // GET: News
        public ActionResult Index(int? page)
        {
            var news = db.Newses.Where(n => n.InfoText == null).OrderByDescending(i => i.DateCreated).ToList();

            var pageSize = 5;
            var pageNumber = (page ?? 1);

            return View(news.ToPagedList(pageNumber, pageSize));
        }
        public ActionResult New()
        {
            return View("NewsForm", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(NewsViewModel newsView, HttpPostedFileBase file)
        {
            if (!ModelState.IsValid)
                return View("NewsForm", newsView);

            if (file != null)
            {
                newsView.UploadImage(file);
                newsView.News.ImgPath = "/Content/Images/News/" + file.FileName;
                newsView.News.ImgName = file.FileName;
            }

            //var sprakWebName = "SprakserviceWeb/SprakWeb";
            //var solutionDir = Directory.GetParent(
            //    Directory.GetCurrentDirectory()).Parent.FullName;

            //var sprakWeb = XDocument.Load(solutionDir + "/" + sprakWebName + "/Content/Images/News/") + file.FileName;

            db.Newses.Add(newsView.News);

            db.SaveChanges();
            //var errors = ModelState.Values.SelectMany(v => v.Errors);

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(NewsViewModel newsView, HttpPostedFileBase file)
        {
            var newsDb = db.Newses.Single(n => n.Id == newsView.Id);

            if (newsDb == null)
                return HttpNotFound();

            newsDb.Title = newsView.News.Title;
            newsDb.ShortDescription = newsView.News.ShortDescription;
            newsDb.Description = newsView.News.Description;
            newsDb.Published = newsView.News.Published;
            newsDb.InfoText = newsView.News.InfoText;


            if (file != null)
            {
                newsView.UploadImage(file);
                newsDb.ImgPath = "/Content/Images/News/" + file.FileName;
                newsDb.ImgName = file.FileName;
            }
            else
            {
                newsView.News.ImgPath = newsDb.ImgPath;
                newsView.News.ImgName = newsDb.ImgName;
            }

            db.SaveChanges();


            if (newsDb.InfoText != null)
            {
                return RedirectToAction("IndexInfo", "News", new { id = newsView.Id });
            }

            return RedirectToAction("Index", "News", new {id = newsView.Id});
        }

        public ActionResult Delete(bool confirm, int id)
        {
            var news = db.Newses.Find(id);

            if (news == null)
                return HttpNotFound();

            if (confirm)
            {
                db.Newses.Remove(news);
                db.SaveChanges();
            }

            return RedirectToAction("Index", "News");
        }

        public ActionResult Edit(int id)
        {
            var newsDb = db.Newses.SingleOrDefault(n => n.Id == id);

            if (newsDb == null)
                return HttpNotFound();

            viewModel.News = newsDb;
            viewModel.Id = id;

            return View("NewsForm", viewModel);
        }

  

        public ActionResult IndexInfo()
        {
            var news = db.Newses.Where(n => n.InfoText != null).OrderByDescending(i => i.DateCreated).ToList();
            var viewModel = new NewsViewModel()
            {
                Newses = news
            };

            return View(viewModel);
        }

        public ActionResult NewInfo()
        {
            return View("InfoForm", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateInfo(NewsViewModel infoView)
        {
            if (!ModelState.IsValid)
                return View("InfoForm", infoView);


            db.Newses.Add(infoView.News);

            db.SaveChanges();

            return RedirectToAction("IndexInfo");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateInfo(NewsViewModel infoView)
        {
            var newsDb = db.Newses.Single(n => n.Id == infoView.Id);

            if (newsDb == null)
                return HttpNotFound();

            newsDb.Published = infoView.News.Published;
            newsDb.InfoText = infoView.News.InfoText;

            db.SaveChanges();

            return RedirectToAction("IndexInfo", "News", new { id = infoView.Id });
        
        }

        public ActionResult DeleteInfo(int id)
        {
            var news = db.Newses.Find(id);

            if (news == null)
                return HttpNotFound();

            db.Newses.Remove(news);
            db.SaveChanges();

            return RedirectToAction("IndexInfo", "News");
        }

        public ActionResult EditInfo(int id)
        {
            var newsDb = db.Newses.SingleOrDefault(n => n.Id == id);

            if (newsDb == null)
                return HttpNotFound();

            viewModel.News = newsDb;
            viewModel.Id = id;

            return View("InfoForm", viewModel);
        }
    }
}