﻿using AccentWebpage.Models;
using AccentWebpage.ViewModels;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace AccentWebpage.Controllers
{
    [Authorize]
    public class QualityTypesController : Controller
    {
        private ApplicationDbContext db;
        private QualityViewModel viewModel;

        public QualityTypesController()
        {
            db = new ApplicationDbContext();
            viewModel = new QualityViewModel();
            ViewData["navigationPartialData"] = new AdminViewModel(db);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
        }

        // GET: Qualities
        public ActionResult Index()
        {
            var qualityTypes = db.QualityTypes.ToList();
            var qualityPricelists = db.QualityPricelists.Include(q => q.Pricelist).ToList();

            viewModel.QualityTypes = qualityTypes;
            viewModel.QualityPricelists = qualityPricelists;

            return View(viewModel);
        }

        public ActionResult New()
        {
            return View("QualityForm", viewModel);
        }

        public ActionResult Edit(int id)
        {
            var qualityTypeDb = db.QualityTypes.SingleOrDefault(r => r.Id == id);
            var qualityPricelistsDb = db.QualityPricelists.Include(r => r.Pricelist).Include(r => r.QualityType).Where(r => r.QualityTypeId == id);
            var pricelistsDb = new List<Pricelist>();

            if (qualityPricelistsDb != null)
            {
                foreach (var ratePricelist in qualityPricelistsDb)
                {
                    pricelistsDb.Add(ratePricelist.Pricelist);
                }
            }

            if (qualityTypeDb == null)
                return HttpNotFound();

            viewModel.QualityType = qualityTypeDb;
            viewModel.Pricelists = pricelistsDb;
            viewModel.Id = id;

            return View("QualityForm", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(QualityViewModel qualityViewModel)
        {
            //var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (!ModelState.IsValid)
                return View("QualityForm", qualityViewModel);

            var pricelist = qualityViewModel.Pricelist;
            pricelist.Name = "Words " + 1;
            db.Pricelists.Add(pricelist);

            var qualityType = qualityViewModel.QualityType;
            db.QualityTypes.Add(qualityType);

            var qualityPricelist = new QualityPricelist
            {
                QualityType = qualityViewModel.QualityType,
                Pricelist = qualityViewModel.Pricelist

            };
            db.QualityPricelists.Add(qualityPricelist);

            db.SaveChanges();

            return RedirectToAction("Edit", "QualityTypes", new { id = qualityViewModel.QualityType.Id });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(QualityViewModel qualityViewModel)
        {
            int i = 1;

            var qualityTypeDb =
                    db.QualityTypes
                        .Single(q => q.Id == qualityViewModel.Id);

            qualityTypeDb.Name = qualityViewModel.QualityType.Name;
            qualityTypeDb.Description = qualityViewModel.QualityType.Description;

            i = db.QualityPricelists.Where(r => r.QualityTypeId == qualityTypeDb.Id).ToList().Count + 1;
            qualityViewModel.Pricelist.Name = "Words " + i;

            if (qualityViewModel.Pricelist.From != 0 && qualityViewModel.Pricelist.To != 0 && qualityViewModel.Pricelist.Cost != 0)
            {
                db.Pricelists.Add(qualityViewModel.Pricelist);

                QualityPricelist qualityPricelist = new QualityPricelist
                {
                    QualityType = qualityTypeDb,
                    Pricelist = qualityViewModel.Pricelist
                };

                db.QualityPricelists.Add(qualityPricelist);
            }

            db.SaveChanges();

            return RedirectToAction("Edit", "QualityTypes", new { id = qualityViewModel.Id });
        }

        public ActionResult Delete(bool confirm, int id)
        {
            var qualityType = db.QualityTypes.Find(id);

            if (qualityType == null)
                return HttpNotFound();

            var qualityPricelist = db.QualityPricelists.Include(r => r.QualityType).Where(r => r.QualityTypeId == id);

            if (confirm)
            {
                db.QualityTypes.Remove(qualityType);

                foreach (var item in qualityPricelist)
                {
                    var pricelist = db.Pricelists.Find(item.PricelistId);

                    if (pricelist != null)
                        db.Pricelists.Remove(pricelist);
                }

                db.SaveChanges();
            }

            return RedirectToAction("Index", "QualityTypes");
        }
    }
}