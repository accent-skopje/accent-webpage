﻿using AccentWebpage.Models;
using AccentWebpage.ViewModels;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace AccentWebpage.Controllers
{
    [Authorize]
    public class DeliveryDeadlineController : Controller
    {
        private ApplicationDbContext db;
        private DeliveryDedlineViewModel viewModel;

        public DeliveryDeadlineController()
        {
            db = new ApplicationDbContext();
            viewModel = new DeliveryDedlineViewModel();
            ViewData["navigationPartialData"] = new AdminViewModel(db);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
        }


        // GET: DeliveryDeadline
        public ActionResult Index()
        {
            var delivery = db.DeliveryDeadlineTypes.ToList();
            var deliveryPricelist = db.DeliveryDeadlinePricelists.Include(p => p.Pricelist).ToList();

            viewModel.DeliveryDeadlineTypes = delivery;
            viewModel.DeliveryDeadlinePricelists = deliveryPricelist;

            return View(viewModel);
        }

        public ActionResult New()
        {
            return View("DeliveryForm", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DeliveryDedlineViewModel deliveryDedlineView)
        {
            if (!ModelState.IsValid)
                return View("DeliveryForm", deliveryDedlineView);

            var pricelist = deliveryDedlineView.Pricelist;
            pricelist.Name = "Words " + 1;
            if (deliveryDedlineView.Pricelist.Rate == null)
                pricelist.Rate = 0;
            if (deliveryDedlineView.Pricelist.Days == null)
                pricelist.Days = 0;
            db.Pricelists.Add(pricelist);

            var deliveryDeadline = deliveryDedlineView.DeliveryDeadlineType;
            db.DeliveryDeadlineTypes.Add(deliveryDeadline);

            var deliveryPricelist = new DeliveryDeadlinePricelist
            {
                DeliveryDeadlineType = deliveryDedlineView.DeliveryDeadlineType,
                Pricelist = deliveryDedlineView.Pricelist
            };

            db.DeliveryDeadlinePricelists.Add(deliveryPricelist);

            db.SaveChanges();

            return RedirectToAction("Edit", "DeliveryDeadline", new { id = deliveryDedlineView.DeliveryDeadlineType.Id });
        }

        public ActionResult Edit(int id)
        {
            var deliveryTypeDb = db.DeliveryDeadlineTypes.SingleOrDefault(r => r.Id == id);
            var deliveryPricelistdb =
                db.DeliveryDeadlinePricelists.Include(r => r.Pricelist)
                    .Include(r => r.DeliveryDeadlineType).Where(r => r.DeliveryDeadlineTypeId == id);
            var pricelistsDb = new List<Pricelist>();

            if (deliveryPricelistdb != null)
            {
                foreach (var deliveryDeadlinePricelist in deliveryPricelistdb)
                {
                    pricelistsDb.Add(deliveryDeadlinePricelist.Pricelist);
                }
            }

            if (deliveryTypeDb == null)
                return HttpNotFound();

            viewModel.DeliveryDeadlineType = deliveryTypeDb;
            viewModel.Pricelists = pricelistsDb;
            viewModel.Id = id;

            return View("DeliveryForm", viewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(DeliveryDedlineViewModel deliveryDedlineView)
        {
            int i = 1;

            var deliveryTypeDb =
                    db.DeliveryDeadlineTypes
                        .Single(q => q.Id == deliveryDedlineView.Id);

            deliveryTypeDb.Name = deliveryDedlineView.DeliveryDeadlineType.Name;

            i = db.DeliveryDeadlinePricelists.Where(r => r.DeliveryDeadlineTypeId == deliveryTypeDb.Id).ToList().Count + 1;
            deliveryDedlineView.Pricelist.Name = "Words " + i;
            if (deliveryDedlineView.Pricelist.Rate == null)
                deliveryDedlineView.Pricelist.Rate = 0;
            if (deliveryDedlineView.Pricelist.Days == null)
                deliveryDedlineView.Pricelist.Days = 0;

            if (deliveryDedlineView.Pricelist != null)
            {
                db.Pricelists.Add(deliveryDedlineView.Pricelist);

                DeliveryDeadlinePricelist deadlinePricelist = new DeliveryDeadlinePricelist
                {
                    DeliveryDeadlineType = deliveryTypeDb,
                    Pricelist = deliveryDedlineView.Pricelist
                };

                db.DeliveryDeadlinePricelists.Add(deadlinePricelist);
            }

            db.SaveChanges();

            return RedirectToAction("Edit", "DeliveryDeadline", new { id = deliveryDedlineView.Id });
        }

        public ActionResult Delete(bool confirm, int id)
        {
            var deliveryType = db.DeliveryDeadlineTypes.Find(id);

            if (deliveryType == null)
                return HttpNotFound();

            var deliveryPricelist =
                db.DeliveryDeadlinePricelists.Include(r => r.DeliveryDeadlineType)
                    .Where(r => r.DeliveryDeadlineTypeId == id);

            if (confirm)
            {
                db.DeliveryDeadlineTypes.Remove(deliveryType);

                foreach (var item in deliveryPricelist)
                {
                    var pricelist = db.Pricelists.Find(item.PricelistId);

                    if (pricelist != null)
                        db.Pricelists.Remove(pricelist);
                }

                db.SaveChanges();
            }

            return RedirectToAction("Index", "DeliveryDeadline");
        }
    }
}