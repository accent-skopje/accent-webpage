﻿using AccentWebpage.Models;
using AccentWebpage.ViewModels;
using System.Linq;
using System.Web.Mvc;

namespace AccentWebpage.Controllers
{
    public class SiteController : Controller
    {
        private ApplicationDbContext db;
        private NewsViewModel viewModel;

        public SiteController()
        {
            db = new ApplicationDbContext();
            viewModel = new NewsViewModel();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
        }

        public ActionResult Index()
        {
            var news = db.Newses.OrderByDescending(i => i.DateCreated).ToList();
            viewModel.Newses = news;

            return View(viewModel);
        }

        public ActionResult Details(int id)
        {
            var news = db.Newses.SingleOrDefault(n => n.Id == id);
            viewModel.News = news;

            //if (news == null)
            //{
            //    return HttpNotFound();
            //}

            return PartialView("Details", viewModel);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}