﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using HSense.IQ.BsinessLogic;
//using iTextSharp.text;
//using iTextSharp.text.pdf;
using AccentWebpage.Models;
using AccentWebpage.ViewModels;
using DocumentItem = AccentWebpage.ViewModels.DocumentItem;
using Font = System.Drawing.Font;
using Language = AccentWebpage.Models.Language;
using Order = AccentWebpage.Models.Order;
using Rate = AccentWebpage.Models.Rate;

namespace AccentWebpage.Controllers
{
    public class QuoteJsonController : Controller
    {
        private ApplicationDbContext db;

        public QuoteJsonController()
        {
            db = new ApplicationDbContext();
            ViewData["navigationPartialData"] = new AdminViewModel(db);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
        }

        // GET: QuoteJson
        [HttpPost]
        public JsonResult Index(List<DocumentItem> DocumentItems, int SourceLangId, int[] DestinationIds, string DeliveryPlanName, string DeliveryDeadlineName)
        {
            var order = new OrderViewModel();
            order.DocumentItems = DocumentItems;
            int wordCount = 0;

            for (int i = 0; i < DocumentItems.Count; i++)
            {
                wordCount += DocumentItems[i].WordCount;
            }

            var qualityType = db.QualityTypes.SingleOrDefault(q => q.Name == DeliveryPlanName);
            var qualityPricelist = db.QualityPricelists.Include(p => p.Pricelist).Where(q => q.QualityTypeId == qualityType.Id && q.Pricelist.From <= wordCount && q.Pricelist.To >= wordCount).ToList();
            
            order.DeliveryPlan = qualityPricelist.Count > 0 ? qualityPricelist.FirstOrDefault().Pricelist : null;
            //order.CostPerWord = qualityPricelist.Count > 0 ? Convert.ToDecimal(qualityPricelist.FirstOrDefault().Pricelist.Cost) : 0;
            order.WordCount = wordCount;

            List<Rate> listRate = new List<Rate>();
            List<RatePricelist> listRatePricelists = new List<RatePricelist>();
            List<Pricelist> listPricelists = new List<Pricelist>();
            List<Language> destinationLanguage = new List<Language>();

            for (int i = 0; i < DestinationIds.Count(); i++)
            {
                int destinationId = DestinationIds[i];
                listRate.AddRange(db.Rates.Where(r => r.SourceLanguage == SourceLangId && r.DestinationLanguage == destinationId).ToList());
                destinationLanguage.AddRange(db.Languages.Where(l => l.Id == destinationId));
            }

            for (int i = 0; i < listRate.Count; i++)
            {
                int rateId = listRate[i].Id;
                listRatePricelists.AddRange(db.RatePricelists.Where(r => r.RateId == rateId).ToList());
            }

            for (int i = 0; i < listRatePricelists.Count; i++)
            {
                int pricelistId = listRatePricelists[i].PriceListId;
                listPricelists.AddRange(db.Pricelists.Where(p => p.Id == pricelistId && p.From <= wordCount && p.To >= wordCount).ToList());
            }

            var deliveryDeadlineCost = db.DeliveryDeadlineTypes.SingleOrDefault(d => d.Name == DeliveryDeadlineName);
            var deliveryDedalinePricelist =
                db.DeliveryDeadlinePricelists.Include(p => p.Pricelist).Where(d => d.DeliveryDeadlineTypeId == deliveryDeadlineCost.Id && d.Pricelist.From <= wordCount && d.Pricelist.To >= wordCount);
            var pricelist = deliveryDedalinePricelist.FirstOrDefault().Pricelist;

            decimal total = 0;

            foreach (var rate in listPricelists)
            {
                var costPerWord = rate.Cost;
                decimal temp = Convert.ToDecimal(wordCount * (costPerWord + order.DeliveryPlan.Cost));
                var hours = GetHoursFromWords(wordCount);
                var qualityControlTotal = Convert.ToDecimal(hours * order.DeliveryPlan.Rate);
                temp += qualityControlTotal;
                var deliveryPercent = DeliveryPercent(temp, pricelist);
                temp += deliveryPercent;

                if (temp < 690)
                {
                    temp = 690;
                }

                order.CostPerWord += Convert.ToDecimal(rate.Cost);
                total += temp;
            }

            order.Rates = listRate;
            order.RatePricelists = listPricelists;
            order.TotalCost = total;
            order.SourceLanguage = db.Languages.SingleOrDefault(l => l.Id == SourceLangId);
            order.TotalCostWithVat = total + (total * Convert.ToDecimal(0.25));
            order.DestinationLanguages = destinationLanguage;
            order.DeliveryDeadline = pricelist;

            return Json(order, JsonRequestBehavior.AllowGet);
        }

        private int GetHoursFromWords(int wordCount)
        {
            var result = 0;
            var div = wordCount / 2000;
            result += div;
            var mod = wordCount % 2000;
            if (mod > 0)
            {
                result++;
            }
            return result;
        }

        private decimal DeliveryPercent(decimal total, Pricelist pricelistDeliveryDeadline)
        {
            return total * (Convert.ToDecimal(pricelistDeliveryDeadline.Rate) / 100u);
        }

        [HttpPost]
        public JsonResult MakeOrder()
        {
            string qs = System.Web.HttpContext.Current.Server.UrlDecode(
                            System.Web.HttpContext.Current.Request.Form["confirmOrder"]
                        );

            var obj = new JavaScriptSerializer().Deserialize<ConfirmOrderViewModel>(qs);

            var files = new List<AttachedFile>();
            for (var i = 0; i < System.Web.HttpContext.Current.Request.Files.Count; i++)
            {
                HttpPostedFile file = System.Web.HttpContext.Current.Request.Files[i];
                if (file.FileName != "undefined" && file.FileName != "blob")
                {
                    files.Add(new AttachedFile
                    {
                        FileName = file.FileName,
                        InputStream = file.InputStream
                    });
                }
            }

            foreach (var documentItem in obj.Order.DocumentItems)
            {
                if (documentItem.Filetype == "Typed text")
                {
                    Stream textStream = GenerateStreamFromString(documentItem.Contents);

                    files.Add(new AttachedFile
                    {
                        FileName = documentItem.Name + ".txt",
                        InputStream = textStream
                    });
                }
            }

            try
            {
                var receiverId = obj.RecepientProfile.Save(db);
                // Fill in the temlate with the additional data (check if there is new version of it)
                var emailContent = EmailSending.GetEmailContent(obj.Order, obj.RecepientProfile, obj.Language);
                int newOrderId = OrderViewModel.CreateNew(receiverId, db, emailContent);
                obj.Order.Id = newOrderId;

                EmailSending es = new EmailSending();

                var emailRecepients = db.EmailRecepients;
                List<EmailRecepient> listEmailRecepients = new List<EmailRecepient>();

                foreach (var recepient in emailRecepients)
                {
                    EmailRecepient emailRecepient = new EmailRecepient();

                    emailRecepient.Id = recepient.Id;
                    emailRecepient.FirstName = recepient.FirstName;
                    emailRecepient.LastName = recepient.LastName;
                    emailRecepient.Email = recepient.Email;

                    listEmailRecepients.Add(emailRecepient);
                }

                if (obj.RecepientProfile.CustomerType.IsNewCustomer)
                {
                    //send obj.RecepientProfile.NewCustomer to Admin
                    es.SendNewCustomerEmail(obj.RecepientProfile, obj.Language);
                }

                try
                {
                    es.SendEmail(obj.Order, obj.RecepientProfile, files, obj.Language, listEmailRecepients);
                    MakeResponse(true);
                }
                catch (Exception ex)
                {
                    MakeResponse(ex);
                }
                finally
                {
                    foreach (var attachedFile in files)
                    {
                        attachedFile.InputStream.Dispose();
                    }
                }
            }
            catch
            {
                MakeResponse(false);
            }

            return Json(files, JsonRequestBehavior.AllowGet);
        }

        private static void MakeResponse(object data)
        {
            string json = new JavaScriptSerializer().Serialize(data);

            System.Web.HttpContext.Current.Response.ContentType = "application/json";
            System.Web.HttpContext.Current.Response.Write(json);
        }

        private static MemoryStream GenerateStreamFromString(string value)
        {
            return new MemoryStream(Encoding.UTF8.GetBytes(value ?? ""));
        }

        public class ConfirmOrderViewModel
        {
            public OrderViewModel Order { get; set; }
            public RecepientProfile RecepientProfile { get; set; }
            public string Language { get; set; }
        }

        [HttpPost]
        public ActionResult Print(List<DocumentItem> DocumentItems, int SourceLangId, int[] DestinationIds, string DeliveryPlanName, string DeliveryDeadlineName)
        {
            List<string> errors = new List<string>();

            try
            {
                //BaseFont bfTimes = BaseFont.CreateFont("C:\\Windows\\Fonts\\Arial.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                ////Font times = new Font(bfTimes, 11, FontStyle.Bold);
                ////Font times1 = new Font(bfTimes, 11, Font.FromHdc());
                ////Font datum = new Font(bfTimes, 10);
                ////Font title = new Font(bfTimes, 14, Font.NORMAL, new BaseColor(255, 255, 255));
                ////Font title1 = new Font(bfTimes, 10, Font.NORMAL);
                ////Font title2 = new Font(bfTimes, 10, Font.BOLD);
                ////Font times_big = new Font(bfTimes, 16);

                //List pom = new List();
                //string arende = "";

                //for (int i = 0; i < DocumentItems.Count; i++)
                //{
                //    arende += DocumentItems[i].Name + ((DocumentItems.Count - i != 1) ? ", " : "");
                //}

                ////if (podIzv == 0)
                ////{
                //string outputFileName = Server.MapPath("../PDFs") + @"\Order.pdf";
                //var doc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4);

                //decimal vk = 0;
                //decimal vkOdPrethodniSituacii = 0;
                //decimal vkPoOvaaSituacija = 0;
                //decimal ddv = 0;
                //decimal total = 0;

                ////create a writer that listens to the document
                //FileStream file = new FileStream(outputFileName, FileMode.Create);
                //PdfWriter writer = PdfWriter.GetInstance(doc, file);

                ////open document
                //if (!doc.IsOpen())
                //    doc.Open();

                //iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(Server.MapPath("../") + @"\Images\sprakservice_logo_full_small.jpg");
                //logo.ScaleToFit(300f, 60f);
                //logo.Alignment = Element.ALIGN_LEFT;
                //logo.SpacingAfter = 20f;

                //doc.Add(logo);
                //Paragraph Title = new Paragraph("Offert");
                //Paragraph Datum = new Paragraph(DateTime.Now.ToShortDateString());
                //Paragraph Arende = new Paragraph("Ärende: " + arende);

                //Title.Alignment = Element.ALIGN_CENTER;
                //Datum.Alignment = Element.ALIGN_LEFT;
                //Arende.Alignment = Element.ALIGN_RIGHT;

                //Title.SpacingAfter = 20f;
                ////p2.SpacingAfter = 10f;

                //doc.Add(Title);
                //doc.Add(Datum);
                //doc.Add(Arende);
                //doc.Add(Chunk.NEWLINE);

                //foreach (var q in raboti)
                //{
                //    var pozicii2 =// db.Situacija.Include(p => p.Pozicija).Include(p => p.Pozicija.Norma).Where(p => p.Pozicija.IdentifikaciskiBroj == q.IdRaboti).Select(n => new {cena=n.Cena,edinicaMera=n.EdinicaMera,Naziv=n.Pozicija.Norma.OpisNorma, Datum = n.Datum, IdProekt = n.IdProekt, IdSituacija = n.IdSituacija, Broj = n.Broj, IdPozicija = n.IdPozicija, NormaNaziv = n.Pozicija.Norma.OpisNorma, EdinicaMera = n.EdinicaMera, VkupnaKolicina = n.VkupnaKolicina, OpisFormula = n.OpisFormula, DopolnitelnaKolicina = n.Pozicija.DopolnitelnaKolicina, VkupnoPredhodniKolicini = n.PrethodnaKolicina, Kolicina = n.Kolicina, Cena = n.Cena, KolicinaOdbivanje = n.KolicinaOdbivanje, CenaOdbivanje = n.CenaOdbivanje, OpisOdbivanje = n.OpisOdbivanje, Podizveduvac = n.Podizveduvac }).Where(c => c.IdProekt == id && c.Broj == br).ToList();
                //         (from k in db.Situacija.Include(p => p.Pozicija).Include(p => p.Pozicija.Norma).Where(p => p.RabotaId == q.IdRaboti).Where(c => c.IdProekt == id).Where(c => c.Broj == br)
                //          select new { IdPozicija = k.IdPozicija, Naziv = k.Pozicija.Norma.OpisNorma, edinicaMera = k.EdinicaMera, Kolicina = k.Kolicina, cena = k.Cena, prKolicina = k.PrethodnaKolicina }).ToList();

                //    if (podIzv != 0)
                //    {
                //        pozicii2 =// db.Situacija.Include(p => p.Pozicija).Include(p => p.Pozicija.Norma).Where(p => p.Pozicija.IdentifikaciskiBroj == q.IdRaboti).Select(n => new {cena=n.Cena,edinicaMera=n.EdinicaMera, Naziv = n.Pozicija.Norma.OpisNorma, Datum = n.Datum, IdProekt = n.IdProekt, IdSituacija = n.IdSituacija, Broj = n.Broj, IdPozicija = n.IdPozicija, NormaNaziv = n.Pozicija.Norma.OpisNorma, EdinicaMera = n.EdinicaMera, VkupnaKolicina = n.VkupnaKolicina, OpisFormula = n.OpisFormula, DopolnitelnaKolicina = n.Pozicija.DopolnitelnaKolicina, VkupnoPredhodniKolicini = n.PrethodnaKolicina, Kolicina = n.Kolicina, Cena = n.Cena, KolicinaOdbivanje = n.KolicinaOdbivanje, CenaOdbivanje = n.CenaOdbivanje, OpisOdbivanje = n.OpisOdbivanje, Podizveduvac = n.Podizveduvac }).Where(c => c.IdProekt == id && c.Broj == br && c.Podizveduvac.IdPodizveduvac == podIzv).ToList();
                //                     pozicii2 = (from k in db.Situacija.Include(p => p.Pozicija).Include(p => p.Pozicija.Norma).Where(p => p.RabotaId == q.IdRaboti).Where(c => c.IdProekt == id).Where(c => c.Broj == br)
                //                                 join p in db.PodizveduvacPozicija.Where(z => z.IdPodizveduvac == podIzv) on k.IdPozicija equals p.IdPozicija
                //                                 select new { IdPozicija = k.IdPozicija, Naziv = k.Pozicija.Norma.OpisNorma, edinicaMera = k.EdinicaMera, Kolicina = k.Kolicina, cena = p.Cena, prKolicina = k.PrethodnaKolicina }).ToList();
                //    }
                //    //Paragraph p5 = new Paragraph("Вкупно до сега: " + sevkupno, times);

                //    if (pozicii2.Count > 0)
                //    {
                //        Paragraph p13 = new Paragraph(q.NazivRabota, title1);
                //        p13.SpacingAfter = 10f;
                //        doc.Add(p13);

                //        PdfPTable table = new PdfPTable(6);
                //        table.TotalWidth = 519f;
                //        table.LockedWidth = true;
                //        float[] widths = new float[] { 1f, 5f, 1f, 2f, 2f, 3f };
                //        table.SetWidths(widths);

                //        PdfPCell cell1 = new PdfPCell(new Phrase("Рбр", title));
                //        cell1.BackgroundColor = new BaseColor(120, 120, 120);
                //        cell1.PaddingBottom = 5f;
                //        cell1.HorizontalAlignment = 1;
                //        table.AddCell(cell1);

                //        PdfPCell cell2 = new PdfPCell(new Phrase("Назив", title));
                //        cell2.BackgroundColor = new BaseColor(120, 120, 120);
                //        cell2.PaddingBottom = 5f;
                //        cell2.HorizontalAlignment = 1;
                //        table.AddCell(cell2);

                //        PdfPCell cell3 = new PdfPCell(new Phrase("Едм", title));
                //        cell3.BackgroundColor = new BaseColor(120, 120, 120);
                //        cell3.PaddingBottom = 5f;
                //        cell3.HorizontalAlignment = 1;
                //        table.AddCell(cell3);

                //        PdfPCell cell4 = new PdfPCell(new Phrase("Количина", title));
                //        cell4.BackgroundColor = new BaseColor(120, 120, 120);
                //        cell4.PaddingBottom = 5f;
                //        cell4.HorizontalAlignment = 1;
                //        table.AddCell(cell4);

                //        PdfPCell cell6 = new PdfPCell(new Phrase("Цена", title));
                //        cell6.BackgroundColor = new BaseColor(120, 120, 120);
                //        cell6.PaddingBottom = 5f;
                //        cell6.HorizontalAlignment = 1;
                //        table.AddCell(cell6);

                //        PdfPCell cell116 = new PdfPCell(new Phrase("Вкупно", title));
                //        cell116.BackgroundColor = new BaseColor(120, 120, 120);
                //        cell116.PaddingBottom = 5f;
                //        cell116.HorizontalAlignment = 1;
                //        table.AddCell(cell116);
                //        int i = 1;
                //        decimal vkPoRabota = 0;
                //        string naziv = "";
                //        string edm = "";

                //        var pozicija = from w in db.Situacija.Where(p => p.IdProekt == id && p.RabotaId == q.IdRaboti && p.Pozicija.Norma.SifraNorma == "X0DP" && p.Broj == br)
                //                       select w;

                //        if (podIzv != 0)
                //            pozicija = from w in db.Situacija.Where(p => p.IdProekt == id && p.RabotaId == q.IdRaboti && p.Pozicija.Norma.SifraNorma == "X0DP" && p.Broj == br && p.IdPodizveduvac == podIzv)
                //                       select w;

                //        foreach (var y in pozicija)
                //        {
                //            decimal kol = 0;
                //            decimal cena = 0;

                //            foreach (var poz in pozicii2.Where(p => p.IdPozicija == y.IdPozicija))
                //            {
                //                kol += Convert.ToDecimal(poz.prKolicina);
                //                naziv = poz.Naziv;
                //                edm = poz.edinicaMera;

                //                if (y.IdPozicija == poz.IdPozicija)
                //                {
                //                    cena = Convert.ToDecimal(poz.cena);
                //                }
                //                else
                //                {
                //                    PdfPCell cell23 = new PdfPCell(new Phrase(i.ToString(), times));
                //                    cell23.PaddingBottom = 2f;
                //                    cell23.PaddingLeft = 3f;
                //                    cell23.HorizontalAlignment = 1;
                //                    cell23.VerticalAlignment = Element.ALIGN_MIDDLE;
                //                    table.AddCell(cell23);

                //                    PdfPCell cell24 = new PdfPCell(new Phrase((poz.Naziv != null ? poz.Naziv.ToString() : ""), times));
                //                    cell24.PaddingBottom = 5f;
                //                    cell24.HorizontalAlignment = 3;
                //                    table.AddCell(cell24);

                //                    PdfPCell cell25 = new PdfPCell(new Phrase((poz.edinicaMera != null ? poz.edinicaMera.ToString() : ""), times));
                //                    cell25.PaddingBottom = 2f;
                //                    cell25.PaddingLeft = 3f;
                //                    cell25.HorizontalAlignment = 1;
                //                    cell25.VerticalAlignment = Element.ALIGN_MIDDLE;
                //                    table.AddCell(cell25);

                //                    PdfPCell cell26 = new PdfPCell(new Phrase((poz.prKolicina != 0 ? Math.Round(Convert.ToDecimal(poz.prKolicina), 3).ToString() : "0"), times));
                //                    cell26.PaddingBottom = 2f;
                //                    cell26.HorizontalAlignment = 2;
                //                    cell26.VerticalAlignment = Element.ALIGN_MIDDLE;
                //                    table.AddCell(cell26);

                //                    PdfPCell cell27 = new PdfPCell(new Phrase((poz.cena != null ? Math.Round(Convert.ToDecimal(poz.cena), 2).ToString() : "0"), times));
                //                    cell27.PaddingBottom = 2f;
                //                    cell27.PaddingLeft = 3f;
                //                    cell27.HorizontalAlignment = 2;
                //                    cell27.VerticalAlignment = Element.ALIGN_MIDDLE;
                //                    table.AddCell(cell27);

                //                    PdfPCell cell227 = new PdfPCell(new Phrase((poz.cena != null && poz.prKolicina != null ? Math.Round(Convert.ToDecimal(poz.cena * poz.prKolicina), 2).ToString() : "0"), times));
                //                    cell227.PaddingBottom = 2f;
                //                    cell227.PaddingLeft = 3f;
                //                    cell227.HorizontalAlignment = 2;
                //                    cell227.VerticalAlignment = Element.ALIGN_MIDDLE;
                //                    table.AddCell(cell227);
                //                    kol -= Convert.ToDecimal(poz.prKolicina);
                //                }
                //            }

                //            PdfPCell cell13 = new PdfPCell(new Phrase(i.ToString(), times));
                //            cell13.PaddingBottom = 2f;
                //            cell13.PaddingLeft = 3f;
                //            cell13.HorizontalAlignment = 1;
                //            cell13.VerticalAlignment = Element.ALIGN_MIDDLE;
                //            table.AddCell(cell13);

                //            PdfPCell cell14 = new PdfPCell(new Phrase(naziv.ToString(), times));
                //            cell14.PaddingBottom = 5f;
                //            cell14.HorizontalAlignment = 3;
                //            table.AddCell(cell14);

                //            PdfPCell cell15 = new PdfPCell(new Phrase(edm.ToString(), times));
                //            cell15.PaddingBottom = 2f;
                //            cell15.PaddingLeft = 3f;
                //            cell15.HorizontalAlignment = 1;
                //            cell15.VerticalAlignment = Element.ALIGN_MIDDLE;
                //            table.AddCell(cell15);

                //            PdfPCell cell16 = new PdfPCell(new Phrase((kol != 0 ? Math.Round(Convert.ToDecimal(kol), 3).ToString("N") : "0"), times));
                //            cell16.PaddingBottom = 2f;
                //            cell16.HorizontalAlignment = 2;
                //            cell16.VerticalAlignment = Element.ALIGN_MIDDLE;
                //            table.AddCell(cell16);

                //            PdfPCell cell17 = new PdfPCell(new Phrase((cena != 0 ? Math.Round(Convert.ToDecimal(cena), 2).ToString("N") : "0"), times));
                //            cell17.PaddingBottom = 2f;
                //            cell17.PaddingLeft = 3f;
                //            cell17.HorizontalAlignment = 2;
                //            cell17.VerticalAlignment = Element.ALIGN_MIDDLE;
                //            table.AddCell(cell17);

                //            PdfPCell cell117 = new PdfPCell(new Phrase((cena != 0 && kol != 0 ? Math.Round(Convert.ToDecimal(cena * kol), 2).ToString("N") : "0"), times));
                //            cell117.PaddingBottom = 2f;
                //            cell117.PaddingLeft = 3f;
                //            cell117.HorizontalAlignment = 2;
                //            cell117.VerticalAlignment = Element.ALIGN_MIDDLE;
                //            table.AddCell(cell117);
                //            vkPoRabota += Convert.ToDecimal(kol * cena);
                //            i++;
                //        }

                //        vk += vkPoRabota;

                //        doc.Add(table);

                //        Paragraph p16 = new Paragraph("Вкупно по " + q.NazivRabota + ": " + Math.Round(vkPoRabota, 2).ToString("N"), times);
                //        p16.Alignment = Element.ALIGN_RIGHT;
                //        doc.Add(p16);
                //    }
                //}

                //doc.Add(Chunk.NEWLINE);

                //ddv = vkPoOvaaSituacija * 18 / 100;
                //total = vkPoOvaaSituacija + ddv;

                //Paragraph p17 = new Paragraph("РЕКАПИТУЛАР:                                                                 Вкупно: " + Math.Round(vk, 2).ToString("N"), times1);
                //p17.Alignment = Element.ALIGN_RIGHT;

                //Paragraph p4 = new Paragraph("Вкупно по претходни ситуации : " + Math.Round(vkOdPrethodniSituacii, 2).ToString("N"), times1);
                //p4.Alignment = Element.ALIGN_RIGHT;
                ////p4.SpacingBefore = 8f;

                //Paragraph p5 = new Paragraph("Вкупно по оваа ситуација: " + Math.Round(vkPoOvaaSituacija, 2).ToString("N"), times1);
                //p5.Alignment = Element.ALIGN_RIGHT;
                ////p5.SpacingBefore = 8f;

                //Paragraph p14 = new Paragraph("ДДВ по оваа ситуација: " + Math.Round(ddv, 2).ToString("N"), times1);
                //p14.Alignment = Element.ALIGN_RIGHT;

                //Paragraph p15 = new Paragraph("Вкупно со ДДВ: " + Math.Round(total, 2).ToString("N"), times1);
                //p15.Alignment = Element.ALIGN_RIGHT;

                //doc.Add(p17);
                //doc.Add(p4);
                //doc.Add(p5);
                //doc.Add(p14);
                //doc.Add(p15);

                //doc.Add(Chunk.NEWLINE);

                //PdfPTable table2 = new PdfPTable(2);
                //table2.WidthPercentage = 100;
                //float[] widths1 = new float[] { 8f, 8f };
                //table2.SetWidths(widths1);

                //PdfPCell cell19 = new PdfPCell(new Phrase("Надзорен орган: ", title1));
                ////cell15.BackgroundColor = new BaseColor(220, 220, 220);
                //cell19.PaddingBottom = 5f;
                //cell19.HorizontalAlignment = 1;
                //cell19.BorderColor = new BaseColor(255, 255, 255);
                //table2.AddCell(cell19);

                //PdfPCell cell18 = new PdfPCell(new Phrase("Инвеститор: ", title1));
                ////cell15.BackgroundColor = new BaseColor(220, 220, 220);
                //cell18.PaddingBottom = 5f;
                //cell18.HorizontalAlignment = 1;
                //cell18.BorderColor = new BaseColor(255, 255, 255);
                //table2.AddCell(cell18);

                //PdfPCell cell21 = new PdfPCell(new Phrase("______________________________", title1));
                ////cell16.BackgroundColor = new BaseColor(220, 220, 220);
                //cell21.PaddingBottom = 15f;
                //cell21.HorizontalAlignment = 1;
                //cell21.BorderColor = new BaseColor(255, 255, 255);
                //table2.AddCell(cell21);

                //PdfPCell cell22 = new PdfPCell(new Phrase("______________________________", title1));
                ////cell16.BackgroundColor = new BaseColor(220, 220, 220);
                //cell22.PaddingBottom = 15f;
                //cell22.HorizontalAlignment = 1;
                //cell22.BorderColor = new BaseColor(255, 255, 255);
                //table2.AddCell(cell22);

                //doc.Add(table2);

                //doc.Close();
                //writer.Close();

                return Json("succes", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                errors.Add("The message form the server is: " + e.Message + "; With inner exception: " + e.InnerException.Message);

                return Json(errors, JsonRequestBehavior.AllowGet);
            }
        }
    }
}