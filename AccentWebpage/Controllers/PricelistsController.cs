﻿using AccentWebpage.Models;
using AccentWebpage.ViewModels;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace AccentWebpage.Controllers
{
    [Authorize]
    public class PricelistsController : Controller
    {
        private ApplicationDbContext db;

        public PricelistsController()
        {
            db = new ApplicationDbContext();
            ViewData["navigationPartialData"] = new AdminViewModel(db);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
        }

        // GET: Pricelists
        public ActionResult Index()
        {
            var pricelists = db.Pricelists.ToList();

            return View(pricelists);
        }

        public ActionResult New()
        {
            var viewModel = new PricelistViewModel();

            return View("PricelistForm", viewModel);
        }

        public ActionResult Edit(int id)
        {
            var pricelist = db.Pricelists.SingleOrDefault(p => p.Id == id);

            if (pricelist == null)
                return HttpNotFound();

            var viewModel = new PricelistViewModel
            {
                Pricelist = pricelist
            };

            return View("PricelistForm", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Pricelist pricelist)
        {
            if (pricelist.Id == 0)
                db.Pricelists.Add(pricelist);
            else
            {
                var pricelistDb = db.Pricelists.Single(p => p.Id == pricelist.Id);

                pricelistDb.From = pricelist.From;
                pricelistDb.To = pricelist.To;
                pricelistDb.Cost = pricelist.Cost;
                pricelistDb.Rate = pricelist.Rate;
                pricelistDb.Days = pricelist.Days;
            }

            db.SaveChanges();

            var ratePricelist = db.RatePricelists.Include(r => r.Rate).SingleOrDefault(r => r.PriceListId == pricelist.Id);
            var qualityPricelist = db.QualityPricelists.Include(r => r.QualityType).SingleOrDefault(r => r.PricelistId == pricelist.Id);
            var deliveryDeadlinePricelist = db.DeliveryDeadlinePricelists.Include(r => r.DeliveryDeadlineType).SingleOrDefault(r => r.PricelistId == pricelist.Id);

            if (ratePricelist == null && qualityPricelist == null && deliveryDeadlinePricelist == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (ratePricelist != null)
            {
                var rate = db.Rates.Find(ratePricelist.RateId);

                return RedirectToAction("Edit", "Rates", new { id = rate.Id });
            }

            if (qualityPricelist != null)
            {
                var qualityType = db.QualityTypes.Find(qualityPricelist.QualityTypeId);

                return RedirectToAction("Edit", "QualityTypes", new { id = qualityType.Id });
            }

            var deliveryDeadlineType = db.DeliveryDeadlineTypes.Find(deliveryDeadlinePricelist.DeliveryDeadlineTypeId);

            return RedirectToAction("Edit", "DeliveryDeadline", new { id = deliveryDeadlineType.Id });
        }

        public ActionResult Delete(bool confirm, int id)
        {
            var pricelist = db.Pricelists.Find(id);

            if (pricelist == null)
                return HttpNotFound();

            var ratePricelist = db.RatePricelists.Include(r => r.Rate).SingleOrDefault(r => r.PriceListId == id);
            var qualityPricelist = db.QualityPricelists.Include(r => r.QualityType).SingleOrDefault(r => r.PricelistId == id);
            var deliveryDeadlinePricelist = db.DeliveryDeadlinePricelists.Include(r => r.DeliveryDeadlineType).SingleOrDefault(r => r.PricelistId == id);

            if (ratePricelist == null && qualityPricelist == null && deliveryDeadlinePricelist == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (confirm)
            {
                db.Pricelists.Remove(pricelist);
                db.SaveChanges();
            }

            if (ratePricelist != null)
            {
                var rate = db.Rates.Find(ratePricelist.RateId);

                return RedirectToAction("Edit", "Rates", new { id = rate.Id });
            }

            if (qualityPricelist != null)
            {
                var qualityType = db.QualityTypes.Find(qualityPricelist.QualityTypeId);

                return RedirectToAction("Edit", "QualityTypes", new { id = qualityType.Id });
            }


            if (deliveryDeadlinePricelist != null)
            {
                var deliveryType = db.DeliveryDeadlineTypes.Find(deliveryDeadlinePricelist.DeliveryDeadlineTypeId);

                return RedirectToAction("Edit", "DeliveryDeadline", new { id = deliveryType.Id });
            }

            var deliveryDeadlineType = db.DeliveryDeadlineTypes.Find(deliveryDeadlinePricelist.DeliveryDeadlineTypeId);

            return RedirectToAction("Edit", "Pricelists", new { id = deliveryDeadlineType.Id });
        }
    }
}