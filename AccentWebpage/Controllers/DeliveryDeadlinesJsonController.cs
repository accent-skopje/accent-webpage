﻿using AccentWebpage.Models;
using AccentWebpage.ViewModels;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace AccentWebpage.Controllers
{
    public class DeliveryDeadlinesJsonController : Controller
    {
        private ApplicationDbContext db;

        public DeliveryDeadlinesJsonController()
        {
            db = new ApplicationDbContext();
            ViewData["navigationPartialData"] = new AdminViewModel(db);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
        }

        [HttpPost]
        // GET: DeliveryDeadlinesJson
        public JsonResult Index(int words)
        {
            var deliveryDeadline = db.DeliveryDeadlineTypes.ToList();
            var deliveryDeadlinePricelists = db.DeliveryDeadlinePricelists.Include(p => p.Pricelist)
                .Where(d => d.Pricelist.From <= words && d.Pricelist.To >= words).ToList();

            var viewModel = new DeliveryDedlineViewModel()
            {
                DeliveryDeadlineTypes = deliveryDeadline,
                DeliveryDeadlinePricelists = deliveryDeadlinePricelists
            };

            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }
    }
}