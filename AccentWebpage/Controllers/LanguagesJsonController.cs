﻿using System.Collections.Generic;
using System.Data.Entity;
using AccentWebpage.Models;
using AccentWebpage.ViewModels;
using System.Linq;
using System.Web.Mvc;

namespace AccentWebpage.Controllers.Iq
{
    public class LanguagesJsonController : Controller
    {
        private ApplicationDbContext db;

        public LanguagesJsonController()
        {
            db = new ApplicationDbContext();
            ViewData["navigationPartialData"] = new AdminViewModel(db);
        }

        [HttpPost]
        // GET: Languages
        public JsonResult Index()
        {
            var languages = db.Languages.ToList();

            var viewModel = new LanguageViewModel()
            {
                Languages = languages
            };

            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetLanguages(string id)
        {
            var rates = db.Rates.Include(r => r.DestinationLanguage).Where(r => r.SourceLanguageId.ToString() == id);
            List<Language> languages = new List<Language>();

            foreach (var rate in rates)
            {
                languages.Add(rate.DestinationLanguageId);
            }

            var viewModel = new LanguageViewModel()
            {
                Languages = languages
            };

            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }

    }
}