﻿using AccentWebpage.Models;
using AccentWebpage.ViewModels;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace AccentWebpage.Controllers
{
    [Authorize]
    public class LanguagesController : Controller
    {
        private ApplicationDbContext db;

        public LanguagesController()
        {
            db = new ApplicationDbContext();
            ViewData["navigationPartialData"] = new AdminViewModel(db);
        }

        // GET: Languages
        public ActionResult Index()
        {
            var languages = db.Languages.ToList();

            var viewModel = new LanguageViewModel
            {
                Languages = languages
            };

            return View(viewModel);
        }

        // GET: Languages/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Language language = db.Languages.Find(id);
            if (language == null)
            {
                return HttpNotFound();
            }
            return View(language);
        }

        // GET: Languages/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,ISO")] Language language)
        {
            if (ModelState.IsValid)
            {
                db.Languages.Add(language);
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            return View(language);
        }

        // GET: Languages/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Language language = db.Languages.Find(id);

            if (language == null)
            {
                return HttpNotFound();
            }

            return View(language);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,ISO")] Language language)
        {
            var languageInDb = db.Languages.Find(language.Id);

            if (languageInDb == null)
            {
                db.Languages.Add(language);
                return RedirectToAction("Index");
            }

            if (ModelState.IsValid)
            {
                db.Entry(languageInDb).CurrentValues.SetValues(language);
                db.Entry(languageInDb).State = EntityState.Modified;
                //db.Entry(language).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            return View(language);
        }

        // GET: Languages/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Language language = db.Languages.Find(id);

            if (language == null)
            {
                return HttpNotFound();
            }

            return View(language);
        }

        // POST: Languages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Language language = db.Languages.Find(id);
            db.Languages.Remove(language);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}