﻿using AccentWebpage.Models;
using AccentWebpage.ViewModels;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace AccentWebpage.Controllers
{
    public class QualityTypesJsonController : Controller
    {
        private ApplicationDbContext db;

        public QualityTypesJsonController()
        {
            db = new ApplicationDbContext();
            ViewData["navigationPartialData"] = new AdminViewModel(db);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
        }

        [HttpPost]
        // GET: QualityTypesJson
        public JsonResult Index()
        {
            var qualityTypes = db.QualityTypes.ToList();
            var qualityTypesPricelists = db.QualityPricelists.Include(q => q.Pricelist).ToList();

            var viewModel = new QualityViewModel()
            {
                QualityTypes = qualityTypes,
                QualityPricelists = qualityTypesPricelists
            };

            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }
    }
}