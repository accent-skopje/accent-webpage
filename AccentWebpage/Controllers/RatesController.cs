﻿using AccentWebpage.Models;
using AccentWebpage.ViewModels;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace AccentWebpage.Controllers
{
    [Authorize]
    public class RatesController : Controller
    {
        private ApplicationDbContext db;

        public RatesController()
        {
            db = new ApplicationDbContext();
            ViewData["navigationPartialData"] = new AdminViewModel(db);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
        }

        // GET: Rates
        public ActionResult Index()
        {
            var rates = db.Rates.Include(l => l.SourceLanguage).Include(l => l.DestinationLanguage).ToList();
            var ratePricelists = db.RatePricelists.Include(r => r.Pricelist).ToList();

            var viewModel = new RateViewModel
            {
                Rates = rates,
                RatePricelists = ratePricelists
            };

            return View(viewModel);
        }

        public ActionResult New()
        {
            var languages = db.Languages.ToList();

            var viewModel = new RateViewModel
            {
                SourceLanguages = languages,
                DestinationLanguages = languages,
                Pricelists = new List<Pricelist>()
            };

            return View("RateForm", viewModel);
        }

        public ActionResult Edit(int id)
        {
            var rateDb = db.Rates.Include(r => r.SourceLanguage).Include(r => r.DestinationLanguage).SingleOrDefault(r => r.Id == id);
            var ratePricelistsDb = db.RatePricelists.Include(r => r.Pricelist).Include(r => r.Rate).Where(r => r.RateId == id);
            var pricelistsDb = new List<Pricelist>();

            if (ratePricelistsDb != null)
            {
                foreach (var ratePricelist in ratePricelistsDb)
                {
                    pricelistsDb.Add(ratePricelist.Pricelist);
                }
            }

            if (rateDb == null)
                return HttpNotFound();

            var viewModel = new RateViewModel
            {
                Rate = rateDb,
                Pricelists = pricelistsDb,
                SourceLanguages = db.Languages.Where(l => l.Id == rateDb.SourceLanguage),
                DestinationLanguages = db.Languages.Where(l => l.Id == rateDb.DestinationLanguage)
            };

            return View("RateForm", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Rate rate, Pricelist pricelist)
        {
            int i = 1;

            if (rate.Id == 0 && pricelist.Id == 0)
            {
                var rateDb =
                    db.Rates.SingleOrDefault(
                        r =>
                            r.SourceLanguageId == rate.SourceLanguageId &&
                            r.DestinationLanguageId == rate.DestinationLanguageId);

                RatePricelist ratePricelist;

                if (rateDb != null)
                {
                    ratePricelist = new RatePricelist
                    {
                        Rate = rateDb,
                        Pricelist = pricelist
                    };
                }
                else
                {
                    db.Rates.Add(rate);

                    ratePricelist = new RatePricelist
                    {
                        Rate = rate,
                        Pricelist = pricelist
                    };
                }

                pricelist.Name = "Words " + i;

                db.Pricelists.Add(pricelist);
                db.RatePricelists.Add(ratePricelist);
            }
            else
            {
                var rateDb =
                    db.Rates.Include(r => r.SourceLanguage)
                        .Include(r => r.DestinationLanguage)
                        .Single(r => r.Id == rate.Id);

                rateDb.SourceLanguageId = rate.SourceLanguageId;
                rateDb.DestinationLanguageId = rate.DestinationLanguageId;
                rateDb.IsSupported = rate.IsSupported;

                i = db.RatePricelists.Where(r => r.RateId == rateDb.Id).ToList().Count + 1;
                pricelist.Name = "Words " + i;

                if (pricelist.From != 0 && pricelist.To != 0 && pricelist.Cost != 0)
                {
                    db.Pricelists.Add(pricelist);

                    RatePricelist ratePricelist = new RatePricelist
                    {
                        Rate = rateDb,
                        Pricelist = pricelist
                    };

                    db.RatePricelists.Add(ratePricelist);
                }
            }

            db.SaveChanges();

            return RedirectToAction("Edit", "Rates", new { id = rate.Id });
        }

        public ActionResult Delete(bool confirm, int id)
        {
            var rate = db.Rates.Find(id);

            if (rate == null)
                return HttpNotFound();

            var ratePricelist = db.RatePricelists.Include(r => r.Rate).Where(r => r.RateId == id);

            if (confirm)
            {
                db.Rates.Remove(rate);

                foreach (var item in ratePricelist)
                {
                    var pricelist = db.Pricelists.Find(item.PriceListId);

                    if(pricelist != null)
                        db.Pricelists.Remove(pricelist);
                }

                db.SaveChanges();
            }

            return RedirectToAction("Index", "Rates");
        }
    }
}