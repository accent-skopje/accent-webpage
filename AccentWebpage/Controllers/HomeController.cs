﻿using AccentWebpage.Models;
using AccentWebpage.ViewModels;
using System.Linq;
using System.Web.Helpers;
using System.Web.Mvc;

namespace AccentWebpage.Controllers
{
    //[Authorize]
    public class HomeController : Controller
    {
        private ApplicationDbContext db;

        public HomeController()
        {
            db = new ApplicationDbContext();
        }

        public ActionResult Index()
        {
            var viewModel = new AdminViewModel(db);

            return View(viewModel);
        }

        public ActionResult MyChart()
        {
            decimal Jan = 0;
            decimal Feb = 0;
            decimal Mar = 4;
            decimal Apr = 0;
            decimal May = 0;
            decimal Jun = 0;
            decimal Jul = 0;
            decimal Aug = 0;
            decimal Sept = 6;
            decimal Oct = 0;
            decimal Nov = 0;
            decimal Dec = 10;

            new Chart(width: 800, height: 200)
                .AddSeries(
                    chartType: "colmn",
                    xValue: new[] {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"},
                    yValues: new[] {Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sept, Oct, Nov, Dec})
                .Write("png");

            return null;
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}