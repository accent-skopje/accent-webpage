﻿using AccentWebpage.Models;
using AccentWebpage.ViewModels;
using System.Linq;
using System.Web.Helpers;
using System.Web.Mvc;

namespace AccentWebpage.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        private ApplicationDbContext db;

        public AdminController()
        {
            db = new ApplicationDbContext();
            ViewData["navigationPartialData"] = new AdminViewModel(db);
        }

        public ActionResult OrderChart()
        {
            decimal Jan = 0;
            decimal Feb = 0;
            decimal Mar = 4;
            decimal Apr = 0;
            decimal May = 0;
            decimal Jun = 0;
            decimal Jul = 0;
            decimal Aug = 0;
            decimal Sept = 6;
            decimal Oct = 0;
            decimal Nov = 0;
            decimal Dec = 10;

            new Chart(width: 800, height: 250, theme: ChartTheme.Yellow)
                .AddSeries(
                    chartType: "column",
                    xValue: new[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec" },
                    yValues: new[] { Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sept, Oct, Nov, Dec })
                .Write("png");

            return null;
        }

        public ActionResult NewsChart()
        {
            decimal Jan = 0;
            decimal Feb = 0;
            decimal Mar = 4;
            decimal Apr = 0;
            decimal May = 5;
            decimal Jun = 0;
            decimal Jul = 0;
            decimal Aug = 0;
            decimal Sept = 6;
            decimal Oct = 0;
            decimal Nov = 11;
            decimal Dec = 10;


        new Chart(width: 800, height: 250, theme: ChartTheme.Yellow)
                .AddSeries(
                    chartType: "column",
                    xValue: new[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec" },
                    yValues: new[] { Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sept, Oct, Nov, Dec })
                .Write("png");

            return null;
        }

        public ActionResult Index()
        {
            var viewModel = new AdminViewModel(db);

            return View(viewModel);
        }
    }
}