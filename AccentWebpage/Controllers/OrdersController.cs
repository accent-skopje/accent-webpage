﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using AccentWebpage.Models;
using AccentWebpage.ViewModels;
using PagedList;

namespace AccentWebpage.Controllers
{
    public class OrdersController : Controller
    {
        private ApplicationDbContext db;

        public OrdersController()
        {
            db = new ApplicationDbContext();
            ViewData["navigationPartialData"] = new AdminViewModel(db);

        }
        // GET: Orders
        public ActionResult Index(int? page)
        {
            var orders = db.Orders.OrderByDescending(o => o.Id).ToList();

            int pageSize = 15;
            int pageNumber = (page ?? 1);
            return View(orders.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult Details(int id)
        {
            var order = db.Orders.SingleOrDefault(o => o.Id == id);
            var viewModel = new OrderViewModel()
            {
                Order = order
            };

            return PartialView("Details", viewModel);
        }
   
    }
}