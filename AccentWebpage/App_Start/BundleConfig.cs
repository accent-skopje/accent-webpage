﻿using System.Web;
using System.Web.Optimization;

namespace AccentWebpage
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery.min.js",
                        "~/Scripts/bootstrap.min.js",
                        "~/Scripts/smoothscroll.js",
                        "~/Scripts/wow.min.js",
                        "~/Scripts/jquery.magnific-popup.min.js",
                        "~/Scripts/owl.carousel.min.js",
                        //"~/Scripts/jquery.validate.min.js",
                        "~/Scripts/jquery.stellar.min.js",
                        "~/Scripts/jquery.easypiechart.min.js",
                        "~/Scripts/jquery.ajaxchimp.min.js",
                        "~/Scripts/interface.js",
                        "~/Scripts/gmap.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/raphael-min",
                      "~/Scripts/morris.js",
                      "~/Scripts/app.min.js",
                      "~/Scripts/pages/dashboard2.js",
                      "~/Scripts/demo.js",
                      "~/Scripts/moment.min.js",
                      "~/Scripts/respond.js"
                      //"~/Scripts/bootstrap.js",
                      //"~/Scripts/bootstrap.min.js",
                      //"~/Scripts/respond.js"
                      ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/font-awesome.min.css",
                      "~/Content/ionicons.min.css",
                      //"~/Content/AdminLTE.css",
                      "~/Content/skins/_all-skins.min.css",
                      "~/Content/blue.css",
                      "~/Content/morris.css",
                      "~/Content/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css",
                      //"~/Content/site.css",
                      "~/Content/style.css"));
        }
    }
}
