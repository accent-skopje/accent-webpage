﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using HSense.IQ.BsinessLogic;
using ICSharpCode.SharpZipLib.Zip;
using Our.Umbraco.uGoLive.Web;
using umbraco.presentation.umbracobase;
using UmbracoProject.umbraco.HSense.IQ.BsinessLogic;
using UmbracoProject.umbraco.HSense.IQ.BsinessLogic.Models;

namespace HSense.IQ.Macro
{
    [RestExtension("quoteAPI")]
    public partial class InstantQuotationMacro : System.Web.UI.UserControl
    {
        private static readonly IEnumerable<Language> sourceLanguages = Language.GetLanguages();
        private static readonly IEnumerable<Language> targetLanguages = Language.GetLanguages();
        private static readonly IEnumerable<Subject> subjects = Subject.GetSubjects();

        [RestExtensionMethod(returnXml = false, allowAll = true)]
        public static void PageLoad()
        {
            PageLoadModel model = new PageLoadModel
                                  {
                                      sourceLanguageList = new List<DropdownModel>(),
                                      targetLanguageList = new List<DropdownModel>(),
                                      subjectList = new List<DropdownModel>()
                                  };

            #region Source Languages

            model.sourceLanguageList = sourceLanguages.Select(item => new DropdownModel()
                                                                     {
                                                                         Value = item.Id,
                                                                         Text = item.Name
                                                                     }).ToList();


            #endregion

            #region Target Languages
            model.targetLanguageList = targetLanguages.Select(item => new DropdownModel()
                                                                                                {
                                                                                                    Value = item.Id,
                                                                                                    Text = item.Name
                                                                                                }).ToList();
            #endregion

            #region Subject Type
            model.subjectList = subjects.Select(item => new DropdownModel()
            {
                Value = item.Id,
                Text = item.Name
            }).ToList();



            #endregion

            var json = new JavaScriptSerializer().Serialize(model);

            HttpContext.Current.Response.ContentType = "application/json";
            HttpContext.Current.Response.Write(json);
        }

        protected void btnSubmit_OnClick(object sender, EventArgs e)
        {
            #region Parse Results
            int sourceLanguageID = Convert.ToInt32(sourceLanguage.SelectedValue);
            int targetLanguageID = Convert.ToInt32(targetLanguage.SelectedValue);
            int subjectFieldID = Convert.ToInt32(subjectField.SelectedValue);
            var dateBy = Convert.ToDateTime(deliveryDate.Text);
            int numWords = Convert.ToInt32(wordCount.Text);
            #endregion

            #region Fill Results
            var allquotes = Calculations.QuoteCalc(sourceLanguageID, targetLanguageID, subjectFieldID, numWords, dateBy);

            if (allquotes.Premium != null)
            {
                PremiumPrice.Text = allquotes.Premium.Price;
                PremiumDate.Text = allquotes.Premium.DeliveryDate;
                PremiumPanel.Visible = true;
            }
            else
            {
                NoPremiumPanel.Visible = true;
            }


            if (allquotes.Professional != null)
            {
                ProfessionalPrice.Text = allquotes.Professional.Price;
                ProfessionalDate.Text = allquotes.Professional.DeliveryDate;
                ProfessionalPanel.Visible = true;
            }
            else
            {
                NoProfessionalPanel.Visible = true;
            }


            if (allquotes.Economy != null)
            {
                EconomyPrice.Text = allquotes.Economy.Price;
                EconomyDate.Text = allquotes.Economy.DeliveryDate;
                EconomyPanel.Visible = true;
            }
            else
            {
                NoEconomyPanel.Visible = true;
            }

            #endregion

            resultView.Visible = true;
        }

        [RestExtensionMethod(returnXml = false, allowAll = true)]

        public static void Hello()
        {
            TestModel model = new TestModel()
                              {
                                  Name = "John",
                                  LastName = "Doe"
                              };
            var json = new JavaScriptSerializer().Serialize(model);

            HttpContext.Current.Response.ContentType = "application/json";
            HttpContext.Current.Response.Write(json);
        }

        public class TestModel
        {
            public string Name { get; set; }
            public string LastName { get; set; }
        }

    }
}