$(function() {
    //$('#document').bootstrapFileInput();
    var control = $("#document");
    
    $('#rootwizard').bootstrapWizard({'tabClass': 'nav nav-tabs'});
    $('select').selectpicker();
    $('#txtPasteText').focus();
    var filesInList = [];
    $("#document").on("change", handleFileSelect);

    var totalWordCount = 0;
    var totalPriceNoOptions = 0;
        
    function handleFileSelect(event) {
        var file = event.target.files[0];
        countWordsFromFile(file);
    }

    /*$('#document').fileupload({
        change: function (e, data) {
            $.each(data.files, function (index, file) {
                console.log('Selected file: ' + file.name);
            });
        }
    });*/

    $("#btnCalculate").on("click", function(){
        if($("#textContainer").is(":visible")) {
            var text = $("#txtPasteText").val();
            wordCount(text);
            $("#textContainer").fadeOut();
            $("#txtPasteText").val("");
        } else {
            var file = $("#document")[0].files[0];
            countWordsFromFile(file);

            control.replaceWith( control = control.clone( true ) );
        }
    });

    $("#btnAddText").on("click", function(){
        if($("#textContainer").is(":visible")){
            $("#textContainer").fadeOut();
            $("#txtPasteText").val("");
        } else {
            $("#textContainer").fadeIn();    
        }
    });

    $("#btnAddWordCount").on("click", function(){
        if($("#wordsContainer").is(":visible")){
            $("#wordsContainer").fadeOut();
            $("#txtNumberOfWords").val("");
        } else {
            $("#wordsContainer").fadeIn();
        }
    });

    $("#btnAddNumber").on("click", function(){
        var numberWords = parseInt($("#txtNumberOfWords").val());
        updateList(numberWords, "Word count");
        /*var totalWordCount = parseInt($("#totalWordCount").text());
        totalWordCount += numberWords;
        $("#totalWordCount").text(totalWordCount);*/
        $("#wordsContainer").fadeOut();
        $("#txtNumberOfWords").val("");
    });

    $("#langTo").on("change", function(){
        var allSelected = $("#langTo").val();
        
        if(allSelected.length == 0){
            $("#target_language_list_render_section").fadeOut();
        } else {
            $("#languagesTable tbody").empty();
            $("#target_language_list_render_section").fadeIn();
            $('#langTo :selected').each(function(i, selected){ 
                var newLangLine = '';
                var langPrice = 0.06*totalWordCount;
                totalPriceNoOptions += langPrice;
                if(i == 0){
                    newLangLine = '<tr class="language_line"><td class="col-lan-name">'+$(selected).text()+'</td><td class="col-unit-price">$0.06 / word</td><td class="col-total-units" rowspan="4">x ' + totalWordCount + ' words</td><td class="col-part-price"><span class="part_price_text">$'+langPrice.toFixed(2)+'</span></td></tr>';
                } else {
                    newLangLine = '<tr class="language_line"><td class="col-lan-name">'+$(selected).text()+'</td><td class="col-unit-price">$0.06 / word</td><td class="col-part-price"><span class="part_price_text">$'+langPrice.toFixed(2)+'</span></td></tr>';
                }

                $("#languagesTable tbody").append(newLangLine);
            });
            $(".col-total-units").attr("rowspan", allSelected.length);
            $(".col-total-units").text("x " + totalWordCount + " words");
            $(".col-total-price").text("$"+totalPriceNoOptions);
            $(".col-total-lang span").text(allSelected.length);
        }
    });

    function countWordsFromFile(file){
        var filetype = getFileExtension(file.name);
        if (filetype == 'docx') {
            countWordsFromDocx(file);
        }
        if (filetype == 'xlsx') {
            countWordsFromXslx(file);
        }
        if(filetype == 'txt'){
            countWordsFromTxt(file);
        }
    }

    function countWordsFromDocx(file){
        readFileInputEventAsArrayBuffer(file, function(arrayBuffer) {
            mammoth.extractRawText({arrayBuffer: arrayBuffer})
                .then(function(result){
                    var filename = file.name
                    wordCount(result.value, filename);
                })
                .done();
        });
    }

    function countWordsFromXslx(file){
        readFileInputEventAsArrayBuffer(file, function(arrayBuffer){
            var wb = XLSX.read(arrayBuffer, {type: 'buffer'});
            var text = "";
            for (var i=0; i<wb.Strings.length; i++){ 
                text += " " + wb.Strings[i].t;
            }
            var filename = file.name
            wordCount(text, filename);
        });
    }

    function countWordsFromTxt(file){
        readFileInputEventAsString(file, function(text){
            var filename = file.name
            wordCount(text, filename);
        });
    }

    function wordCount(text, filename){
        var regex = /\s+/gi;
        var wordsArray = text.trim().replace(regex, ' ').split(' ');
        var wordCount = wordsArray.length;
        if(!filename){
            var firstFive = wordsArray.slice(0, 5);
            filename = firstFive.join(" ");
        }
        
        updateList(wordCount, filename);
    }

    function updateList(wordCount, filename){
        $("#files").append('<li class="list-group-item"><span class="badge">'+wordCount+'</span>' + filename + '</li>');
        filesInList.push({filename: filename, wordCount: wordCount});
        if($("#textContainer").is(":visible")){
            $("#textContainer").fadeOut();
            $("#txtPasteText").val("");
        }
        displayTotalWords(filesInList);
    }

    function getFileExtension(filename){
        return filename.split('.').pop();
    }
    
    function displayResult(result) {
        document.getElementById("output").innerHTML = result.value;
        
        var messageHtml = result.messages.map(function(message) {
            return '<li class="' + message.type + '">' + escapeHtml(message.message) + "</li>";
        }).join("");
        
        document.getElementById("messages").innerHTML = "<ul>" + messageHtml + "</ul>";
    }

    function displayTotalWords(listOfFiles){
        var total = 0;
        var numberItems = listOfFiles.length;
        for (var i = 0; i < numberItems; i++) {
            total += listOfFiles[i].wordCount;
        };
        $("#totalWordCount").text(total);
        $("#numberItems").text(numberItems);
        totalWordCount = total;
    }
    
    function readFileInputEventAsArrayBuffer(file, callback) {
        var file = file;

        var reader = new FileReader();
        
        reader.onload = function(loadEvent) {
            var arrayBuffer = loadEvent.target.result;
            callback(arrayBuffer);
        };
        
        reader.readAsArrayBuffer(file);
    }

    function readFileInputEventAsString(file, callback){
        var file = file;

        var reader = new FileReader();
        
        reader.onload = function(loadEvent) {
            var text = loadEvent.target.result;
            callback(text);
        };
        
        reader.readAsText(file);
    }

    function escapeHtml(value) {
        return value
            .replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
    }
});