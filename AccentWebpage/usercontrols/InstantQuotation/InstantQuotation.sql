/****** Object:  Table [dbo].[hsenseIqEmailRecepients]    Script Date: 25.02.2015 23:16:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[hsenseIqEmailRecepients](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[hsenseIqEmailSettings]    Script Date: 25.02.2015 23:17:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[hsenseIqEmailSettings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LogoUrl] [nvarchar](150) NULL,
	[IntroText] [text] NULL,
	[OutroText] [text] NULL,
	[CompanyName] [nvarchar](50) NULL,
	[TelephoneNumber] [nvarchar](50) NULL,
	[VisitingAddress] [nvarchar](50) NULL,
	[PotalAddress] [nvarchar](50) NULL,
	[FooterTelephone] [nvarchar](50) NULL,
	[Fax] [nvarchar](50) NULL,
	[OrgNr] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Website] [nvarchar](50) NULL,
 CONSTRAINT [PK_hsenseIqEmailSettings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

/****** Object:  Table [dbo].[hsenseIqLanguages]    Script Date: 25.02.2015 23:18:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[hsenseIqLanguages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[IsoCode] [nvarchar](3) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[hsenseIqOrderRecepients]    Script Date: 25.02.2015 23:18:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[hsenseIqOrderRecepients](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
 CONSTRAINT [PK_hsenseIqOrderRecepients] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[hsenseIqPlans]    Script Date: 25.02.2015 23:18:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[hsenseIqPlans](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Value] [decimal](18, 3) NOT NULL DEFAULT ((1)),
	[Days] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_hsenseiqPhases] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[hsenseIqTextTypes]    Script Date: 25.02.2015 23:19:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[hsenseIqTextTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Value] [decimal](18, 3) NOT NULL CONSTRAINT [DF_hsenseIqTextTypes_Value]  DEFAULT ((1)),
 CONSTRAINT [PK_hsenseiqSubject] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[hsenseIqRates]    Script Date: 25.02.2015 23:19:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[hsenseIqRates](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SourceLanguage] [int] NOT NULL,
	[DestinationLanguage] [int] NOT NULL,
	[CostPerWord] [decimal](18, 3) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[hsenseIqRates]  WITH CHECK ADD  CONSTRAINT [FK_hsenseIqRates_hsenseIqLanguages] FOREIGN KEY([SourceLanguage])
REFERENCES [dbo].[hsenseIqLanguages] ([Id])
GO

ALTER TABLE [dbo].[hsenseIqRates] CHECK CONSTRAINT [FK_hsenseIqRates_hsenseIqLanguages]
GO

ALTER TABLE [dbo].[hsenseIqRates]  WITH CHECK ADD  CONSTRAINT [FK_hsenseIqRates_hsenseIqLanguages1] FOREIGN KEY([DestinationLanguage])
REFERENCES [dbo].[hsenseIqLanguages] ([Id])
GO

ALTER TABLE [dbo].[hsenseIqRates] CHECK CONSTRAINT [FK_hsenseIqRates_hsenseIqLanguages1]
GO

