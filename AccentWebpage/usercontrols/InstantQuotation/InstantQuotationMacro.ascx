﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InstantQuotationMacro.ascx.cs" Inherits="HSense.IQ.Macro.InstantQuotationMacro" %>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%=ResolveClientUrl("~/usercontrols/InstantQuotation/bootstrap.css") %>">
<link rel="stylesheet" type="text/css" href="<%=ResolveClientUrl("~/usercontrols/InstantQuotation/bootstrap-select.css") %>">
<link rel="stylesheet" type="text/css" href="<%=ResolveClientUrl("~/usercontrols/InstantQuotation/bootstrap-datepicker3.min.css") %>" />
<link rel="stylesheet" type="text/css" href="<%=ResolveClientUrl("~/usercontrols/InstantQuotation/style.css") %>">
<div class="container" style="background: #fff; padding-bottom: 20px;">
    <div class="row">
        <div id="rootwizard" class="tabbable col-md-9 col-xs-12">
            <ul>
                <li><a href="#tab1" data-toggle="tab">Text</a></li>
                <li><a href="#tab2" data-toggle="tab">Languages</a></li>
                <li><a href="#tab3" data-toggle="tab">Options</a></li>
            </ul>

            <div class="tab-content" style="border: 2px #D2AD4C solid; min-height: 375px;">

                <div class="tab-pane" id="tab1">
                    <div class="panel panel-default">
                        <div class="panel-heading">Add text to be translated</div>
                        <div class="panel-body">
                            <p>
                                <small class="text-muted" style="margin-bottom: 10px;">Observe that in uploaded files hidden texts, footnotes, comments, Picture titles are not Always included in the final Word Count. There might be differences in prices and we will be in Contact with you.</small>
                            </p>
                            <ul id="files" class="list-group"></ul>
                            <div id="textContainer" style="display: none;">
                                <textarea class="form-control" id="txtPasteText" style="width: 100%;" placeholder="Start typing or paste text here"></textarea>
                                <span class="row">
                                    <span class="btn btn-default pull-right btn-sm" id="btnCalculate">Calculate</span>
                                </span>
                            </div>

                            <div class="input-group col-xs-6 col-md-6 pull-right" id="wordsContainer" style="display: none;">
                                <input type="number" class="form-control" id="txtNumberOfWords" placeholder="Number of words">
                                <span class="input-group-btn">
                                    <input type="button" class="btn btn-default" id="btnAddNumber" value="Add" />
                                </span>
                            </div>

                            <div class="row btn-group" role="group" style="margin: auto;">
                                <span class="btn btn-default fileinput-button">
                                    <span>Add File</span>
                                    <!-- The file input field used as target for the file upload widget -->
                                    <input id="document" type="file" name="files[]" multiple>
                                </span>

                                <span class="btn btn-default" id="btnAddWordCount">Add Word Count</span>
                                <span class="btn btn-default" id="btnAddText">Paste Text</span>
                            </div>
                        </div>
                        <small class="text-muted">Following file types are supported .docx, .xlsx and .txt</small>
                    </div>
                </div>
                <div class="tab-pane" id="tab2">
                    <div class="panel panel-default">
                        <div class="panel-heading">Choose languages for translation</div>
                        <div class="panel-body">
                            <div class="row col-md-6 col-xs-12">
                                <h4 style="font-size: 20px;">Translate from</h4> 
                                
                                    <select id="langFrom" multiple data-max-options="1" data-style="btn-default" data-size="5" data-title="Select a language"></select>
                                
                                <small class="text-muted" style="display: block;padding-top: 10px;width: 220px;">Observe that you only can select "one" source language at a time</small>
                            </div>
                            <div class="row col-md-6 col-xs-12">
                                <h4 style="font-size: 20px;">Translate to</h4>
                                
                                <select id="langTo" multiple data-style="btn-default" data-size="5" data-title="Select a language">
                                </select>
                                
                                
                            </div>
                            <div class="row col-md-12 col-xs-12" style="margin-top: 15px;">
                                <div id="target_language_list_render_section" style="display: none;">
                                    <span>Prices shown are per document and for all selected destination languages. Order details:</span>
                                    <div id="target_language_list_section">
                                        <table class="table" id="languagesTable">
                                            <tbody>
                                            </tbody>
                                            <tfoot id="language_summary_render_section">
                                                <tr id="language_summary_section">
                                                    <th class="col-total-lang" colspan="2"></th>
                                                    <th colspan="2"><div class="pull-right">Total: <span class="col-total-price"></span></div></th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        <small style="display: none;" class="lblMinimumOrderPrice text-danger text-uppercase">Minimum order price is 690 SEK <span class="exclude-vat"></span> / 862,5 SEK <span class="include-vat"></span></small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab3">
                    <div class="panel panel-default">
                        <div class="panel-heading">Options for translation</div>
                        <div class="panel-body">
                            <div style="padding: 15px 0 22px 15px;">
                                <div class="row col-md-12 col-xs-12">
                                    <h4 style="font-size: 20px;">Choose from one of our propsals</h4>
                                    <table id="plansTable" class="row col-md-12 col-xs-12">
                                        <tr>
                                        </tr>
                                    </table>
                                </div>

                                <div class="row col-md-12 col-xs-12" style="margin-top: 20px;">
                                    <h4 style="font-size: 20px;">Choose a predefined delivery deadline</h4>
                                    <%--<div class="row">--%>
                                        <div class="list-group" id="pricingTable">
                                        </div>
                                    <%--</div>--%>
                                </div>
                                
                                <div class="row col-md-12 col-xs-12">
                                    <h4 style="font-size: 20px; padding: 0;" class="col-md-9 col-xs-12">Choose your own delivery date </h4>
                                    <input class="col-md-3 col-xs-12 datepicker" data-provide="datepicker">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>


                <ul class="pager wizard" style="margin-left: 0; border-top: #d2ad4c 2px solid;">
                    <li class="previous first " style="display: none;"><a href="#">First</a></li>
                    <li class="previous "><a href="#">Previous</a></li>
                    <li class="next last " style="display: none;"><a href="#">Last</a></li>
                    <li class="next "><a href="#">Next</a></li>
                </ul>
            </div>
        </div>

        <div id="quote_section" class="col-md-3 col-xs-12">

            <div style="margin-top: 30px; border-left: #d2ad4c solid 2px; min-height: 410px;">
                <div style="margin-left: 20px;">
                    <div id="quote_summary_section">
                        <h4>Your Order</h4>
                        <table class="table quote-table">
                            <tbody>
                                <tr class="adding_content_row complete clickable">

                                    <td colspan="2">
                                        <strong id="numberItems">0</strong> items
                                    </td>
                                    <td colspan="2" class="quote-value">
                                        <strong class="pull-right"><span id="totalWordCount">0</span> words</strong>
                                    </td>
                                </tr>
                                <tr class="choose_language_row complete clickable">
                                    <td colspan="2">Source Language
                                    </td>
                                    <td colspan="2" class="quote-value col-src-lang">
                                        <strong class="pull-right"></strong>
                                    </td>
                                </tr>
                                <tr class="choose_language_row complete clickable">
                                    <td colspan="2">Destination Languages
                                    </td>
                                    <td colspan="2" class="quote-value col-total-lang">
                                        <strong class="pull-right">0</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" style="border-top: 0;">
                                        <ul id="languagesList">
                                        </ul>
                                    </td>
                                </tr>
                                <tr style="display: none;" class="lblMinimumOrderPrice">
                                    <td colspan="4" style="border-top: 0;">
                                        <small class="text-danger text-uppercase">Minimum order price is 690 SEK</small>
                                    </td>
                                </tr>
                                <tr class="optional_row complete ">
                                    <td colspan="2">Delivery plan
                                    </td>
                                    <td colspan="2">
                                        <strong class="pull-right" id="deliveryPlanName"></strong>
                                    </td>
                                </tr>
                                <tr class="optional_row complete ">
                                    <td colspan="2">Estimated delivery
                                    </td>
                                    <td colspan="2">
                                        <strong class="pull-right" id="estimatedDelivery"></strong>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>


                                <tr>
                                    <th colspan="2" class="quote-title">
                                        <div style="color: black;">Order total <br/><span class="exclude-vat"></span></div>
                                        <div>Order total <br/><span class="include-vat"></span></div>
                                    </th>

                                    <th colspan="2">
                                        <br/>
                                        <div style="color: black;"><strong class="pull-right" id="orderTotal"></strong>
                                        </div><br/>
                                        <br/><strong class="pull-right" id="orderTotalWithVat"></strong>
                                    </th>
                                </tr>

                            </tfoot>
                        </table>
                    </div>
                    <p>
                    <small style="display: none;" class="lblMinimumOrderPrice text-danger text-uppercase">Minimum order price is 690 SEK <span class="exclude-vat"></span></small>
                    </p>
                    <p>
                        <small style="display: none;" class="lblMinimumOrderPrice text-danger text-uppercase">Minimum order price is 862,5 SEK <span class="include-vat"></span></small>
                    </p>
                    <%--<p class="eta-text">Estimated delivery <strong class="estimatedDelivery"></strong><i class="helper_eta_popover_icon icon-tooltip" data-placement="top"></i></p>--%>


                    <label>
                        <input id="agree_term_chk" type="checkbox">
                        I agree to the
                        <asp:HyperLink runat="server" CssClass="termsLink" ID="lnkTerms" NavigateUrl="~/sv/vara-tjanster/oversattning/instantquotation/termsandconditions_iq/" Target="_blank">Terms &amp; Conditions</asp:HyperLink>
                    </label>


                    <button type="button" id="change_complete_btn" class="btn btn-core btn-next-page btn-block">Confirm Order</button>
                    <div class="modal fade" id="agreeTerms" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong>Agree on the terms & conditions</strong>
                                </div>
                                <div class="modal-body center-block">
                                    You have to agree to terms and conditions to confirm the order
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="removeWordCounts" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong>Remove Word Count items from the list</strong>
                                </div>
                                <div class="modal-body center-block">
                                    You cannot complete an order when a word count is in place. <br />
                                    Please remove the word count from your list before you can confirm the order.
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="profileData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong>Fill in your personal details to complete the order</strong>
                                </div>
                                <div class="modal-body center-block row">
                                    <form id="loginForm" method="post" class="form-horizontal">
                                        <h4 class="col-md-12 required"></h4>
                                        <div class="col-md-12 back-action">
                                            <span class="glyphicon glyphicon-chevron-left"></span><span class="glyphicon glyphicon-chevron-left"></span>&nbsp;<a href id="btnBack">Back</a>
                                        </div>
                                        <div class="form-group col-md-12 usertype">
                                            <span class="private-picker"><input type="checkbox" name="chbPrivate" id="chbPrivate" value="first_checkbox"/> <label for="chbPrivate">Private</label></span>
                                            <span class="company-picker"><input type="checkbox" name="chbCompany" id="chbCompany" value="first_checkbox"/> <label for="chbCompany">Company</label></span>
                                        </div>
                                        <div id="orderForm">
                                            <div class="company-fields">
                                                <div class="col-md-12">If you do not already have a customer number you first need to register one by clicking 'new customer'.Observe that only company customers can register for a customer number</div>
                                                <div class="form-group col-md-12">
                                                    <input type="checkbox" name="chbNew" id="chbNew" value="first_checkbox"/> <label for="chbNew">New Customer</label>    
                                                </div>
                                                <fieldset class="col-md-12"><legend>Customer details</legend>
                                                <div class="form-group col-md-6 col-xs-12 required">
                                                        <label class="control-label">Customer name</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon glyphicon glyphicon-briefcase"></span>
                                                            <input type="text" class="form-control" id="txtCompanyName" placeholder="Provide name" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6 col-xs-12 customer-number required">
                                                        <label class="control-label">Customer number</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon glyphicon glyphicon-barcode"></span>
                                                            <input type="text" class="form-control" id="txtCustomerNumber" placeholder="Sprekservice number" />
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <fieldset class="col-md-12"><legend>Contact person</legend>
										    <div class="form-group col-md-6 col-xs-12 required">
                                                    <label class="control-label">First name</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon glyphicon glyphicon-user"></span>
                                                        <input type="text" class="form-control" id="txtFirstName" placeholder="Provide name" />
                                                    </div>
                                                </div>
                                            <div class="form-group col-md-6 col-xs-12 required">
                                                    <label class="control-label">Last name</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon glyphicon glyphicon-user"></span>
                                                        <input type="text" class="form-control" id="txtLastName" placeholder="Provide name" />
                                                    </div>
                                                </div>
                                            <div class="form-group col-md-6 col-xs-12 required">
                                                    <label class="control-label">Email</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon glyphicon glyphicon-envelope"></span>
                                                        <input type="text" class="form-control" id="txtEmail" placeholder="provide email address" />
                                                    </div>
                                                </div>
                                            <div class="form-group col-md-6 col-xs-12 required">
                                                    <label class="control-label">Contact person's telefon number</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon glyphicon glyphicon-phone"></span>
                                                        <input type="text" class="form-control" id="txtPhone" placeholder="Provide number" />
                                                    </div>
                                                    <div>Provide area code-number</div>
                                                </div>
                                                <div class="private-fields">
                                                    <div class="form-group col-md-6 col-xs-12">
                                                        <label class="control-label">Personal number</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon glyphicon glyphicon-file"></span>
                                                            <input type="text" class="form-control" id="txtPersonalNumber" placeholder="Social security number" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset class="col-md-12"><legend>Delivery details</legend>
                                                <div class="form-group col-md-6 col-xs-12 required">
                                                    <label class="control-label">Provide delivery address</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon glyphicon glyphicon-map-marker"></span>
                                                        <input type="text" class="form-control" id="txtDeliveryAddress" placeholder="Provide postal adress/email address" />
                                                    </div>
                                                    <div>Delivery address for the translation</div>
                                                </div>
                                            </fieldset>
                                            <div class="private-fields">
                                                <div class="form-group col-md-12">
                                                    If you accept the quote you need to pay the whole sum in advance. Pay via internet bank to bankgiro: 418-3109. When paying please provide the booking number that we have provided. Once the payment has been received then we will start working with your translation.
                                                </div>
                                            </div>
                                            <div class="new-customer-fields">
                                                <fieldset class="col-md-12"><legend>Company details</legend>
                                                    <div class="form-group col-md-6 col-xs-12">
                                                        <label class="control-label">Organisation number</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon glyphicon glyphicon-file"></span>
                                                            <input type="text" class="form-control" id="txtNewCompanyOrganisationNumber" placeholder="Organisation number" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6 col-xs-12 required">
                                                        <label class="control-label">Department, unit or as such</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon glyphicon glyphicon-credit-card"></span>
                                                            <input type="text" class="form-control" id="txtNewTitle" placeholder="Provide name" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6 col-xs-12 required">
                                                        <label class="control-label">Telephone number</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon glyphicon glyphicon-phone"></span>
                                                            <input type="text" class="form-control" id="txtNewCompanyPhone" placeholder="Provide number" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6 col-xs-12 required">
                                                        <label class="control-label">Email</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon glyphicon glyphicon-envelope"></span>
                                                            <input type="text" class="form-control" id="txtNewCompanyEmail" placeholder="Provide email address" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6 col-xs-12">
                                                        <label class="control-label">Fax number</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon glyphicon glyphicon-print"></span>
                                                            <input type="text" class="form-control" id="txtNewCompanyFax" placeholder="Provide number" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6 col-xs-12 required">
                                                        <label class="control-label">Visiting address</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon glyphicon glyphicon-map-marker"></span>
                                                            <input type="text" class="form-control" id="txtNewCompanyVisitingAddress" placeholder="Provide address" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6 col-xs-12 required">
                                                        <label class="control-label">Post number</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon glyphicon glyphicon-inbox"></span>
                                                            <input type="text" class="form-control" id="txtNewCompanyPostNumber" placeholder="Provide post number" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6 col-xs-12 required">
                                                        <label class="control-label">Postal area</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon glyphicon glyphicon-globe"></span>
                                                            <input type="text" class="form-control" id="txtNewCompanyCity" placeholder="Provide name" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6 col-xs-12">
                                                        <label class="control-label">Country/Land</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon glyphicon glyphicon-picture"></span>
                                                            <input type="text" class="form-control" id="txtNewCompanyCountry" placeholder="Company country" />
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <fieldset class="col-md-12"><legend>Billing information</legend>
                                                    <div class="form-group col-md-6 col-xs-12 required">
                                                        <label class="control-label">Postal address/Box</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon glyphicon glyphicon-map-marker"></span>
                                                            <input type="text" class="form-control" id="txtNewBillingAddress" placeholder="Provide name/box" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6 col-xs-12 required">
                                                        <label class="control-label">Post number</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon glyphicon glyphicon-inbox"></span>
                                                            <input type="text" class="form-control" id="txtNewBillingPostNumber" placeholder="Provide post number" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6 col-xs-12 required">
                                                        <label class="control-label">Postal area</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon glyphicon glyphicon-globe"></span>
                                                            <input type="text" class="form-control" id="txtNewBillingCity" placeholder="Provide name" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6 col-xs-12">
                                                        <label class="control-label">Country</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon glyphicon glyphicon-picture"></span>
                                                            <input type="text" class="form-control" id="txtNewBillingCountry" placeholder="Billing country" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6 col-xs-12 required">
                                                        <label class="control-label">Reference/Cost center</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon glyphicon glyphicon-duplicate"></span>
                                                            <input type="text" class="form-control" id="txtNewBillingReference" placeholder="Billing reference" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6 col-xs-12 required">
                                                        <label class="control-label">Billing receiver</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon glyphicon glyphicon-user"></span>
                                                            <input type="text" class="form-control" id="txtNewBillingRecepient" placeholder="Provide name" />
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <div class="error" style="display: none;" id="lblErrorMsg">Fill in all required data</div>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary" id="confirmOrder">Confirm order</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="orderReceived" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong>Tack för din beställning!</strong>
                                </div>
                                <div class="modal-body center-block">
                                    Språkservice har mottagit din order.
                                    <br />
                                    <br />
                                    <em>Med vänlig hälsning</em><br />
                                    <em>Språkservice Sverige AB</em>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <asp:HyperLink type="button" runat="server" NavigateUrl="~/sv/full-quote" Target="_blank" ID="download_quotes_btn" CssClass="download_quotes_btn btn btn-block">View Full Quote</asp:HyperLink>
                    <div class="modal fade" id="cantViewOrder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">You can't see the complete order</h4>
                                </div>
                                <div class="modal-body center-block">
                                    You need to add some items and choose languages for translation before you can complete the order
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        
        <script src="<%=ResolveClientUrl("~/usercontrols/InstantQuotation/localization.js") %>"></script>
        <script type="text/javascript">
            var lang = eng;
        </script>
        <script src="<%=ResolveClientUrl("~/usercontrols/InstantQuotation/mammoth.browser.js") %>"></script>
        <script src="<%=ResolveClientUrl("~/usercontrols/InstantQuotation/jszip.js") %>"></script>
        <script src="<%=ResolveClientUrl("~/usercontrols/InstantQuotation/xlsx.js") %>"></script>
        <script src="<%=ResolveClientUrl("~/usercontrols/InstantQuotation/jquery.bootstrap.wizard.js") %>"></script>
        <script src="<%=ResolveClientUrl("~/usercontrols/InstantQuotation/bootstrap-select.js") %>"></script>
        <script src="<%=ResolveClientUrl("~/usercontrols/InstantQuotation/bootstrap-datepicker.min.js") %>"></script>
        <script src="<%=ResolveClientUrl("~/usercontrols/InstantQuotation/moment.min.js") %>"></script>
        <script src="<%=ResolveClientUrl("~/usercontrols/InstantQuotation/moment-business.min.js") %>"></script>
        <script src="<%=ResolveClientUrl("~/usercontrols/InstantQuotation/jquery.cookie.js") %>"></script>
        <script src="<%=ResolveClientUrl("~/usercontrols/InstantQuotation/jquery.ui.widget.js") %>"></script>
        <script src="<%=ResolveClientUrl("~/usercontrols/InstantQuotation/jquery.iframe-transport.js") %>"></script>
        <script src="<%=ResolveClientUrl("~/usercontrols/InstantQuotation/jquery.fileupload.js") %>"></script>
        <script src="<%=ResolveClientUrl("~/usercontrols/InstantQuotation/iq_app.js") %>"></script>
    </div>
</div>
