﻿using System;
using System.Web;
using System.Web.Script.Serialization;
using AccentWebpage.Controllers;
using AccentWebpage.ViewModels;
//using UmbracoProject.umbraco.HSense.IQ.BsinessLogic;

namespace HSense.IQ.Macro
{
    public partial class FullQuoteReview : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var httpCookie = Request.Cookies["order"];
            if (httpCookie != null)
            {
                if (httpCookie.Value != null)
                {
                    var obj =
                        new JavaScriptSerializer().Deserialize<QuoteJsonController.ConfirmOrderViewModel>(
                            HttpUtility.UrlDecode(httpCookie.Value));

                    var content = EmailSending.GetEmailContent(obj.Order, null, obj.Language);
                    Response.Write(content);
                }
                else
                {
                    Response.Write("Order cookie value is Null");
                }
            }
            else
            {
                Response.Write("Order cookie is Null. Available cookies are: " + string.Join(",\n", Request.Cookies.AllKeys));
            }
        }
    }
}