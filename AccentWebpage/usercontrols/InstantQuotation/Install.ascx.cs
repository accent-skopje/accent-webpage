﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;

namespace HSense.IQ.Macro
{
    public partial class Install : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnExecuteSql_OnClick(object sender, EventArgs e)
        {

            try
            {

                string sqlConnectionString = ConfigurationManager.AppSettings["umbracoDbDSN"];
                string script;
                // Object for stream reader
                StreamReader srFile = default(StreamReader);
                // Creates a instance of the script file
                srFile = File.OpenText(Server.MapPath(@"~\usercontrols\InstantQuotation\InstantQuotation.sql"));
                // sets the text as string
                script = srFile.ReadToEnd();
                srFile.Close();

                // Executes the script at the server
                SqlConnection conn = new SqlConnection(sqlConnectionString);
                Server server = new Server(new ServerConnection(conn));
                server.ConnectionContext.ExecuteNonQuery(script);
                //server.ConnectionContext.ExecuteReader("SELECT count(0) from hsenseIqLanguages");

                lblMsg.Text = "successfull";
            }
            catch (Exception exc)
            {
                lblMsg.Text = "error :" + exc.Message.ToString();
            }
        }
    }
}