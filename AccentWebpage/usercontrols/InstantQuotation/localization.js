﻿var swe =
{
    language: "swe",
    "estimated-days": "Översättningen beräknas vara klar om {0} arbetsdag(ar)",
    "well-contact-you": "Vi kommer att kontakta dig angående leveransdatum",
    "sek-word": "SEK/ord",
    "words": "ord",
    "including-vat": "inkl. moms",
    "excluding-vat": "exkl. moms",
    dateformat: "DD/MM/YYYY"
};

var eng =
{
    language: "eng",
    "estimated-days": "The translation will be ready in {0} working days latest",
    "well-contact-you": "We will be in Contact with you about a delivery date",
    "sek-word": "SEK/word",
    "words": "words",
    "including-vat": "incl. VAT",
    "excluding-vat": "excl. VAT",
    dateformat: "MM/DD/YYYY"
};

/*eng["estimated-days"] = "The translation will be ready in {0} working days latest";
swe["estimated-days"] = "Översättningen kommer att vara klar under {0} arbetsdagar senaste";

eng["well-contact-you"] = "We will be in Contact with you about a delivery date";
swe["well-contact-you"] = "Vi kommer att vara i kontakt med dig om ett leveransdatum";

eng["sek-word"] = "SEK/word";
swe["sek-word"] = "SEK/ord";

eng["words"] = "words";
swe["words"] = "ord";

eng["typed-text"] = "Typed text";
swe["typed-text"] = "Inskriven text";

eng["word-count"] = "Word count";
swe["word-count"] = "Ordantal";*/