﻿<%@ Control Language="C#" AutoEventWireup="true" %>
<style type="text/css">
    		.termsContainer{
    			width: 700px !important;
				font: 12px Helvetica, Arial, sans-serif !important;
    		}

            .termsContainer h1 {
                font: bold 24px Arial, sans-serif !important;
            }
    		.termsContainer h2 {
    			line-height: 0.5em !important;
    		    text-transform: none !important;
                font: bold 18px Arial, sans-serif !important;
    		    margin: 0 !important;
    		}
    		.termsContainer p{
    			margin-bottom: 2em !important;
    		}
        </style>
<div class="termsContainer">
<h1>ALLMÄNNA VILLKOR ÖVERSÄTTNINGSTJÄNSTER 2015</h1>

<p>
	Uppdragsavtal ingås genom att Språkservice Sverige AB bekräftar uppdraget. I den mån uppdrag påbörjas eller utförs utan skriftligt avtal eller orderbekräftelse, gäller dessa villkor i tillämpliga delar.
</p>

<h2>Leveranstider</h2>
<p>Texter upp till 4000 ord - 1 arbetsvecka. Längre texter enligt överenskommelse.</p>

<h2>Expressleveranser</h2>
<p>För texter upp till 500 ord med leverans samma dag +200 %
<br />
För texter upp till 2000 ord med lev. inom 2 arbetsdagar +100 %
<br />
För texter upp till 4000 ord inom 3 arbetsdagar + 50 %
<br />
Längre texter enligt överenskommelse</p>

<h2>Rätt till förlängd leveranstid</h2>
<p>Omständigheter som Språkservice Sverige AB inte kan råda över, till exempel nätverkshaveri eller avbrott

i telekommunikation, ger Språkservice Sverige AB rätt till förlängning av leveranstid.</p>

<h2>Minimidebitering</h2>
<p>För samtliga våra uppdrag gäller en minimidebitering. Vid tidsdebitering är den 1 timme. Sker i stället 

debitering per ord framgår minimidebiteringen av vår offert/avtal.</p>

<h2>Tillägg/Ändringar</h2>
<p>Eventuella ändringar, rättningar och tillägg till ursprungstexten vilka kommer Språkservice Sverige AB

tillhanda efter det att översättningen är påbörjad debiteras extra. Om kund ändrar förutsättningarna för 

uppdraget under uppdragets gång har Språkservice rätt att justera leveransdag och pris.</p>

<h2>Volymberäkning</h2>
<p>Volymberäkning baseras på ordantalet i källtexten det vill säga den text kunden tillhandahåller, om inte

annat har överenskommits, eller om antalet ord i källtexten inte går att räkna maskinellt. 

Alla ord och sifferkombinationer räknas som ord.</p>

<h2>Kvalitetsgaranti</h2>
<p>Språkservice Sverige AB ansvarar för att kvaliteten på översättningsuppdraget står i överensstämmelse

med god översättarsed. Om kunden anvisat eller tillhandahållit ordlistor, övriga uppslagsverk samt 

bilder, ska underleverantören använda sig av dessa.</p>

<h2>Säkerhet/Sekretess</h2>
<p>Språkservice Sverige AB förbinder sig att förvara samtliga dokument och annat material som företaget 

erhåller för översättning under sådana former att det inte kan komma obehöriga tillhanda.</p>

<h2>Referensexemplar/Kolofon</h2>
<p>Beställaren förbinder sig att till Språkservice Sverige ABs referensbibliotek översända ett exemplar av 

den översatta och tryckta texten. I material som trycks skall kolofon enligt nedanstående finnas i 

inlagan: Översatt av www.sprakservice.se</p>

<h2>Upphovsrätt</h2>
<p>När betalningen kommit Språkservice Sverige AB tillhanda, har kunden upphovsrätt till den levererade 

översättningen. Språkservice Sverige AB har dock rätt att fritt och utan ersättning förfoga över ordlistor 

som upprättats under och på grund av uppdraget.</p>

<h2>Avbrytande</h2>
<p>Skulle uppdraget bli avbrutet, försenat eller på annat sätt uppehållet av skäl vilka ligger utanför

Språkservice Sverige ABs kontroll debiteras detta i enlighet med de faktiska kostnader som uppstått av 

denna anledning.
<br />
Dessutom förbehåller sig Språkservice Sverige AB rätten att debitera minimum 50 % av resterande 

offererat arvode såsom skadestånd. Skulle uppdragets påbörjande bli fördröjt, efter det att det bokats 

och tidsmässigt inplanerats hos Språkservice Sverige ABs översättare, förbehåller sig Språkservice 

Sverige AB rätten att debitera för de faktiska kostnader som därvid uppstått.</p>

<h2>Pris</h2>
<p>Om fast pris skriftligen överenskommits eller orderbekräftats av Språkservice Sverige AB gäller detta pris 

om inte annat framgår av dessa allmänna villkor. Visar sig förutsättningarna felaktiga eller de ändras är 

inte Språkservice Sverige AB bundet av det fasta priset.
<br />
Språkservice uppdrag utförs ibland helt eller delvis utanför Sverige. Om kursen för det aktuella landets 

valuta i förhållande till den svenska kronan skulle ändras med mer än 4 % från uppdragets ingående till 

fakturering har Språkservice Sverige AB rätt att justera fakturabeloppet i motsvarande mån som hela 

valutaändringen.</p>

<h2>Betalningsvillkor</h2>
<p>Språkservice Sverige AB äger rätt att begära att 50 % av överenskommet eller uppskattat pris erläggs a

conto före uppdragets igångsättande. Betalning ska ske 10 dagars netto från fakturans utställande. Efter

förfallodatumet debiteras dröjsmålsränta enligt gällande bestämmelser i räntelagen tills dess att full 

betalning skett.</p>

<h2>Mervärdeskatt</h2>
<p>Mervärdesskatt tillkommer på samtliga priser om inget annat anges.</p>

<h2>Reklamationer</h2>
<p>Reklamationer av översättningar ska komma Språkservice Sverige AB tillhanda genast, dock senast 10 

dagar efter materialets översändande.</p>

<h2>Ansvarsbegränsning</h2>
<p>Vid fel eller brist är Språkservice Sverige AB och dess underleverantörers ansvar begränsat till utbyte och

rättad översättning. Detta ansvar gäller dock endast under förutsättning att den felaktiga översättningen 

returneras till Språkservice Sverige AB för granskning och kontroll av reklamationen inom 

reklamationstiden angivet i offerten eller i dessa villkor. Språkservice Sverige AB och dess 

underleverantörers ansvar är under alla förhållanden begränsat till det belopp som motsvarar det 

bekräftade priset för uppdraget.</p>

<h2>Force Majeure</h2>
<p>Språkservice Sverige AB är inte ansvarigt för förlust som kan uppstå om företagets eller dess

underleverantörers skyldighet fördröjs på grund av sjukdom, eldsvåda, trafikolycka, arbetskonflikt eller 

annan liknande oförutsedd omständighet som ligger utanför företagets kontroll.</p>

</div>