﻿$(function () {
    //$('#document').bootstrapFileInput();
    var control = $("#document");
    var filesList = [];
    
    $('#rootwizard').bootstrapWizard({'tabClass': 'nav nav-tabs'});
    
    $('#txtPasteText').focus();
    var filesInList = [];
    $("#document").on("change", handleFileSelect);

    var totalWordCount = 0;
    var sourceLangId = null;
    var destinationIds = [];
    var deliveryPlanId = 0;
    var deliveryPlanName = null;
    //var deliveryDeadlineId = null;
    var deliveryDeadlineName = null;
    var deliveryPlans;
    var deliveryDeadlines;
    var days = 1;
    var order = null;

    $(".exclude-vat").text("(" + lang["excluding-vat"] + ")");
    $(".include-vat").text("(" + lang["including-vat"] + ")");

    $.ajax({
        type: "POST",
        url: "/LanguagesJson/",
        processData: false,
        data: JSON.stringify({ languageId: 0 }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var options = '';
            for (var i = 0; i < response.Languages.length; i++) {
                options += '<option value="' + response.Languages[i].Id + '">' + response.Languages[i].Name + '</option>';
            }
            $("#langFrom").append(options);
            $("#langTo").append(options);
            $('#langFrom').selectpicker();
            $("#langTo").selectpicker();

            //changeSourceLang();
        },
        error: function (e) {
            console.log(e.message);
        }
    });

$.ajax({
            type: "POST",
            url: "/QualityTypesJson/",
            processData: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                deliveryPlans = response;

                //$("#plansTable").find("a").remove();
                $("#plansTable tr").find("td").remove();
            
                var newColumn = "";
                for (var i = 0; i < response.QualityTypes.length; i++) {
                    newColumn = '<td class="list-group-item col-xs-4 remove-padding">\
                                <a href="#" class="" data-id="' + response.QualityTypes[i].Id + '" data-name="' + response.QualityTypes[i].Name + '">\
                                    <div class="package-content-wrapper">\
                                        <h4 class="list-group-item-heading">' + response.QualityTypes[i].Name + '</h4>\
                                        <p class="list-group-item-text">' + response.QualityTypes[i].Description + '</p>\
                                    </div>\
                                </a>\
                            </td>';

                    var $newColumn = $(newColumn);
                    $("#plansTable tr").append($newColumn);
                
                    if (i === 1) {
                        $newColumn.addClass("active");
                        deliveryPlanId = response.QualityTypes[i].Id;
                        deliveryPlanName = response.QualityTypes[i].Name;
                    }
                }

                updateDeliveryPlan();
            },
            error: function (e) {
                console.log(e.message);
            }
        });

    function updateDeliveryDeadlines(words) {
        $.ajax({
            type: "POST",
            url: "/DeliveryDeadlinesJson/",
            processData: false,
            data: JSON.stringify({ words: words }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                deliveryDeadlines = response;
                $("#pricingTable").find("a").remove();

                for (var i = 0; i < deliveryDeadlines.DeliveryDeadlineTypes.length; i++) {
                    var deliveryEstimation =
                        deliveryDeadlines.DeliveryDeadlinePricelists[i].Pricelist.Days ?
                            String.format(lang["estimated-days"], deliveryDeadlines.DeliveryDeadlinePricelists[i].Pricelist.Days) //"The translation will be ready in " + deliveryDeadlines[i].Days + " working days latest"
                            : lang["well-contact-you"];//"We will be in Contact with you about a delivery date";

                    var newRow = '<a href="#" class="list-group-item plan-item" data-id="' + deliveryDeadlines.DeliveryDeadlineTypes[i].Id + '">\
                                <h4 class="list-group-item-heading">' + deliveryDeadlines.DeliveryDeadlineTypes[i].Name + '</h4>\
                                <p class="list-group-item-text">'+ deliveryEstimation + '</p>\
                            </a>';
                    //<h4 class="list-group-item-heading pull-right">' + deliveryDeadlines[i].Value + ' SEK/word</h4>\
                    var $newRow = $(newRow);
                    $("#pricingTable").append($newRow);

                    if (i === 0) {
                        var minDeliveryDays = deliveryDeadlines.DeliveryDeadlinePricelists[i].Pricelist.Days ? deliveryDeadlines.DeliveryDeadlinePricelists[i].Pricelist.Days : 0;
                        var now = moment();
                        //var deadline = now.add(minDeliveryDays, 'day');
                        var deadline = addWeekdays(new Date(), minDeliveryDays);
                        //console.log(deadline);
                        $('.datepicker').datepicker('setStartDate', deadline);
                    }

                    if (i === deliveryDeadlines.DeliveryDeadlineTypes.length - 1) { // always select the last one
                        $newRow.addClass("active");
                        deliveryDeadlineName = deliveryDeadlines.DeliveryDeadlineTypes[i].Name;
                    }
                }
            },
            error: function (e) {
                console.log(e.message);
            }
        });
    }

    updateDeliveryDeadlines(0);

    function updateDeliveryPlan() {
        $("#deliveryPlanName").text(deliveryPlanName);
    }

    $(".btn-order").on("click", function() {
        var deliveryDeadlineId = $(this).data("id");
        deliveryDeadlineName = getDeliveryDeadlineName(deliveryDeadlineId);
        getQuote();
    });

    function getQuote() {
        if (filesInList.length > 0 &&
            sourceLangId &&
            destinationIds.length > 0 &&
            deliveryPlanName &&
            deliveryDeadlineName) {
            var data = {
                DocumentItems: [],
                SourceLangId: sourceLangId,
                DestinationIds: destinationIds,
                DeliveryPlanName: deliveryPlanName,
                DeliveryDeadlineName: deliveryDeadlineName
            };
            //remove the text Content to reduce the payload
            for (var i = 0; i < filesInList.length; i++) {
                var file = filesInList[i];
                data.DocumentItems.push(
                {
                    Name: file.Name,
                    WordCount: file.WordCount,
                    Filetype: file.Filetype,
                    Id: file.Id,
                    Contents: ""
                });
            }
            $.ajax({
                type: "POST",
                url: "/QuoteJson",
                traditional: true,
                data: JSON.stringify(data),
                encoding: "UTF-8",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    order = response;
                    //add the Content back
                    order.DocumentItems = filesInList;
                    updateUi(response);

                    var data = {
                        Order: order,
                        Language: lang.language
                };
                    $.cookie("order", JSON.stringify(data), { expires: 1, path: '/' });
                },
                error: function (e) {
                    console.log(e);
                }
            });
        }
    }

    function updateUi(data) {
        // Update ui on Languages selection
        $("#languagesTable tbody").empty();
        $("#target_language_list_render_section").fadeIn();
        var costPerWord = data.CostPerWord;
        var costPerItems = 0;
        for (var i = 0; i < data.DocumentItems.length; i++) {
            var documentItem = data.DocumentItems[i];
            var newLangLine = '';
            var costPerItem = documentItem.WordCount * costPerWord;
            costPerItems += costPerItem;
            if (i == 0) {
                newLangLine =
                    '<tr class="language_line"><td class="col-lan-name">'
                    + documentItem.Name
                    + '</td><td class="col-unit-price">'
                    + documentItem.WordCount
                    + ' '+ lang.words +'</td><td class="col-total-units" rowspan="4">x '
                    + costPerWord + ' ' + lang["sek-word"]
                    + '</td><td class="col-part-price"><span class="part_price_text">'
                    + Number(costPerItem.toFixed(2)).toLocaleString(lang.language)
                    + ' SEK</span></td></tr>';
            } else {
                newLangLine =
                    '<tr class="language_line"><td class="col-lan-name">'
                    + documentItem.Name
                    + '</td><td class="col-unit-price">'
                    + documentItem.WordCount
                    + ' ' + lang["words"]
                    + '</td><td class="col-part-price"><span class="part_price_text">'
                    + Number(costPerItem.toFixed(2)).toLocaleString(lang.language)
                    + ' SEK</span></td></tr>';
            }

            $("#languagesTable tbody").append(newLangLine);
        }

        $(".col-total-units").attr("rowspan", data.DocumentItems.length);
        $(".col-total-units").text("x " + costPerWord + " " + lang["sek-word"]);
        $(".col-total-price").text(Number(costPerItems.toFixed(2)).toLocaleString(lang.language) + " SEK " + "(" + lang["excluding-vat"] + ")");
        $(".col-total-lang strong").text(destinationIds.length);
        $(".col-src-lang strong").text(data.SourceLanguage.Name);

        //Update Oder right bar
        if (data.TotalCost <= 690) {
            $(".lblMinimumOrderPrice").fadeIn();
            //order.TotalCost = data.TotalCost = 690;
        } else {
            $(".lblMinimumOrderPrice").fadeOut();
        }
        $("#orderTotal").text(Number(data.TotalCost.toFixed(2)).toLocaleString(lang.language) + " SEK");
        $("#orderTotalWithVat").text(Number(data.TotalCostWithVat.toFixed(2)).toLocaleString(lang.language) + " SEK");
        var langItem = '';
        $("#languagesList").empty();
        for (var i = 0; i < data.DestinationLanguages.length; i++) {
            var destLang = data.DestinationLanguages[i];
            var destRate;
            for (var j = 0; j < data.RatePricelists.length; j++) {
                if (data.Rates[j].DestinationLanguageId == destLang.Id) {
                    destRate = data.RatePricelists[j];
                    break;
                }
            }

            if (destRate) {
                var langCost = destRate.Cost * data.WordCount;
                langItem =
                    '<li><section>'
                    + destLang.Name
                    + '</section>';
                var price = '<strong class="pull-right">('
                    + destRate.Cost
                    + "x" + data.WordCount + ") "
                    + Number(langCost.toFixed(2)).toLocaleString(lang.language)
                    + ' SEK</strong>';
                price = '';
                langItem += price + '</li>';
                $("#languagesList").append(langItem);
            }
        }

        var deadline = addWeekdays(new Date(), data.DeliveryDeadline.Days);

        var deliveryEstimation =
                        data.DeliveryDeadline.Days ?
                        deadline.getDate() + "/" + (deadline.getMonth() + 1) + "/" + deadline.getFullYear()
                        : lang["well-contact-you"];
        $("#estimatedDelivery").text(deliveryEstimation);
    }

    function addWeekdays(date, days) {
        date.setDate(date.getDate());
        var counter = 0;
        if (days > 0) {
            while (counter < days) {
                date.setDate(date.getDate() + 1); // Add a day to get the date tomorrow
                var check = date.getDay(); // turns the date into a number (0 to 6)
                if (check == 0 || check == 6) {
                    // Do nothing it's the weekend (0=Sun & 6=Sat)
                }
                else {
                    counter++;  // It's a weekday so increase the counter
                }
            }
        }
        return date;
    }

    function handleFileSelect(event) {
        var file = event.target.files[0];
        countWordsFromFile(file);
    }

    $("#btnCalculate").on("click", function () {
        if ($("#textContainer").is(":visible")) {
            var text = $("#txtPasteText").val();
            var wordsArray = getWordsArray(text);
            var firstFive = wordsArray.slice(0, 5);
            var filename = firstFive.join(" ");
            
            wordCount(text, filename, "Typed text");
            $("#textContainer").fadeOut();
            $("#txtPasteText").val("");

            var myBlob = new Blob(["dummy"], { type: "text/plain" });
            filesList.push(myBlob);
            $('#document').fileupload('add', { files: filesList });
        } else {
            var file = $("#document")[0].files[0];
            countWordsFromFile(file);
        }
    });

    $("#btnAddText").on("click", function () {
        if ($("#textContainer").is(":visible")) {
            $("#textContainer").fadeOut();
            $("#txtPasteText").val("");
        } else {
            $("#textContainer").fadeIn();
        }
    });

    $("#btnAddWordCount").on("click", function () {
        if ($("#wordsContainer").is(":visible")) {
            $("#wordsContainer").fadeOut();
            $("#txtNumberOfWords").val("");
        } else {
            $("#wordsContainer").fadeIn();
        }
    });

    $("#btnAddNumber").on("click", function () {
        if ($("#txtNumberOfWords").val() != "") {
            var numberWords = parseInt($("#txtNumberOfWords").val());
            updateList(numberWords, "Word count", "WordCount");
            $("#wordsContainer").fadeOut();
            $("#txtNumberOfWords").val("");
        }
    });

    $("#langFrom").on("change", function () {
        changeSourceLang();
    });

    $("#langTo").on("change", function () {
        destinationIds = $("#langTo").val();

        if (destinationIds.length == 0) {
            $("#target_language_list_render_section").fadeOut();
        } else {
            getQuote();
        }
    });

    $("#plansTable").on("click", "a", function (e) {
        $("#plansTable td").removeClass("active");
        $(this).parent().addClass("active");

        deliveryPlanId = $(this).data("id");
        deliveryPlanName = $(this).data("name");

        updateDeliveryPlan();
        getQuote();

        e.preventDefault();
    });

    $("#pricingTable").on("click", "a", function (e, numDays) {

        $("#pricingTable a").removeClass("active");
        $(this).addClass("active");
        var deliveryDeadlineId = $(this).data("id");
        deliveryDeadlineName = getDeliveryDeadlineName(deliveryDeadlineId);

        getQuote();
        e.preventDefault();
    });

    $("#change_complete_btn").on("click", function (ev) {
        var canViewFullQuote =
            filesInList.length > 0 &&
                sourceLangId &&
                destinationIds.length > 0 &&
                deliveryPlanName &&
                deliveryDeadlineName;
            //deliveryDeadlineId;
        if (!canViewFullQuote) {
            $("#cantViewOrder").modal("show");
            return false;
        }

        var wordCountExist = false;
        for (var i = 0; i < order.DocumentItems.length; i++) {
            var documentItem = order.DocumentItems[i];
            if (documentItem.Filetype == "WordCount") {
                wordCountExist = true;
                break;
            }
        }

        if (!$("#agree_term_chk").is(":checked")) {
            $("#agreeTerms").modal("show");
        } else if (wordCountExist) {
            $("#removeWordCounts").modal("show");
        } else {
            $("#profileData").modal("show");
            $("#orderForm").hide();
        }

    });

    $(".download_quotes_btn").on("click", function (ev) {
        var canViewFullQuote =
            filesInList.length > 0 &&
                sourceLangId &&
                destinationIds.length > 0 &&
                deliveryPlanName &&
                deliveryDeadlineName;
            //deliveryDeadlineId;
        if (!canViewFullQuote) {
            $("#cantViewOrder").modal("show");
            ev.preventDefault();
        }
    });

    function canConfirmTheOrder() {
        var canConfirm = true;
        if (!$("#chbPrivate").is(":checked") && !$("#chbCompany").is(":checked")) {
            return false;
        }

        $("#orderForm").find("input").each(function () {
            if ($(this).parent().parent().is(".required") && $(this).is(":visible") && $(this).val() === "") {
                canConfirm = false;
                return canConfirm;
            }
        });
        return canConfirm;
    }

    $("#document").fileupload({
        dataType: 'json',
        type: "POST",
        url: "/QuoteJson/MakeOrder",
        add: function (e, data) {
            $("#confirmOrder").off("click");
            data.context = $("#confirmOrder").on("click", function () {
                if (order && canConfirmTheOrder()) {
                    $("#lblErrorMsg").hide();
                    $("#confirmOrder").attr("disabled", "disabled");
                    data.formData = getOrderData();
                    data.files = filesList;
                    data.submit();
                } else {
                    $("#lblErrorMsg").show();
                }
            });
        },
        done: function (e, data) {
            $("#confirmOrder").removeAttr("disabled");
            $('#profileData').modal('hide');
            $("#orderReceived").modal("show");
        }
    });

    $(document).on("click", ".document-item-remove", function (ev) {
        console.log("clicked");
        var clickedId = jQuery(this).attr("data-id");

        var itemRemoving = filesInList.filter(function(e) {
            return e.Id == clickedId;
        });

        itemRemoving = itemRemoving[0];
        filesInList.splice(filesInList.indexOf(itemRemoving), 1);
        filesList = filesList.filter(function(e) {
            return e.name != itemRemoving.Name;
        });

        /*filesInList = filesInList.filter(function(e) {
            return e.Id != clickedId;
        });*/

        displayTotalWords(filesInList);
        getQuote();

        //now delete element
        $(this).closest("li").remove();
        ev.preventDefault();
    });

    $(".datepicker").datepicker({
        startDate: new Date(),
        daysOfWeekDisabled: [0, 6],
        todayBtn: true,
        todayHighlight: true
    }).on("changeDate", function (e) {
        var today = moment();
        var selectedDate = moment(e.date);
        days = selectedDate.diff(today, "days") + 1;
        var selectedDeliveryDeadline = null;
        for (var i = 0; i < deliveryDeadlines.DeliveryDeadlineTypes.length; i++) {

            if (i < deliveryDeadlines.DeliveryDeadlineTypes.length - 1) {
                var currentDeliveryDeadline = deliveryDeadlines.DeliveryDeadlineTypes[i];
                var nextDeliveryDeadline = deliveryDeadlines.DeliveryDeadlineTypes[i + 1];
                if (currentDeliveryDeadline.Days <= days && days < nextDeliveryDeadline.Days) {
                    selectedDeliveryDeadline = deliveryDeadlines.DeliveryDeadlineTypes[i];
                    break;
                }
            } else {
                selectedDeliveryDeadline = deliveryDeadlines.DeliveryDeadlineTypes[i];
            }
        }

        if (selectedDeliveryDeadline) {
            $("#pricingTable").find("[data-id='" + selectedDeliveryDeadline.Id + "']").trigger("click", days);
        }
    });

    $("#chbPrivate").on("change", function () {
        var isThisChecked = $(this).is(":checked");
        $("#chbCompany").prop("checked", !isThisChecked);
        $("#orderForm").show();
        if (isThisChecked) {
            $(".private-fields").show();
            $(".company-fields").hide();
            $(".new-customer-fields").hide();
            $("#chbNew").prop("checked", false);
            $(".back-action").show();
            $(".company-picker").hide();
        } else {
            $(".private-fields").hide();
            $(".company-fields").show();
        }
    });

    $("#chbCompany").on("change", function () {
        var isThisChecked = $(this).is(":checked");
        $("#chbPrivate").prop("checked", !isThisChecked);
        $("#orderForm").show();
        if (isThisChecked) {
            $(".company-fields").show();
            $(".private-fields").hide();
            $(".back-action").show();
            $(".private-picker").hide();
        } else {
            $(".company-fields").hide();
            $(".private-fields").show();
        }
    });

    $("#btnBack").on("click", function(e) {
        $(".back-action").hide();
        $("#chbPrivate").prop("checked", false);
        $("#chbCompany").prop("checked", false);
        $(".private-picker").show();
        $(".company-picker").show();
        $("#orderForm").hide();
        e.preventDefault();
    });

    $("#chbNew").on("change", function() {
        var isThisChecked = $(this).is(":checked");
        if (isThisChecked) {
            $(".new-customer-fields").show();
            $(".customer-number").hide();
        } else {
            $(".new-customer-fields").hide();
            $(".customer-number").show();
        }
    });

    /*$("#orderForm input").on("blur", function() {
        console.log("Finished editing: " + $(this).id);
    });*/

    function countWordsFromFile(file) {
        var filetype = getFileExtension(file.name);
        if (filetype == 'docx') {
            countWordsFromDocx(file);
            filesList.push(file);
        }
        if (filetype == 'xlsx') {
            countWordsFromXslx(file);
            filesList.push(file);
        }
        if (filetype == 'txt') {
            countWordsFromTxt(file);
            filesList.push(file);
        }
    }

    function countWordsFromDocx(file) {
        readFileInputEventAsArrayBuffer(file, function (arrayBuffer) {
            mammoth.extractRawText({ arrayBuffer: arrayBuffer })
                .then(function (result) {
                    var filename = file.name;
                    wordCount(result.value, filename, "Word");
                })
                .done();
        });
    }

    function countWordsFromXslx(file) {
        readFileInputEventAsArrayBuffer(file, function (arrayBuffer) {
            var wb = XLSX.read(arrayBuffer, { type: 'buffer' });
            var text = "";
            for (var i = 0; i < wb.Strings.length; i++) {
                text += " " + wb.Strings[i].t;
            }
            var filename = file.name;
            wordCount(text, filename, "Excel");
        });
    }

    function countWordsFromTxt(file) {
        readFileInputEventAsString(file, function (text) {
            var filename = file.name;
            wordCount(text, filename, "Text");
        });
    }

    function wordCount(text, filename, fileType) {
        var wordsArray = getWordsArray(text);
        var words = wordsArray.length;
        
        updateList(words, filename, fileType, fileType == "Typed text" ? text : "");
    }

    function getWordsArray(text) {
        var regex = /\s+/gi;
        var wordsArray = text.trim().replace(regex, ' ').split(' ');
        return wordsArray;
    }

    function updateList(words, filename, fileType, text) {
        var timestamp = new Date().getUTCMilliseconds();
        $("#files").append('<li class="list-group-item"><a href="#" data-id="' + timestamp + '" class="document-item-remove pull-right"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></a><span class="badge">' + words + '</span>' + filename + '</li>');
        filesInList.push({ Name: filename, WordCount: words, Filetype: fileType, Id: timestamp, Contents: text ? text : "" });
        if($("#textContainer").is(":visible")){
            $("#textContainer").fadeOut();
            $("#txtPasteText").val("");
        }
        displayTotalWords(filesInList);
        //alert(filesInList[0].WordCount);

        getQuote();
    }

    function getFileExtension(filename) {
        return filename.split('.').pop();
    }

    function displayTotalWords(listOfFiles) {
        var total = 0;
        var numberItems = listOfFiles.length;
        for (var i = 0; i < numberItems; i++) {
            total += listOfFiles[i].WordCount;
        };
        $("#totalWordCount").text(total);
        $("#numberItems").text(numberItems);
        totalWordCount = total;
        updateDeliveryDeadlines(totalWordCount);
    }

    function readFileInputEventAsArrayBuffer(file, callback) {
        var file = file;

        var reader = new FileReader();

        reader.onload = function (loadEvent) {
            var arrayBuffer = loadEvent.target.result;
            callback(arrayBuffer);
        };

        reader.readAsArrayBuffer(file);
    }

    function readFileInputEventAsString(file, callback) {
        var file = file;

        var reader = new FileReader();

        reader.onload = function (loadEvent) {
            var text = loadEvent.target.result;
            callback(text);
        };

        reader.readAsText(file);
    }

    function escapeHtml(value) {
        return value
            .replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
    }

    function changeSourceLang() {
        sourceLangId = $("#langFrom").val();
        if (sourceLangId.constructor === Array) {
            sourceLangId = sourceLangId[0];
        }
        $('#langTo').attr("disabled", "disabled");
        getSupportedLanguages(sourceLangId);
        //getQuote();
    }

    function getSupportedLanguages(sourceLangId) {
        $.ajax({
            type: "GET",
            url: "/LanguagesJson/GetLanguages",
            async: false,
            data: { id: sourceLangId },
            //processData: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var options = '';
                for (var i = 0; i < response.Languages.length; i++) {
                    options += '<option value="' + response.Languages[i].Id + '">' + response.Languages[i].Name + '</option>';
                }

                $("#langTo")
                    .find("option")
                    .remove()
                    .end()
                    .append(options);
                $('#langTo').removeAttr("disabled");
                $("#langTo").selectpicker("refresh");
            },
            error: function (e) {
                console.log(e.message);
            }
        });
    }

    function getOrderData() {
        var contactPerson = {
            FirstName: $("#txtFirstName").val(),
            LastName: $("#txtLastName").val(),
            Email: $("#txtEmail").val(),
            PhoneNumber: $("#txtPhone").val(),
            PersonalNumber: ""//$("#txtPersonalNumber").val()
        };

        var customerType = {
            IsPrivate: $("#chbPrivate").is(":checked"),
            IsCompany: $("#chbCompany").is(":checked"),
            IsNewCustomer: $("#chbNew").is(":checked")
        };

        var billingInformation = {
            Address: $("#txtDeliveryAddress").val()
        };

        var subscription = {
            CompanyName: "",//$("#txtCompanyName").val(),
            CustomerNumber: $("#txtCustomerNumber").val()
        };

        var newCompanyInformation = {
            Title: $("#txtNewTitle").val(),
            Phone: $("#txtNewCompanyPhone").val(),
            Email: $("#txtNewCompanyEmail").val(),
            Fax: $("#txtNewCompanyFax").val(),
            OrganisationNumber: $("#txtNewCompanyOrganisationNumber").val(),
            ZipCode: $("#txtNewCompanyPostNumber").val(),
            City: $("#txtNewCompanyCity").val(),
            VisitingAddress: $("#txtNewCompanyVisitingAddress").val(),
            Country: $("#txtNewCompanyCountry").val()
        };

        var newBillingInformation = {
            Address: $("#txtNewBillingAddress").val(),
            ZipCode: $("#txtNewBillingPostNumber").val(),
            City: $("#txtNewBillingCity").val(),
            Country: $("#txtNewBillingCountry").val(),
            Reference: $("#txtNewBillingReference").val(),
            Recepient: $("#txtNewBillingRecepient").val()
        };
        var newCustomerData = {
            CompanyInformation: newCompanyInformation,
            BillingInformation: newBillingInformation
        };
        var profileData = {
            CustomerType: customerType,
            Subscription: subscription,
            ContactPerson: contactPerson,
            BillingInformation: billingInformation,
            NewCustomer: newCustomerData
        };

        var confirmOrder = {
            Order: order,
            RecepientProfile: profileData,
            Language : lang.language
        };

        return { confirmOrder: encodeURIComponent(JSON.stringify(confirmOrder)) };
    }

    function getDeliveryDeadlineName(deliveryDeadlineId) {
        for (var i = 0; i < deliveryDeadlines.DeliveryDeadlineTypes.length; i++) {
            var deadline = deliveryDeadlines.DeliveryDeadlineTypes[i];
            if (deadline.Id == deliveryDeadlineId) {
                return deadline.Name;
            }
        }
        return "";
    }

    String.format = function () {
        // The string containing the format items (e.g. "{0}")
        // will and always has to be the first argument.
        var theString = arguments[0];

        // start with the second argument (i = 1)
        for (var i = 1; i < arguments.length; i++) {
            // "gm" = RegEx options for Global search (more than one instance)
            // and for Multiline search
            var regEx = new RegExp("\\{" + (i - 1) + "\\}", "gm");
            theString = theString.replace(regEx, arguments[i]);
        }

        return theString;
    }

    //$("#document").fileupload({
    //    dataType: 'json',
    //    type: "POST",
    //    url: "/QuoteJson/ShowPDF",
    //    add: function (e, data) {
    //        //$("#print").off("click");
    //        data.context = $("#print").on("click", function () {
    //            if (order) {
    //                $("#lblErrorMsg").hide();
    //                //$("#print").attr("disabled", "disabled");
    //                data.formData = getOrderData();
    //                data.files = filesList;
    //                data.submit();
    //            } else {
    //                $("#lblErrorMsg").show();
    //            }
    //        });
    //    },
    //    done: function (e, data) {
    //        $("#print").removeAttr("disabled");
    //        $('#profileData').modal('hide');
    //        $("#orderReceived").modal("show");
    //    }
    //});

    $("#print").on("click", function (ev) {
        var canViewFullQuote =
            filesInList.length > 0 &&
                sourceLangId &&
                destinationIds.length > 0 &&
                deliveryPlanName &&
                deliveryDeadlineName;
        //deliveryDeadlineId;
        if (!canViewFullQuote) {
            $("#cantViewOrder").modal("show");
            ev.preventDefault();
        }

        var data = {
            DocumentItems: [],
            SourceLangId: sourceLangId,
            DestinationIds: destinationIds,
            DeliveryPlanName: deliveryPlanName,
            DeliveryDeadlineName: deliveryDeadlineName
        };
        //remove the text Content to reduce the payload
        for (var i = 0; i < filesInList.length; i++) {
            var file = filesInList[i];
            data.DocumentItems.push(
            {
                Name: file.Name,
                WordCount: file.WordCount,
                Filetype: file.Filetype,
                Id: file.Id,
                Contents: ""
            });
        }

        $.ajax({
            type: 'POST',
            url: '/QuoteJson/Print',
            dataType: "json",
            data: data,
            //data: JSON.stringify(data),
            success: function (data) {
                if (data.toString() == "succes") {
                    $(".featured").removeClass("dim");
                    $("#state").removeClass("show");
                    var url = addhttp(window.location.host) + "/PDFs/Order.pdf";
                    window.open(url);
                } else if (data.toString() == "false") {
                    alert("Print is not possible");
                }
                else {
                    alert(data);
                }
            }
        });
    });

    function addhttp(url) {
        if (!/^(f|ht)tps?:\/\//i.test(url)) {
            url = "http://" + url;
        }
        return url;
    }
});