<!-- Latest compiled and minified CSS -->
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"> -->

<!-- Optional theme -->
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css"> -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="../usercontrols/styles/bootstrap.css">
<link rel="stylesheet" type="text/css" href="../usercontrols/styles/bootstrap-select.css">
<link rel="stylesheet" type="text/css" href="../usercontrols/styles/style.css">

<div>
    <div class="container banner">
        <h1><a href="/">Sprakservice</a></h1>
    </div>
</div>
<div class="container">
    <div class="row">


        <!-- <div class="row">
        <input id="document" type="file" class="col-xs-12 col-md-8" />
        <input type="button" class="btn btn-primary pull-right" id="btnCalculate" value="Calculate" />
      </div> -->

        <div id="rootwizard" class="tabbable col-md-8 col-xs-12">

            <ul>
                <li><a href="#tab1" data-toggle="tab">Text</a></li>
                <li><a href="#tab2" data-toggle="tab">Languages</a></li>
                <li><a href="#tab3" data-toggle="tab">Options</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane" id="tab1">
                    <div class="panel panel-default">
                        <div class="panel-heading">Add text to be translated</div>
                        <div class="panel-body">
                            <ul id="files" class="list-group"></ul>
                            <div id="textContainer">
                                <textarea class="form-control" id="txtPasteText" rows="15"></textarea>
                                <span class="input-group-btn">
                                    <input type="button" class="btn btn-link" id="btnCalculate" value="Calculate" />
                                </span>
                            </div>

                            <div class="input-group col-xs-6 col-md-6 pull-right" id="wordsContainer" style="display: none;">
                                <input type="text" class="form-control" id="txtNumberOfWords" placeholder="Number of words">
                                <span class="input-group-btn">
                                    <input type="button" class="btn btn-default" id="btnAddNumber" value="Add" />
                                </span>
                            </div>

                            <div class="row btn-group pull-right" role="group" style="margin: 10px 0px;">
                                <span class="btn btn-default fileinput-button">
                                    <span>Add File</span>
                                    <!-- The file input field used as target for the file upload widget -->
                                    <input id="document" type="file" name="files[]" multiple="">
                                </span>

                                <input type="button" class="btn btn-default" id="btnAddWordCount" value="Add Word Count" />
                                <input type="button" class="btn btn-default" id="btnAddText" value="Paste Text" />
                                <!-- <input type="button" class="btn btn-primary" id="btnCalculate" value="Calculate" /> -->
                            </div>
                        </div>
                        <small class="text-muted">Following file types are supported .docx, .xlsx and .txt</small>
                    </div>
                </div>



                <div class="tab-pane" id="tab2">




                    <div class="panel panel-default">
                        <div class="panel-heading">Choose languages for translation</div>
                        <div class="panel-body">
                            <div style="padding: 24px 0 22px 35px;">
                                <div class="row col-md-6 col-xs-12">
                                    <h3>Translate from</h3>
                                    <select id="langFrom"  data-style="btn-default">
                                        <option value="0" selected>Select a Language</option>
                                    </select>
                                </div>
                                <div class="row col-md-6 col-xs-12">
                                    <h3>Translate to</h3>
                                    <select id="langTo"  data-style="btn-default">
                                        <option value="0">Select a Language</option>
                                    </select>
                                </div>
                                <div class="row col-md-12 col-xs-12" style="margin-top: 15px;">
                                    <div id="target_language_list_render_section" style="display: none;">
                                        <div id="target_language_list_section">
                                            <table class="table" id="languagesTable">
                                                <tbody>
                                                </tbody>
                                                <tfoot id="language_summary_render_section">
                                                    <tr id="language_summary_section">
                                                        <th class="col-total-lang" colspan="3"><span>Choose</span> Languages</th>
                                                        <th class="col-total-price"></th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab3">
                    <div class="panel panel-default">
                        <div class="panel-heading">Options for translation</div>
                        <div class="panel-body">
                            <div style="padding: 24px 0 22px 35px;">
                                <div class="row col-md-12 col-xs-12">
                                    <h3>Choose document type:</h3>
                                    <select id="documentType" data-style="btn-default">
                                        <option value="0">Select a Subject</option>
                                    </select>
                                </div>
                                <div class="row col-md-12 col-xs-12">
                                    <h3>Choose plan:</h3>
                                    <div class="row">
                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                                            <!-- PRICE ITEM -->
                                            <div class="panel price panel-green">
                                                <div class="panel-heading arrow_box text-center">
                                                    <h4>EARLY PLAN</h4>
                                                </div>
                                                <div class="panel-body text-center">
                                                    <p class="lead" style="font-size: 25px"><strong>$0.12 / word</strong></p>
                                                </div>
                                                <ul class="list-group list-group-flush text-center">
                                                    <li class="list-group-item"><i class="icon-ok text-success"></i>Earler than 4 days</li>
                                                </ul>
                                                <div class="panel-footer">
                                                    <a class="btn btn-lg btn-block btn-success" href="#">BUY NOW!</a>
                                                </div>
                                            </div>
                                            <!-- /PRICE ITEM -->
                                        </div>

                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                                            <!-- PRICE ITEM -->
                                            <div class="panel price panel-grey">
                                                <div class="panel-heading arrow_box text-center">
                                                    <h4>LATE PLAN</h4>
                                                </div>
                                                <div class="panel-body text-center">
                                                    <p class="lead" style="font-size: 25px"><strong>$0.06 / word</strong></p>
                                                </div>
                                                <ul class="list-group list-group-flush text-center">
                                                    <li class="list-group-item"><i class="icon-ok text-success"></i>Later than 4 days</li>
                                                </ul>
                                                <div class="panel-footer">
                                                    <a class="btn btn-lg btn-block btn-primary" href="#">BUY NOW!</a>
                                                </div>
                                            </div>
                                            <!-- /PRICE ITEM -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="pager wizard">
                    <li class="previous first" style="display: none;"><a href="#">First</a></li>
                    <li class="previous"><a href="#">Previous</a></li>
                    <li class="next last" style="display: none;"><a href="#">Last</a></li>
                    <li class="next"><a href="#">Next</a></li>
                </ul>
            </div>
        </div>

        <div id="quote_section" class="col-md-4 col-xs-12">
            <div id="quote_summary_section">
                <h3>Your Order</h3>
                <table class="table quote-table">
                    <tbody>
                        <tr class="adding_content_row complete clickable">

                            <td class="quote_status_icon">

                                <span class="complete"></span>

                            </td>
                            <td colspan="2">

                                <span id="numberItems">5</span> items
                  
                            </td>
                            <td class="quote-value">


                                <span class="pull-right"><span id="totalWordCount">0</span> words</span>
                            </td>
                        </tr>
                        <tr class="choose_language_row complete clickable">

                            <td class="quote_status_icon">


                                <span class="complete"></span>


                            </td>
                            <td colspan="2">Languages
                  
                            </td>
                            <td class="quote-value col-total-lang">
                                <span class="pull-right">0</span>
                            </td>
                        </tr>
                        <tr class="optional_row complete ">

                            <td class="quote_status_icon">

                                <span class="active"></span>

                            </td>
                            <td colspan="2">Business
                  
                            </td>
                            <td>
                                <span class="pull-right">from $0.12 / word</span>
                            </td>
                        </tr>
                        <tr class="space"></tr>
                    </tbody>
                    <tfoot>


                        <tr>
                            <th class="quote_status_icon">

                                <span class="active"></span>

                            </th>
                            <th colspan="2" class="quote-title">Order total</th>

                            <th>
                                <span class="pull-right">$1,011.96</span>
                            </th>

                        </tr>


                    </tfoot>
                </table>
            </div>


            <p class="eta-text">Estimated delivery <strong>26 hours.</strong> <i class="helper_eta_popover_icon icon-tooltip" data-placement="top"></i></p>


            <label>
                <input id="agree_term_chk" type="checkbox">
                I agree to the <a href="/legal/" target="_blank">Terms &amp; Conditions</a> and <a href="http://translate.gengo.com/translation-quality/" target="_blank">Quality Policy</a>
            </label>


            <button type="button" id="change_complete_btn" class="btn btn-core btn-next-page btn-block">Pay &amp; Confirm Order</button>

            <a type="button" id="download_quotes_btn" class="btn btn-block" href="https://gengo.com/order/mitsumori">View Full Quote</a>


        </div>
    </div>

    <script src="../usercontrols/styles/mammoth.browser.js"></script>
    <script src="../usercontrols/styles/jszip.js"></script>
    <script src="../usercontrols/styles/xlsx.js"></script>
    <script src="../usercontrols/styles/jquery.bootstrap.wizard.js"></script>
    <script src="../usercontrols/styles/bootstrap-select.js"></script>
    <!--<script src="jquery.ui.widget.js"></script>
    <script src="jquery.fileupload.js"></script>
    <script src="bootstrap.file-input.js"></script> -->
    <script src="../usercontrols/styles/demo.js"></script>

    <script language="javascript">
        function valSubmit() {
            var doc = document.forms[0];
            var submit = true;
            if (doc.sourceLanguage.value == "") {
                $('#sourceLanguage').addClass("required");
                submit = false;
            }
            if (doc.targetLanguage.value == "") {
                $('#targetLanguage').addClass("required");
                submit = false;
            }

            if (doc.sourceLanguage.value == doc.targetLanguage.value) {
                alert("please check your language selection");
                submit = false;
            }

            if (doc.deliveryDate.value == "") {
                $('#deliveryDate').addClass("required");
                submit = false;
            }
            if (doc.subjectField.value == "") {
                $('#subjectField').addClass("required");
                submit = false;
            }
            if (doc.wordCount.value == "") {
                $('#wordCount').addClass("required");
                submit = false;
            }
            if (submit) {
                return true;
            } else {
                return false;
            }
        }


        function removeValidation() {
            $('#sourceLanguage').removeClass("required");
            $('#targetLanguage').removeClass("required");
            $('#deliveryDate').removeClass("required");
            $('#subjectField').removeClass("required");
            $('#wordCount').removeClass("required");
        }
    </script>

    <style>
        #instantQuote {
            border: 2px solid grey;
        }

            #instantQuote h2, #instantQuote h3 {
                text-align: center;
            }

            #instantQuote h3 {
                font-style: normal;
                font-size: 16px;
            }

        .required {
            border: 2px solid red !important;
            border-radius: 7px !important;
        }
    </style>

    <script>


        $.ajax({
            type: "POST",
            url: "/Base/quoteAPI/PageLoad",
            //data: { name: "John", location: "Boston" }
            success: function (data) {
                fillData(data);
            }
        });



        function fillData(data) {
            for (var i = 0; i < data.sourceLanguageList.length; i++) {
                $("#langFrom").append($("<option>", {
                    value: data.sourceLanguageList[i].Value,
                    text: data.sourceLanguageList[i].Text
                })).selectpicker("refresh");
            }

            for (var i = 0; i < data.targetLanguageList.length; i++) {
                $("#langTo").append($("<option>", {
                    value: data.targetLanguageList[i].Value,
                    text: data.targetLanguageList[i].Text
                })).selectpicker("refresh");
            }

            for (var i = 0; i < data.subjectList.length; i++) {
                $("#documentType").append($("<option>", {
                    value: data.subjectList[i].Value,
                    text: data.subjectList[i].Text
                })).selectpicker("refresh");
            }
        }




    </script>

</div>
