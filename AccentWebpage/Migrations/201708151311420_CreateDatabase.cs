namespace AccentWebpage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateDatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DeliveryDeadlinePricelists",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DeliveryDeadlineTypeId = c.Int(nullable: false),
                        PricelistId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DeliveryDeadlineTypes", t => t.DeliveryDeadlineTypeId, cascadeDelete: true)
                .ForeignKey("dbo.Pricelists", t => t.PricelistId, cascadeDelete: true)
                .Index(t => t.DeliveryDeadlineTypeId)
                .Index(t => t.PricelistId);
            
            CreateTable(
                "dbo.DeliveryDeadlineTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        OrderOnPage = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Pricelists",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        From = c.Int(nullable: false),
                        To = c.Int(nullable: false),
                        Cost = c.Single(),
                        Rate = c.Single(),
                        Days = c.Short(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EmailRecepients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EmailSettings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LogoUrl = c.String(),
                        IntroText = c.String(),
                        OutroText = c.String(),
                        CompanyName = c.String(),
                        TelephoneNumber = c.String(),
                        VisitingAddress = c.String(),
                        PortalAddress = c.String(),
                        FooterTelephone = c.String(),
                        Fax = c.String(),
                        OrgNr = c.String(),
                        Email = c.String(),
                        Website = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.HeroSections",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 1000),
                        SubTitle = c.String(nullable: false, maxLength: 1000),
                        ButtonTitle = c.String(nullable: false, maxLength: 255),
                        ButtonLink = c.String(nullable: false, maxLength: 255),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Languages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        ISO = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.News",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        ShortDescription = c.String(),
                        Description = c.String(),
                        Url = c.String(),
                        Published = c.Boolean(nullable: false),
                        ImgPath = c.String(),
                        ImgPathFront = c.String(),
                        ImgName = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        InfoText = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OrderRecepients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ReceiverProfileId = c.Int(nullable: false),
                        Email = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ReceiverProfiles", t => t.ReceiverProfileId, cascadeDelete: true)
                .Index(t => t.ReceiverProfileId);
            
            CreateTable(
                "dbo.ReceiverProfiles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Email = c.String(),
                        PhoneNumber = c.String(),
                        CompanyName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.QualityPricelists",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        QualityTypeId = c.Int(nullable: false),
                        PricelistId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Pricelists", t => t.PricelistId, cascadeDelete: true)
                .ForeignKey("dbo.QualityTypes", t => t.QualityTypeId, cascadeDelete: true)
                .Index(t => t.QualityTypeId)
                .Index(t => t.PricelistId);
            
            CreateTable(
                "dbo.QualityTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        OrderOnPage = c.Short(nullable: false),
                        Price = c.Single(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RatePricelists",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PriceListId = c.Int(nullable: false),
                        RateId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Pricelists", t => t.PriceListId, cascadeDelete: true)
                .ForeignKey("dbo.Rates", t => t.RateId, cascadeDelete: true)
                .Index(t => t.PriceListId)
                .Index(t => t.RateId);
            
            CreateTable(
                "dbo.Rates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SourceLanguage = c.Int(nullable: false),
                        DestinationLanguage = c.Int(nullable: false),
                        IsSupported = c.Boolean(nullable: false),
                        DestinationLanguageId_Id = c.Int(),
                        SourceLanguageId_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Languages", t => t.DestinationLanguageId_Id)
                .ForeignKey("dbo.Languages", t => t.SourceLanguageId_Id)
                .Index(t => t.DestinationLanguageId_Id)
                .Index(t => t.SourceLanguageId_Id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.RatePricelists", "RateId", "dbo.Rates");
            DropForeignKey("dbo.Rates", "SourceLanguageId_Id", "dbo.Languages");
            DropForeignKey("dbo.Rates", "DestinationLanguageId_Id", "dbo.Languages");
            DropForeignKey("dbo.RatePricelists", "PriceListId", "dbo.Pricelists");
            DropForeignKey("dbo.QualityPricelists", "QualityTypeId", "dbo.QualityTypes");
            DropForeignKey("dbo.QualityPricelists", "PricelistId", "dbo.Pricelists");
            DropForeignKey("dbo.Orders", "ReceiverProfileId", "dbo.ReceiverProfiles");
            DropForeignKey("dbo.DeliveryDeadlinePricelists", "PricelistId", "dbo.Pricelists");
            DropForeignKey("dbo.DeliveryDeadlinePricelists", "DeliveryDeadlineTypeId", "dbo.DeliveryDeadlineTypes");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Rates", new[] { "SourceLanguageId_Id" });
            DropIndex("dbo.Rates", new[] { "DestinationLanguageId_Id" });
            DropIndex("dbo.RatePricelists", new[] { "RateId" });
            DropIndex("dbo.RatePricelists", new[] { "PriceListId" });
            DropIndex("dbo.QualityPricelists", new[] { "PricelistId" });
            DropIndex("dbo.QualityPricelists", new[] { "QualityTypeId" });
            DropIndex("dbo.Orders", new[] { "ReceiverProfileId" });
            DropIndex("dbo.DeliveryDeadlinePricelists", new[] { "PricelistId" });
            DropIndex("dbo.DeliveryDeadlinePricelists", new[] { "DeliveryDeadlineTypeId" });
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Rates");
            DropTable("dbo.RatePricelists");
            DropTable("dbo.QualityTypes");
            DropTable("dbo.QualityPricelists");
            DropTable("dbo.ReceiverProfiles");
            DropTable("dbo.Orders");
            DropTable("dbo.OrderRecepients");
            DropTable("dbo.News");
            DropTable("dbo.Languages");
            DropTable("dbo.HeroSections");
            DropTable("dbo.EmailSettings");
            DropTable("dbo.EmailRecepients");
            DropTable("dbo.Pricelists");
            DropTable("dbo.DeliveryDeadlineTypes");
            DropTable("dbo.DeliveryDeadlinePricelists");
        }
    }
}
