﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AccentWebpage.Startup))]
namespace AccentWebpage
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
