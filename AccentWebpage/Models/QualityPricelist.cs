﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccentWebpage.Models
{
    public class QualityPricelist
    {
        public int Id { get; set; }

        public QualityType QualityType { get; set; }
        public int QualityTypeId { get; set; }

        public Pricelist Pricelist { get; set; }
        public int PricelistId { get; set; }
    }
}