﻿namespace AccentWebpage.Models
{
    public class Pricelist
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int From { get; set; }

        public int To { get; set; }

        public float? Cost { get; set; }

        public float? Rate { get; set; }

        public short? Days { get; set; }
    }
}