﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AccentWebpage.Models
{
    public class News
    {
        private DateTime? _dateCreated;
        public int Id { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }

        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string Description { get; set; }
        public string Url { get; set; }
        public bool Published { get; set; }
        //public DateTime PostedOn { get; set; }
        //public DateTime? Edited { get; set; }
        public string ImgPath { get; set; }
        public string ImgPathFront { get; set; }
        public string ImgName { get; set; }

        [DataType(DataType.Date)]
        public DateTime DateCreated
        {
            get { return _dateCreated ?? DateTime.Now; }
            set { _dateCreated = value; }
        }

        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string InfoText { get; set; }
    }
}