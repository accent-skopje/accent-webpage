﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AccentWebpage.Models
{
    public class Order
    {
        private DateTime? _dateCreated;
        public int Id { get; set; }

        public ReceiverProfile ReceiverProfile { get; set; }

        [Required]
        public int ReceiverProfileId { get; set; }

        public string Email { get; set; }

        [DataType(DataType.Date)]
        public DateTime DateCreated
        {
            get { return _dateCreated ?? DateTime.Now; }
            set { _dateCreated = value; }
        }
    }
}