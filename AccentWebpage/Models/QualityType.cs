﻿using System.Web.Mvc;

namespace AccentWebpage.Models
{
    [Bind(Exclude = "Id")]
    public class QualityType
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public short OrderOnPage { get; set; }

        public float Price { get; set; }

        public string Description { get; set; }
    }
}