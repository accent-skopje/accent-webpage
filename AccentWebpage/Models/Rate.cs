﻿namespace AccentWebpage.Models
{
    public class Rate
    {
        public int Id { get; set; }
        
        public Language SourceLanguageId { get; set; }

        [IfIsSameLanguageSelected]
        public int SourceLanguage { get; set; }

        public Language DestinationLanguageId { get; set; }
        public int DestinationLanguage { get; set; }

        public bool IsSupported { get; set; }
    }
}