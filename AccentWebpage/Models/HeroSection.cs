﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AccentWebpage.Models
{
    public class HeroSection
    {
        public int Id { get; set; }
        [Required]
        [StringLength(1000)]
        public string Title { get; set; }

        [Required]
        [StringLength(1000)]
        public string SubTitle { get; set; }

        [Required]
        [StringLength(255)]
        public string ButtonTitle { get; set; }

        [Required]
        [StringLength(255)]
        public string ButtonLink { get; set; }
    }
}