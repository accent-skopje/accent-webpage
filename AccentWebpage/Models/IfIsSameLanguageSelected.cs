﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AccentWebpage.Models;

namespace AccentWebpage.Models
{
    public class IfIsSameLanguageSelected : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var language = (Rate) validationContext.ObjectInstance;

            if (language.SourceLanguageId != language.DestinationLanguageId)
            {
                return ValidationResult.Success;
            }

            return new ValidationResult("Can't select same language !");
           
        }
    }
}