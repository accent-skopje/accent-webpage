﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccentWebpage.Models
{
    public class RatePricelist
    {
        public int Id { get; set; }

        public Pricelist Pricelist { get; set; }
        public int PriceListId { get; set; }

        public Rate Rate { get; set; }
        public int RateId { get; set; }
    }
}