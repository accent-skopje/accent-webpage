﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using AccentWebpage.Models;

namespace AccentWebpage.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<DeliveryDeadlinePricelist> DeliveryDeadlinePricelists { get; set; }
        public DbSet<DeliveryDeadlineType> DeliveryDeadlineTypes { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Pricelist> Pricelists { get; set; }
        public DbSet<QualityPricelist> QualityPricelists { get; set; }
        public DbSet<QualityType> QualityTypes { get; set; }
        public DbSet<Rate> Rates { get; set; }
        public DbSet<RatePricelist> RatePricelists { get; set; }
        public DbSet<News> Newses { get; set; }
        public DbSet<HeroSection> HeroSections { get; set; }
        public DbSet<ReceiverProfile> ReceiverProfile { get; set; }
        public DbSet<OrderRecepients> OrderRecepients { get; set; }
        public DbSet<EmailRecepients> EmailRecepients { get; set; }
        public DbSet<EmailSettings> EmailSettings { get; set; }

        public ApplicationDbContext()
            : base("AccentConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}