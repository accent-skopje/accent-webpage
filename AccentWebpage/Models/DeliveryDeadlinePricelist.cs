﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccentWebpage.Models
{
    public class DeliveryDeadlinePricelist
    {
        public int Id { get; set; }

        public DeliveryDeadlineType DeliveryDeadlineType { get; set; }
        public int DeliveryDeadlineTypeId { get; set; }

        public Pricelist Pricelist { get; set; }
        public int PricelistId { get; set; }
    }
}