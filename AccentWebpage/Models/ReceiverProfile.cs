using System.ComponentModel.DataAnnotations.Schema;

namespace AccentWebpage.Models
{
    public class ReceiverProfile
    {
        public int Id { get; set; }

        //[Column(TypeName = "nvarchar(150)")]
        public string FirstName { get; set; }

        //[Column(TypeName = "nvarchar(150)")]
        public string LastName { get; set; }

        //[Column(TypeName = "nvarchar(50)")]
        public string Email { get; set; }

        //[Column(TypeName = "nvarchar(50)")]
        public string PhoneNumber { get; set; }

        //[Column(TypeName = "nvarchar(150)")]
        public string CompanyName { get; set; }
    }
}