﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccentWebpage.Models
{
    public class EmailSettings
    {
        public int Id { get; set; }

        public string LogoUrl { get; set; }

        public string IntroText { get; set; }

        public string OutroText { get; set; }

        public string CompanyName { get; set; }

        public string TelephoneNumber { get; set; }

        public string VisitingAddress { get; set; }

        public string PortalAddress { get; set; }

        public string FooterTelephone { get; set; }

        public string Fax { get; set; }

        public string OrgNr { get; set; }

        public string Email { get; set; }

        public string Website { get; set; }
    }
}