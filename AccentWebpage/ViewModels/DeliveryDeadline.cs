﻿using System;

namespace AccentWebpage.ViewModels
{
    public class DeliveryDeadline : IEntity
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public decimal Value { get; set; }
        public int Days { get; set; }
        public int FromWords { get; set; }
        public int ToWords { get; set; }
        public int OrderNumber { get; set; }
        public int OrderOnPage { get; set; }

        public int Save()
        {
            throw new NotImplementedException();
        }

        public void Delete()
        {
            throw new NotImplementedException();
        }

        //public int Save()
        //{
        //    int newId;
        //    var nameParam = Application.SqlHelper.CreateParameter("@Name", Name);
        //    var idParam = Application.SqlHelper.CreateParameter("@Id", Id);
        //    var valueParam = Application.SqlHelper.CreateParameter("@Value", Value);
        //    var daysParam = Application.SqlHelper.CreateParameter("@Days", Days);
        //    var fromWordsParam = Application.SqlHelper.CreateParameter("@FromWords", FromWords);
        //    var toWordsParam = Application.SqlHelper.CreateParameter("@ToWords", ToWords);
        //    var orderNumberParam = Application.SqlHelper.CreateParameter("@OrderNumber", OrderNumber);
        //    var orderOnPageParam = Application.SqlHelper.CreateParameter("@OrderOnPage", OrderOnPage);

        //    if (Id <= 0)
        //    {
        //        const string sql = @"Insert into hsenseIqDeliveryDeadlines 
        //                                (Name, Value, Days, FromWords, ToWords, OrderNumber, OrderOnPage) 
        //                            OUTPUT Inserted.Id values 
        //                                (@Name, @Value, @Days, @FromWords, @ToWords, @OrderNumber, @OrderOnPage)";
        //        newId = Application.SqlHelper.ExecuteScalar<int>(sql, nameParam, valueParam, daysParam, fromWordsParam,
        //            toWordsParam, orderNumberParam, orderOnPageParam);
        //    }
        //    else
        //    {
        //        const string sql = @"Update hsenseIqDeliveryDeadlines set 
        //                                Name = @Name, 
        //                                Value = @Value, 
        //                                Days = @Days,
        //                                FromWords = @FromWords,
        //                                ToWords = @ToWords,
        //                                OrderOnPage = @OrderOnPage
        //                            where Id = @Id";
        //        Application.SqlHelper.ExecuteNonQuery(sql, nameParam, idParam, valueParam, daysParam, fromWordsParam,
        //            toWordsParam, orderOnPageParam);
        //        newId = Id;
        //    }
        //    return newId;
        //}

        //public void Delete()
        //{
        //    const string sql = "Delete from hsenseIqDeliveryDeadlines where Id = @Id";
        //    var idParam = Application.SqlHelper.CreateParameter("@Id", Id);

        //    Application.SqlHelper.ExecuteNonQuery(sql, idParam);
        //}

        //public static IEnumerable<DeliveryDeadline> GetDeadlines()
        //{
        //    var result = new List<DeliveryDeadline>();
        //    const string sql = @"Select 
        //                            MIN(Id) AS Id, 
        //                            Name, 
        //                            0.0 AS Value, 
        //                            0 AS FromWords, 
        //                            0 AS ToWords, 
        //                            0 AS Days, 
        //                            1 AS OrderNumber,
        //                            OrderOnPage
        //                        FROM hsenseIqDeliveryDeadlines
        //                        GROUP BY Name, OrderOnPage
        //                        ORDER BY OrderOnPage";
        //    var reader = Application.SqlHelper.ExecuteReader(sql);
        //    while (reader.Read())
        //    {
        //        var textType = Read(reader);
        //        result.Add(textType);
        //    }

        //    return result;
        //}

        //private static DeliveryDeadline Read(IRecordsReader reader)
        //{
        //    var deliveryDeadline = new DeliveryDeadline
        //    {
        //        Id = reader.Get<int>("Id"),
        //        Name = reader.Get<string>("Name"),
        //        Value = reader.Get<decimal>("Value"),
        //        Days = reader.Get<int>("Days"),
        //        FromWords = reader.Get<int>("FromWords"),
        //        ToWords = reader.Get<int>("ToWords"),
        //        OrderNumber = reader.Get<int>("OrderNumber"),
        //        OrderOnPage = reader.Get<int>("OrderOnPage")
        //    };
        //    return deliveryDeadline;
        //}

        //public static DeliveryDeadline GetDeadline(int id)
        //{
        //    const string sql = @"Select Id, 
        //                            Name, 
        //                            Value, 
        //                            Days, 
        //                            FromWords, 
        //                            ToWords, 
        //                            OrderNumber,
        //                            OrderOnPage 
        //                        from hsenseIqDeliveryDeadlines 
        //                        where Id = @Id";
        //    var idParam = Application.SqlHelper.CreateParameter("@Id", id);
        //    var reader = Application.SqlHelper.ExecuteReader(sql, idParam);
        //    reader.Read();
        //    return Read(reader);
        //}

        //public static DeliveryDeadline GetDeadlineByDays(int days)
        //{
        //    const string sql = @"SELECT TOP 1  
        //                            Id, 
        //                            Name, 
        //                            Value, 
        //                            Days, 
        //                            FromWords, 
        //                            ToWords, 
        //                            OrderNumber 
        //                        FROM dbo.hsenseIqDeliveryDeadlines 
        //                        WHERE Days < @Days 
        //                        ORDER BY OrderOnPage ";
        //    var daysParam = Application.SqlHelper.CreateParameter("@Days", days);
        //    var reader = Application.SqlHelper.ExecuteReader(sql, daysParam);
        //    reader.Read();
        //    return Read(reader);
        //}

        //public static DeliveryDeadline GetDeadlineByWords(string deadlineName, int words)
        //{
        //    const string sql = @"SELECT TOP 1  Id, Name, Value, Days, FromWords, ToWords, OrderNumber,
        //                            OrderOnPage  
        //                            FROM dbo.hsenseIqDeliveryDeadlines 
        //                        WHERE Name = @Name
        //                        AND @Words BETWEEN FromWords AND ToWords
        //                            ORDER BY OrderOnPage";
        //    var nameParam = Application.SqlHelper.CreateParameter("@Name", deadlineName);
        //    var wordsParam = Application.SqlHelper.CreateParameter("@Words", words);
        //    var reader = Application.SqlHelper.ExecuteReader(sql, wordsParam, nameParam);
        //    reader.Read();
        //    return Read(reader);
        //}

        //public static List<DeliveryDeadline> GetDeadlines(int words)
        //{
        //    var result = new List<DeliveryDeadline>();
        //    const string sql = @"SELECT Id, Name, Value, Days, FromWords, ToWords, OrderNumber,
        //                            OrderOnPage  
        //                            FROM dbo.hsenseIqDeliveryDeadlines 
        //                        WHERE @Words BETWEEN FromWords AND ToWords
        //                            ORDER BY OrderOnPage ";

        //    var wordsParam = Application.SqlHelper.CreateParameter("@Words", words);
        //    var reader = Application.SqlHelper.ExecuteReader(sql, wordsParam);
        //    while (reader.Read())
        //    {
        //        var textType = Read(reader);
        //        result.Add(textType);
        //    }

        //    return result;
        //}

        //public static void DeleteByNameAndOrderNumber(string name, int orderNumber)
        //{
        //    var nameParam = Application.SqlHelper.CreateParameter("@Name", name);
        //    var orderNumberParam = Application.SqlHelper.CreateParameter("@OrderNumber", orderNumber);
        //    const string sql = "DELETE FROM hsenseIqDeliveryDeadlines WHERE Name = @Name AND OrderNumber = @OrderNumber";

        //    Application.SqlHelper.ExecuteNonQuery(sql, nameParam, orderNumberParam);

        //}

        //public static bool DeadlineExists(string name)
        //{
        //    var nameParam = Application.SqlHelper.CreateParameter("@Name", name);
        //    const string sql = "SELECT COUNT(0) as Count FROM hsenseIqDeliveryDeadlines WHERE Name = @Name";

        //    var count = Application.SqlHelper.ExecuteScalar<int>(sql, nameParam);

        //    return count > 0;
        //}
    }
}