﻿using AccentWebpage.Models;

namespace AccentWebpage.ViewModels
{
    public class PricelistViewModel
    {
        public Pricelist Pricelist { get; set; }

        public string Title
        {
            get
            {
                if (Pricelist != null && Pricelist.Id != 0)
                    return "Edit Pricelist";

                return "Add New Pricelist";
            }
        }
    }
}