﻿namespace AccentWebpage.ViewModels
{
    public class WordsDeadline
    {
        public DeliveryDeadline Words1 { get; set; }
        public DeliveryDeadline Words2 { get; set; }
        public DeliveryDeadline Words3 { get; set; }
        public static string Name { get; set; }
    }
}