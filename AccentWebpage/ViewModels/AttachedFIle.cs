﻿using System.IO;

namespace AccentWebpage.ViewModels
{
    public class AttachedFile
    {
        public string FileName { get; set; }
        public Stream InputStream { get; set; }
    }
}