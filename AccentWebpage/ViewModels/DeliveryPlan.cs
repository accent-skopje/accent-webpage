﻿using System;
using System.Collections.Generic;
using HSense.IQ.BsinessLogic;

namespace AccentWebpage.ViewModels
{
    public class DeliveryPlan : IEntity
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public decimal Value { get; set; }
        public int FromWords { get; set; }
        public int ToWords { get; set; }
        public decimal QualityControl { get; set; }
        public int OrderNumber { get; set; }
        public string Description { get; set; }
        public int OrderOnPage { get; set; }

        public static IEnumerable<DeliveryPlan> GetDeliveryPlans()
        {
            var result = new List<DeliveryPlan>();
            //const string sql = @"Select 
            //                        MIN(Id) AS Id, 
            //                        Name, 
            //                        0.0 AS Value, 
            //                        0 AS FromWords, 
            //                        0 AS ToWords, 
            //                        0.0 AS QualityControl, 
            //                        1 AS OrderNumber,
            //                        Description,
            //                        OrderOnPage
            //                    from hsenseIqDeliveryPlans
            //                        GROUP BY Name, OrderOnPage, Description
            //                    ORDER BY OrderOnPage";
            //var reader = Application.SqlHelper.ExecuteReader(sql);
            //while (reader.Read())
            //{
            //    var deliveryPlan = Read(reader);
            //    result.Add(deliveryPlan);
            //}

            return result;
        }

        public static IEnumerable<DeliveryPlan> GetDeliveryPlans(int words)
        {
            var result = new List<DeliveryPlan>();
            //const string sql = @"Select 
            //                        MIN(Id) AS Id, 
            //                        Name, 
            //                        0.0 AS Value, 
            //                        0 AS FromWords, 
            //                        0 AS ToWords, 
            //                        0.0 AS QualityControl, 
            //                        1 AS OrderNumber,
            //                        '' AS Description,
            //                        OrderOnPage
            //                    from hsenseIqDeliveryPlans
            //                        GROUP BY Name, OrderOnPage";
            //var reader = Application.SqlHelper.ExecuteReader(sql);
            //while (reader.Read())
            //{
            //    var deliveryPlan = Read(reader);
            //    result.Add(deliveryPlan);
            //}

            return result;
        }

        //private static DeliveryPlan Read(IRecordsReader reader)
        //{
        //    var deliveryPlan = new DeliveryPlan
        //    {
        //        Id = reader.Get<int>("Id"),
        //        Name = reader.Get<string>("Name"),
        //        Value = reader.Get<decimal>("Value"),
        //        FromWords = reader.Get<int>("FromWords"),
        //        ToWords = reader.Get<int>("ToWords"),
        //        QualityControl = reader.Get<decimal>("QualityControl"),
        //        OrderNumber = reader.Get<int>("OrderNumber"),
        //        Description = reader.Get<string>("Description"),
        //        OrderOnPage = reader.Get<int>("OrderOnPage")
        //    };
        //    return deliveryPlan;
        //}

        //public static DeliveryPlan GetDeliveryPlan(int deliveryPlanId)
        //{
        //    const string sql = @"Select 
        //                            Id, 
        //                            Name, 
        //                            Value, 
        //                            FromWords, 
        //                            ToWords, 
        //                            QualityControl, 
        //                            OrderNumber,
        //                            Description,
        //                            OrderOnPage 
        //                        from hsenseIqDeliveryPlans where Id = @Id";
        //    var idParam = Application.SqlHelper.CreateParameter("@Id", deliveryPlanId);
        //    var reader = Application.SqlHelper.ExecuteReader(sql, idParam);
        //    reader.Read();
        //    return Read(reader);
        //}

        //public static DeliveryPlan GetDeliveryPlanByWords(string planName, int words)
        //{
        //    const string sql = @"Select 
        //                            Id, 
        //                            Name, 
        //                            Value, 
        //                            FromWords, 
        //                            ToWords, 
        //                            QualityControl, 
        //                            OrderNumber,
        //                            Description,
        //                            OrderOnPage
        //                        from hsenseIqDeliveryPlans 
        //                        where Name = @Name
        //                        AND @Words BETWEEN FromWords AND ToWords";
        //    var wordsParam = Application.SqlHelper.CreateParameter("@Words", words);
        //    var nameParam = Application.SqlHelper.CreateParameter("@Name", planName);
        //    var reader = Application.SqlHelper.ExecuteReader(sql, wordsParam, nameParam);
        //    reader.Read();
        //    return Read(reader);
        //}

        //public static void DeleteByNameAndOrderNumber(string planName, int orderNumber)
        //{
        //    var nameParam = Application.SqlHelper.CreateParameter("@Name", planName);
        //    var orderNumberParam = Application.SqlHelper.CreateParameter("@OrderNumber", orderNumber);
        //    const string sql = "DELETE FROM hsenseIqDeliveryPlans WHERE Name = @Name AND OrderNumber = @OrderNumber";

        //    Application.SqlHelper.ExecuteNonQuery(sql, nameParam, orderNumberParam);

        //}

        //public static bool PlanExists(string name)
        //{
        //    var nameParam = Application.SqlHelper.CreateParameter("@Name", name);
        //    const string sql = "SELECT COUNT(0) as Count FROM hsenseIqDeliveryPlans WHERE Name = @Name";

        //    var count = Application.SqlHelper.ExecuteScalar<int>(sql, nameParam);

        //    return count > 0;
        //}

        public int Save()
        {
            throw new NotImplementedException();
        }

        public void Delete()
        {
            throw new NotImplementedException();
        }
    }
}