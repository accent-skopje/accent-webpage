﻿using System;

namespace AccentWebpage.ViewModels
{
    public class EmailTemplate : IEntity
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public string LogoUrl { get; set; }
        public string IntroText { get; set; }
        public string OutroText { get; set; }
        public string CompanyName { get; set; }
        public string TelephoneNumber { get; set; }
        public string VisitingAddress { get; set; }
        public string PostalAddress { get; set; }
        public string FooterTelephone { get; set; }
        public string Fax { get; set; }
        public string OrgNr { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }

        public int Save()
        {
            throw new NotImplementedException();
        }

        public void Delete()
        {
            throw new NotImplementedException();
        }

        //public int Save()
        //{
        //    int newId;
        //    var nameParam = Application.SqlHelper.CreateParameter("@Name", Name);
        //    var idParam = Application.SqlHelper.CreateParameter("@Id", Id);
        //    var logoParam = Application.SqlHelper.CreateParameter("@LogoUrl", "Something");
        //    var introParam = Application.SqlHelper.CreateParameter("@IntroText", IntroText);
        //    var outroParam = Application.SqlHelper.CreateParameter("@OutroText", OutroText);
        //    var companyNameParam = Application.SqlHelper.CreateParameter("@CompanyName", CompanyName);
        //    var telNumberParam = Application.SqlHelper.CreateParameter("@TelephoneNumber", TelephoneNumber);
        //    var visitingAddrParam = Application.SqlHelper.CreateParameter("@VisitingAddress", VisitingAddress);
        //    var postalAddressParam = Application.SqlHelper.CreateParameter("@PotalAddress", PostalAddress);
        //    var footerTelephoneParam = Application.SqlHelper.CreateParameter("@FooterTelephone", FooterTelephone);
        //    var faxParam = Application.SqlHelper.CreateParameter("@Fax", Fax);
        //    var orgNrParam = Application.SqlHelper.CreateParameter("@OrgNr", OrgNr);
        //    var emailParam = Application.SqlHelper.CreateParameter("@Email", Email);
        //    var websiteParam = Application.SqlHelper.CreateParameter("@Website", Website);

        //    if (Id <= 0)
        //    {
        //        const string sql = @"INSERT INTO hsenseIqEmailSettings
        //                                       (LogoUrl
        //                                       ,IntroText
        //                                       ,OutroText
        //                                       ,CompanyName
        //                                       ,TelephoneNumber
        //                                       ,VisitingAddress
        //                                       ,PotalAddress
        //                                       ,FooterTelephone
        //                                       ,Fax
        //                                       ,OrgNr
        //                                       ,Email
        //                                       ,Website)
        //                                 VALUES
        //                                       (@LogoUrl
        //                                       ,@IntroText
        //                                       ,@OutroText
        //                                       ,@CompanyName
        //                                       ,@TelephoneNumber
        //                                       ,@VisitingAddress
        //                                       ,@PotalAddress
        //                                       ,@FooterTelephone
        //                                       ,@Fax
        //                                       ,@OrgNr
        //                                       ,@Email
        //                                       ,@Website)";
        //        newId = Application.SqlHelper.ExecuteScalar<int>(sql, logoParam, introParam, outroParam,
        //            companyNameParam, telNumberParam, visitingAddrParam, postalAddressParam,
        //            footerTelephoneParam, faxParam, orgNrParam, emailParam, websiteParam);
        //    }
        //    else
        //    {
        //        const string sql = @"UPDATE hsenseIqEmailSettings
        //                                SET LogoUrl = @LogoUrl
        //                                    ,IntroText = @IntroText
        //                                    ,OutroText = @OutroText
        //                                    ,CompanyName = @CompanyName
        //                                    ,TelephoneNumber = @TelephoneNumber
        //                                    ,VisitingAddress = @VisitingAddress
        //                                    ,PotalAddress = @PotalAddress
        //                                    ,FooterTelephone = @FooterTelephone
        //                                    ,Fax = @Fax
        //                                    ,OrgNr = @OrgNr
        //                                    ,Email = @Email
        //                                    ,Website = @Website
        //                                WHERE @Id = @Id";
        //        Application.SqlHelper.ExecuteNonQuery(sql, nameParam, idParam, logoParam, introParam, outroParam,
        //            companyNameParam, telNumberParam, visitingAddrParam, postalAddressParam,
        //            footerTelephoneParam, faxParam, orgNrParam, emailParam, websiteParam);
        //        newId = Id;
        //    }
        //    return newId;
        //}

        //public void Delete()
        //{
        //    const string sql = "Delete from hsenseIqEmailSettings where Id = @Id";
        //    var idParam = Application.SqlHelper.CreateParameter("@Id", Id);

        //    Application.SqlHelper.ExecuteNonQuery(sql, idParam);
        //}

        //public static EmailTemplate Get()
        //{
        //    const string sql = @"SELECT Id
        //                              ,LogoUrl
        //                              ,IntroText
        //                              ,OutroText
        //                              ,CompanyName
        //                              ,TelephoneNumber
        //                              ,VisitingAddress
        //                              ,PotalAddress
        //                              ,FooterTelephone
        //                              ,Fax
        //                              ,OrgNr
        //                              ,Email
        //                              ,Website
        //                          FROM hsenseIqEmailSettings";
        //    var reader = Application.SqlHelper.ExecuteReader(sql);

        //    return reader.Read() ? Read(reader) : new EmailTemplate();
        //}

        //private static EmailTemplate Read(IRecordsReader reader)
        //{
        //    var emailTemplate = new EmailTemplate
        //    {
        //        Id = reader.Get<int>("Id"),
        //        CompanyName = reader.Get<string>("CompanyName"),
        //        Email = reader.Get<string>("Email"),
        //        Fax = reader.Get<string>("Fax"),
        //        FooterTelephone = reader.Get<string>("FooterTelephone"),
        //        IntroText = reader.Get<string>("IntroText"),
        //        LogoUrl = reader.Get<string>("LogoUrl"),
        //        OrgNr = reader.Get<string>("OrgNr"),
        //        Name = "",
        //        OutroText = reader.Get<string>("OutroText"),
        //        PostalAddress = reader.Get<string>("PotalAddress"),
        //        TelephoneNumber = reader.Get<string>("TelephoneNumber"),
        //        VisitingAddress = reader.Get<string>("VisitingAddress"),
        //        Website = reader.Get<string>("Website")
        //    };
        //    return emailTemplate;
        //}
    }
}