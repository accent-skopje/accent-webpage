﻿namespace HSense.IQ.BsinessLogic
{
    public class DocumentItem
    {
        public string Name { get; set; }
        public string Filetype { get; set; }
        public int WordCount { get; set; }
        public string Contents { get; set; }
    }
}