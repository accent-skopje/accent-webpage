﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AccentWebpage.Models;

namespace AccentWebpage.ViewModels
{
    public class AdminViewModel
    {
        public int CountLanguages { get; set; }

        public int CountRates { get; set; }

        public int CountDeliveryDeadlines { get; set; }

        public int CountQualityTypes { get; set; }

        public int CountNews { get; set; }

        public int CountOrders { get; set; }

        public IEnumerable<Models.Order> Orders { get; set; }

        public IEnumerable<News> Newses { get; set; }

        public AdminViewModel(ApplicationDbContext db)
        {
            CountLanguages = db.Languages.ToList().Count;
            CountRates = db.Rates.ToList().Count;
            CountDeliveryDeadlines = db.DeliveryDeadlineTypes.ToList().Count;
            CountQualityTypes = db.QualityTypes.ToList().Count;
            CountNews = db.Newses.Where(n => n.InfoText == null).ToList().Count;
            CountOrders = db.Orders.ToList().Count;
            Orders = db.Orders.OrderByDescending(o => o.Id).ToList().Take(5);
            Newses = db.Newses.OrderByDescending(n => n.Id).ToList().Take(5);
        }
    }
}