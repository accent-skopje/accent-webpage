namespace AccentWebpage.ViewModels
{
    public interface IEntity
    {
        string Name { get; set; }
        int Id { get; set; }
        int Save();
        void Delete();
    }
}