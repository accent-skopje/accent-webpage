﻿using AccentWebpage.Models;
using System.Collections.Generic;

namespace AccentWebpage.ViewModels
{
    public class LanguageViewModel
    {
        public IEnumerable<Models.Language> Languages { get; set; }

        public Language Language { get; set; }
    }
}