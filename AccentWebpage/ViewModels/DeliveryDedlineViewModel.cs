﻿using Antlr.Runtime.Misc;
using AccentWebpage.Controllers;
using AccentWebpage.Models;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace AccentWebpage.ViewModels
{
    public class DeliveryDedlineViewModel
    {
        public int Id { get; set; }
        public IEnumerable<DeliveryDeadlineType> DeliveryDeadlineTypes { get; set; }
        public DeliveryDeadlineType DeliveryDeadlineType { get; set; }
        public IEnumerable<DeliveryDeadlinePricelist> DeliveryDeadlinePricelists { get; set; }
        public DeliveryDeadlinePricelist DeliveryDeadlinePricelist { get; set; }
        public IEnumerable<Pricelist> Pricelists { get; set; }
        public Pricelist Pricelist { get; set; }

        public string Title
        {
            get
            {
                if (DeliveryDeadlineType != null && DeliveryDeadlineType.Id != 0)
                {
                    return "Edit Delivery Deadline";
                }
                return "Add Delivery Deadline";
            }
        }

        public string Action
        {
            get
            {
                Expression<Func<DeliveryDeadlineController, ActionResult>> create = (c => c.Create(this));
                Expression<Func<DeliveryDeadlineController, ActionResult>> update = (c => c.Update(this));
                var action = (Id != 0) ? update : create;

                return (action.Body as MethodCallExpression).Method.Name;
            }
        }

        public DeliveryDedlineViewModel()
        {
            Pricelists = new List<Pricelist>();
        }
    }
}