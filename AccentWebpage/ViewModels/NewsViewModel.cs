﻿using Antlr.Runtime.Misc;
using AccentWebpage.Controllers;
using AccentWebpage.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace AccentWebpage.ViewModels
{
    public class NewsViewModel
    {
        public int Id { get; set; }
        public IEnumerable<News> Newses { get; set; }
        public News News { get; set; }

        public HttpPostedFileBase file { get; set; }

        public string Title
        {
            get
            {
                if (News != null && News.Id != 0)
                {
                    return "Edit News";
                }
                return "Add News";
            }
        }

        public string Action
        {
            get
            {
                Expression<Func<NewsController, ActionResult>> create = (c => c.Create(this, file));
                Expression<Func<NewsController, ActionResult>> update = (c => c.Update(this, file));
                var action = (Id != 0) ? update : create;

                return (action.Body as MethodCallExpression).Method.Name;
            }
        }

        public string Action1
        {
            get
            {
                Expression<Func<NewsController, ActionResult>> createInfo = (c => c.CreateInfo(this));
                Expression<Func<NewsController, ActionResult>> updateInfo = (c => c.UpdateInfo(this));
                var action = (Id != 0) ? updateInfo : createInfo;

                return (action.Body as MethodCallExpression).Method.Name;
            }
        }

        public void UploadImage(HttpPostedFileBase file)
        {
            var path = string.Empty;

            if (file.ContentLength > 0)
            {
                if (Path.GetExtension(file.FileName).ToLower() == ".jpg"
                    || Path.GetExtension(file.FileName).ToLower() == ".png"
                    || Path.GetExtension(file.FileName).ToLower() == ".jpeg"
                    || Path.GetExtension(file.FileName).ToLower() == ".gif")
                {
                    path = Path.Combine(HttpContext.Current.Server.MapPath("/Content/Images/News/"), file.FileName);
                    file.SaveAs(path);
                }
            }
        }
    }
}