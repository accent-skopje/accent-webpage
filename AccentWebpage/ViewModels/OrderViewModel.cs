﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Web;
using AccentWebpage.Models;

namespace AccentWebpage.ViewModels
{
    public class OrderViewModel
    {
        public IEnumerable<Order> Orders { get; set; }

        public Order Order { get; set; }

        public static decimal Vat = 25;
        public int Id { get; set; }

        public List<DocumentItem> DocumentItems { get; set; }

        public int WordCount;
        //{
        //    get { return DocumentItems.Sum(di => di.WordCount); }
        //}

        public Language SourceLanguage { get; set; }

        public List<Language> DestinationLanguages { get; set; }

        public decimal CostPerWord { get; set; }

        public decimal TotalCost { get; set; }

        //public decimal TotalCost
        //{
        //    get
        //    {
        //        decimal total = 0;
        //        foreach (var rate in Rates)
        //        {
        //            var costPerWord = rate.CostPerWord;

        //            var temp = WordCount * (costPerWord + DeliveryPlan.Value);
        //            var hours = GetHoursFromWords(WordCount);
        //            var qualityControlTotal = hours * DeliveryPlan.QualityControl;
        //            temp += qualityControlTotal;
        //            var deliveryPercent = DeliveryPercent(temp);
        //            temp += deliveryPercent;
        //            if (temp < 690)
        //            {
        //                temp = 690;
        //            }

        //            total += temp;
        //        }
        //        /*
        //        var total = WordCount*(CostPerWord + DeliveryPlan.Value);
        //        var hours = GetHoursFromWords(WordCount);
        //        var qualityControlTotal = hours*DeliveryPlan.QualityControl;
        //        total += qualityControlTotal;
        //        var deliveryPercent = DeliveryPercent(total);
        //        total += deliveryPercent
        //        */

        //        return total;
        //    }
        //}

        public decimal TotalCostWithVat { get; set; }
        //{
        //    get
        //    {
        //        var costWithVat = TotalCost + (TotalCost * (Vat / 100u));
        //        return costWithVat;
        //    }
        //}

        //private decimal DeliveryPercent(decimal total)
        //{
        //    return total * (Convert.ToDecimal(DeliveryDeadline.Cost) / 100u);
        //}

        //private int GetHoursFromWords(int wordCount)
        //{
        //    var result = 0;
        //    var div = wordCount / 2000;
        //    result += div;
        //    var mod = wordCount % 2000;
        //    if (mod > 0)
        //    {
        //        result++;
        //    }
        //    return result;
        //}

        /*public bool QualityControl { get; set; }*/

        public Pricelist DeliveryDeadline { get; set; }
        public Pricelist DeliveryPlan { get; set; }
        public List<Rate> Rates { get; set; }
        public List<Pricelist> RatePricelists { get; set; }
        //public EmailRecepient Receiver { get; set; }
        //public string EmailContent { get; set; }

        public static int CreateNew(int receiverId, ApplicationDbContext db, string emailContent)
        {
            int newId;
            
            Order order = new Order();
            order.ReceiverProfileId = receiverId;
            order.Email = emailContent;
            db.Orders.Add(order);
            db.SaveChanges();

            newId = order.Id;

            return newId;
        }

        //public static void Save(int orderId, string emailContent)
        //{
        //    var orderIdParam = Application.SqlHelper.CreateParameter("@Id", orderId);
        //    var emailContentParam = Application.SqlHelper.CreateParameter("@Email", emailContent);
        //    const string sql = "UPDATE hsenseIqOrder SET Email = @Email WHERE Id = @Id";
        //    Application.SqlHelper.ExecuteNonQuery(sql, orderIdParam, emailContentParam);
        //}

        //public static List<Order> GetOrders(int page, int itemsPerPage, out int pages)
        //{
        //    var result = new List<Order>();
        //    page = page > 0 ? page : 1;
        //    itemsPerPage = itemsPerPage > 0 ? itemsPerPage : 50;

        //    const string countSql = @"SELECT COUNT(0) AS Total FROM hsenseIqOrder";
        //    var totalItems = MediaTypeNames.Application.SqlHelper.ExecuteScalar<int>(countSql);
        //    pages = (totalItems / itemsPerPage) + 1;

        //    var startIndex = (page - 1) * itemsPerPage + 1;
        //    var endIndex = startIndex + itemsPerPage;
        //    var startIndexParam = MediaTypeNames.Application.SqlHelper.CreateParameter("@startIndex", startIndex);
        //    var endIndexParam = MediaTypeNames.Application.SqlHelper.CreateParameter("@endIndex", endIndex);

        //    const string sql = @"SELECT  *
        //                        FROM    ( SELECT    ROW_NUMBER() OVER ( ORDER BY o.Id DESC ) AS RowNum, o.Id AS OrderId, r.FirstName, r.LastName, r.Email
        //                                  FROM      hsenseIqOrder AS o 
        //                            INNER JOIN hsenseIqReceiverProfile AS r ON r.Id = o.Receiver
        //                                ) AS RowConstrainedResult
        //                        WHERE   RowNum >= @startIndex
        //                            AND RowNum < @endIndex
        //                        ORDER BY RowNum";
        //    var reader = MediaTypeNames.Application.SqlHelper.ExecuteReader(sql, startIndexParam, endIndexParam);
        //    while (reader.Read())
        //    {
        //        var order = Read(reader);
        //        result.Add(order);
        //    }

        //    return result;
        //}

        //private static Order Read(IRecordsReader reader)
        //{
        //    if (!reader.HasRecords)
        //        return null;

        //    var order = new Order
        //    {
        //        Id = reader.Get<int>("OrderId"),
        //        Receiver = new EmailRecepient
        //        {
        //            FirstName = reader.Get<string>("FirstName"),
        //            LastName = reader.Get<string>("LastName"),
        //            Email = reader.Get<string>("Email")
        //        }
        //    };

        //    try
        //    {
        //        order.EmailContent = reader.Get<string>("EmailContent");
        //    }
        //    catch
        //    {
        //        order.EmailContent = "";
        //    }
        //    return order;
        //}

        //public static Order Get(int orderId)
        //{
        //    var orderIdParam = MediaTypeNames.Application.SqlHelper.CreateParameter("@Id", orderId);
        //    const string sql =
        //    @"SELECT o.Id AS OrderId, o.Email as EmailContent, r.FirstName, r.LastName, r.Email FROM dbo.hsenseIqOrder AS o
        //        INNER JOIN dbo.hsenseIqReceiverProfile AS r ON r.Id = o.Receiver
        //        WHERE o.Id = @Id";
        //    var reader = MediaTypeNames.Application.SqlHelper.ExecuteReader(sql, orderIdParam);
        //    reader.Read();
        //    return Read(reader);
        //}

        //public static List<Order> Get(string email)
        //{
        //    var result = new List<Order>();
        //    var emailParam = MediaTypeNames.Application.SqlHelper.CreateParameter("@Email", email);
        //    const string sql =
        //        @"SELECT o.Id AS OrderId, o.Email as EmailContent, r.FirstName, r.LastName, r.Email FROM dbo.hsenseIqOrder AS o
        //            INNER JOIN dbo.hsenseIqReceiverProfile AS r ON r.Id = o.Receiver
        //            WHERE r.Email = @Email";

        //    var reader = MediaTypeNames.Application.SqlHelper.ExecuteReader(sql, emailParam);
        //    while (reader.Read())
        //    {
        //        var order = Read(reader);
        //        result.Add(order);
        //    }

        //    return result;
        //}
    }
}