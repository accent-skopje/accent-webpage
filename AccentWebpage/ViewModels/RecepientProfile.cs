﻿using System;
using System.Linq;
using AccentWebpage.Models;

namespace AccentWebpage.ViewModels
{
    public class RecepientProfile
    {
        public Subscription Subscription { get; set; }
        public ContactPerson ContactPerson { get; set; }
        public BillingInformation BillingInformation { get; set; }
        public NewCustomer NewCustomer { get; set; }
        public CustomerType CustomerType { get; set; }

        /*public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string CompanyName { get; set; }*/
        public string Name { get; set; }
        public int Id { get; set; }

        public int Save(ApplicationDbContext db)
        {
            int newId;
            var firstNameParam = ContactPerson.FirstName;
            var lastNameParam = ContactPerson.LastName;
            var emailParam = ContactPerson.Email;
            var phoneNumberParam = ContactPerson.PhoneNumber;
            var companyNameParam = Subscription.CompanyName ?? "";

            var existingRecepeientId = FindRecepient(db);

            if (existingRecepeientId > 0)
            {
                newId = existingRecepeientId;
            }
            else
            {
                ReceiverProfile rp = new ReceiverProfile();
                rp.FirstName = firstNameParam;
                rp.LastName = lastNameParam;
                rp.Email = emailParam;
                rp.PhoneNumber = phoneNumberParam;
                rp.CompanyName = companyNameParam;
                db.ReceiverProfile.Add(rp);
                db.SaveChanges();

                newId = rp.Id;
            }

            return newId;
        }

        public int FindRecepient(ApplicationDbContext db)
        {
            var receiverProfile =
                db.ReceiverProfile.FirstOrDefault(
                    r => r.FirstName == ContactPerson.FirstName && r.LastName == ContactPerson.LastName && r.Email == ContactPerson.Email);
            var id = receiverProfile != null ? receiverProfile.Id : 0;

            return id;
        }

        public void Delete()
        {
            throw new NotImplementedException();
        }
    }

    public class CustomerType
    {
        public bool IsPrivate { get; set; }
        public bool IsCompany { get; set; }
        public bool IsNewCustomer { get; set; }

        public string GetType(string language)
        {
            if (IsPrivate)
            {
                return language == "eng" ? "Private" : "Privatperson";
            }

            if (IsCompany)
            {
                return language == "eng" ? "Company" : "Företagskund";
            }

            return "";
        }

        public string GetNumberType(string language)
        {
            //if (IsPrivate)
            //{
            //    return language == "eng" ? "Personal number" : "Personnumer";
            //}
            if (IsCompany)
            {
                return language == "eng" ? "Organisation number/Customer number:" : "Organisationsnummer/Kundnummer:";
            }

            return "";
        }
    }

    public class Subscription
    {
        public string CompanyName { get; set; }
        public string CustomerNumber { get; set; }
    }

    public class ContactPerson
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string PersonalNumber { get; set; }
    }

    public class BillingInformation
    {
        public string Address { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Recepient { get; set; }
        public string Reference { get; set; }
    }

    public class CompanyInformation
    {
        public string Title { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public string OrganisationNumber { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string VisitingAddress { get; set; }
        public string Country { get; set; }
    }

    public class NewCustomer
    {
        public CompanyInformation CompanyInformation { get; set; }
        public BillingInformation BillingInformation { get; set; }
    }
}