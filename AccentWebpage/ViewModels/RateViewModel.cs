﻿using AccentWebpage.Models;
using System.Collections.Generic;

namespace AccentWebpage.ViewModels
{
    public class RateViewModel
    {
        public IEnumerable<Language> SourceLanguages { get; set; }

        public IEnumerable<Language> DestinationLanguages { get; set; }

        public Rate Rate { get; set; }

        public IEnumerable<Rate> Rates { get; set; }

        public Pricelist Pricelist { get; set; }

        public IEnumerable<Pricelist> Pricelists { get; set; }

        public RatePricelist RatePricelist { get; set; }

        public IEnumerable<RatePricelist> RatePricelists { get; set; }

        public string Title
        {
            get
            {
                if (Rate != null && Rate.Id != 0)
                    return "Edit Rate";

                return "Add New Rate";
            }
        }
    }
}