﻿using System.Collections.Generic;
using System.Linq;

namespace AccentWebpage.ViewModels
{
    public class Calculations
    {

        public static OrderViewModel GetOrder(
            List<DocumentItem> documentItems, 
            int sourceLanguageId,
            List<int> destinationLanguageIds, 
            string deliveryPlanName, 
            //int deliveryDeadlineId,
            string deadlineName)
        {
            var order = new OrderViewModel()
            {
                DocumentItems = documentItems
            };

            var wordCount = order.WordCount;
            //var deliveryPlan = DeliveryPlan.GetDeliveryPlanByWords(deliveryPlanName, wordCount);
            ////var deliveryDeadline = DeliveryDeadline.GetDeadline(deliveryDeadlineId);
            //var deliveryDeadline = DeliveryDeadline.GetDeadlineByWords(deadlineName, wordCount);

            //var sourceLang = Language.GetLanguage(sourceLanguageId);
            //var destinationLangs = Language.GetLanguages(destinationLanguageIds);
            //var rates = Rate.GetRatesforCalculation(sourceLanguageId, destinationLanguageIds, wordCount);

            //var rateCostPerWord = rates.Sum(r => r.CostPerWord);

            //order.CostPerWord = rateCostPerWord;
            ////order.QualityControl = deliveryPlan.QualityControl > 0;
            //order.DestinationLanguages = destinationLangs;
            //order.SourceLanguage = sourceLang;
            //order.DeliveryDeadline = deliveryDeadline;
            //order.DeliveryPlan = deliveryPlan;
            //order.Rates = rates;

            return order;
        }
    }
}