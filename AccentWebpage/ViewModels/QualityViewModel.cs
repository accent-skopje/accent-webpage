﻿using Antlr.Runtime.Misc;
using AccentWebpage.Controllers;
using AccentWebpage.Models;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace AccentWebpage.ViewModels
{
    public class QualityViewModel
    {
        public int Id { get; set; }

        public QualityType QualityType { get; set; }

        public IEnumerable<QualityType> QualityTypes { get; set; }

        public QualityPricelist QualityPricelist { get; set; }

        public IEnumerable<QualityPricelist> QualityPricelists { get; set; }

        public Pricelist Pricelist { get; set; }

        public IEnumerable<Pricelist> Pricelists { get; set; }

        public string Title
        {
            get
            {
                if (QualityType != null && QualityType.Id != 0)
                    return "Edit Quality";

                return "Add New Quality";
            }
        }

        public string Action
        {
            get
            {
                Expression<Func<QualityTypesController, ActionResult>> create = (c => c.Create(this));
                Expression<Func<QualityTypesController, ActionResult>> update = (c => c.Update(this));
                var action = (Id != 0) ? update : create;

                return (action.Body as MethodCallExpression).Method.Name;
            }
        }

        public QualityViewModel()
        {
            Pricelists = new List<Pricelist>();
        }
    }
}