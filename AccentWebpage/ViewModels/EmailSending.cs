﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web;
using AccentWebpage.ViewModels;
using HSense.IQ.BsinessLogic;

namespace AccentWebpage.ViewModels
{
    public class EmailSending
    {
        public bool SendEmail(OrderViewModel order, RecepientProfile recepient, List<AttachedFile> files, string language, List<EmailRecepient> emailRecepients)
        {
            var content = GetEmailContent(order, recepient, language);

            SmtpClient client = new SmtpClient("smtp.gmail.com");
            client.Port = 587;
            client.Credentials = new System.Net.NetworkCredential("sprakservicetest@gmail.com", "p@ssw0rd1p@ssw0rd1");
            client.EnableSsl = true;
            //client.UseDefaultCredentials = false;

            const string headerLogoRelativePath = "../usercontrols/headerlogo.png";
            const string footerLogoRelativePath = "../usercontrols/footerlogo.png";
            var headerLogoPhysicalPath = HttpContext.Current.Server.MapPath(headerLogoRelativePath);
            var footerLogoPhysicalPath = HttpContext.Current.Server.MapPath(footerLogoRelativePath);
            LinkedResource headerImage = new LinkedResource(headerLogoPhysicalPath, MediaTypeNames.Image.Jpeg);
            LinkedResource footerImage = new LinkedResource(footerLogoPhysicalPath, MediaTypeNames.Image.Jpeg);
            headerImage.ContentId = "headerimage";
            footerImage.ContentId = "footerimage";

            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(content, null, MediaTypeNames.Text.Html);
            avHtml.LinkedResources.Add(headerImage);
            avHtml.LinkedResources.Add(footerImage);

            MailMessage msg = new MailMessage();
            //msg.From = new MailAddress("sprakservicetest@gmail.com", "Sprakservice Team");
            msg.From = new MailAddress("\"Språkservice Sverige AB\" <sprakservicetest@gmail.com>", "Språkservice Sverige AB");
            msg.Subject = "Your order";
            msg.To.Add(recepient.ContactPerson.Email);

            foreach (var emailRecepient in emailRecepients)
            {
                msg.Bcc.Add(emailRecepient.Email);
            }

            msg.AlternateViews.Add(avHtml);
            msg.IsBodyHtml = true;

            if (files != null)
            {
                foreach (var attachedFile in files)
                {
                    msg.Attachments.Add(new Attachment(attachedFile.InputStream, attachedFile.FileName));
                }
            }

            client.Send(msg);

            return true;
        }

        public bool SendNewCustomerEmail(RecepientProfile recepient, string language)
        {
            SmtpClient client = new SmtpClient("smtp.gmail.com");
            //client.EnableSsl = true;
            client.Port = 587;
            client.Credentials = new System.Net.NetworkCredential("sprakservicetest@gmail.com", "p@ssw0rd1p@ssw0rd1");
            client.EnableSsl = true;
            //client.UseDefaultCredentials = false;

            MailMessage msg = new MailMessage();
            //msg.From = new MailAddress("sprakservicetest@gmail.com", "Sprakservice Team");
            msg.From = new MailAddress("\"Språkservice Sverige AB\" <sprakservicetest@gmail.com>", "Sprakservice Team");
            msg.Subject = "New Customer registration";

            msg.To.Add("lazarov_boban@yahoo.com");

            StringBuilder bodyContent = new StringBuilder();

            if (language == "swe")
            {
                msg.Subject = "Ny kund registrering";
                bodyContent.Append("Tjänst: Översättning\n");
                bodyContent.Append("Företag info\n");

                bodyContent.AppendFormat("Kundnamn (Företagsnamn): {0}\n", recepient.Subscription.CompanyName);
                bodyContent.AppendFormat("Organisationsnummer: {0}\n", recepient.NewCustomer.CompanyInformation.OrganisationNumber);
                bodyContent.AppendFormat("Avdelning, beskrivning el dylikt: {0}\n", recepient.NewCustomer.CompanyInformation.Title);
                bodyContent.AppendFormat("Telefon: {0}\n", recepient.NewCustomer.CompanyInformation.Phone);
                bodyContent.AppendFormat("Faxnummer: {0}\n", recepient.NewCustomer.CompanyInformation.Fax);
                bodyContent.AppendFormat("Besöksadress: {0}\n", recepient.NewCustomer.CompanyInformation.VisitingAddress);
                bodyContent.AppendFormat("Postnummer: {0}\n", recepient.NewCustomer.CompanyInformation.ZipCode);
                bodyContent.AppendFormat("Ort: {0}\n", recepient.NewCustomer.CompanyInformation.City);
                bodyContent.AppendFormat("E-post: {0}\n", recepient.NewCustomer.CompanyInformation.Email);
                bodyContent.AppendFormat("Kontaktperson: {0}\n", recepient.ContactPerson.FirstName + " " + recepient.ContactPerson.LastName);
                bodyContent.AppendFormat("Telefon kontaktperson: {0}\n", recepient.ContactPerson.PhoneNumber);

                bodyContent.Append("\nFakturamottagare\n");
                bodyContent.AppendFormat("Fakturamottagare: {0}\n", recepient.NewCustomer.BillingInformation.Recepient);
                bodyContent.AppendFormat("Referens/Kostnadsställe el dylikt: {0}\n", recepient.NewCustomer.BillingInformation.Reference);
                bodyContent.AppendFormat("Gatuadress/box: {0}\n", recepient.NewCustomer.BillingInformation.Address);
                bodyContent.AppendFormat("Postnummer: {0}\n", recepient.NewCustomer.BillingInformation.ZipCode);
                bodyContent.AppendFormat("Ort: {0}\n", recepient.NewCustomer.BillingInformation.City);
            }
            else
            {
                msg.Subject = "New Customer registration";
                bodyContent.Append("Service: Translation\n");
                bodyContent.Append("Company informaations\n");

                bodyContent.AppendFormat("Customer name (Company name):: {0}\n", recepient.Subscription.CompanyName);
                bodyContent.AppendFormat("Organisation number: {0}\n", recepient.NewCustomer.CompanyInformation.OrganisationNumber);
                bodyContent.AppendFormat("Title: {0}\n", recepient.NewCustomer.CompanyInformation.Title);
                bodyContent.AppendFormat("Phone: {0}\n", recepient.NewCustomer.CompanyInformation.Phone);
                bodyContent.AppendFormat("Fax: {0}\n", recepient.NewCustomer.CompanyInformation.Fax);
                bodyContent.AppendFormat("Visiting address: {0}\n", recepient.NewCustomer.CompanyInformation.VisitingAddress);
                bodyContent.AppendFormat("Zip code: {0}\n", recepient.NewCustomer.CompanyInformation.ZipCode);
                bodyContent.AppendFormat("City: {0}\n", recepient.NewCustomer.CompanyInformation.City);
                bodyContent.AppendFormat("E-mail: {0}\n", recepient.NewCustomer.CompanyInformation.Email);
                bodyContent.AppendFormat("Contact person: {0}\n", recepient.ContactPerson.FirstName + " " + recepient.ContactPerson.LastName);
                bodyContent.AppendFormat("Contact person phone: {0}\n", recepient.ContactPerson.PhoneNumber);

                bodyContent.Append("\nBilling information\n");
                bodyContent.AppendFormat("Invoicee: {0}\n", recepient.NewCustomer.BillingInformation.Recepient);
                bodyContent.AppendFormat("Reference: {0}\n", recepient.NewCustomer.BillingInformation.Reference);
                bodyContent.AppendFormat("Address/box: {0}\n", recepient.NewCustomer.BillingInformation.Address);
                bodyContent.AppendFormat("Zip code: {0}\n", recepient.NewCustomer.BillingInformation.ZipCode);
                bodyContent.AppendFormat("City: {0}\n", recepient.NewCustomer.BillingInformation.City);
            }

            msg.Body = bodyContent.ToString();

            client.Send(msg);

            return true;
        }

        public static string GetEmailContent(OrderViewModel order, RecepientProfile recepient, string language)
        {
            string filledRelativePath = "~/usercontrols/filledemailtemplate.html";
            string ultimateRuleMessage = "The minimum cost per order is no less than 690.00SEK (excl. VAT) and non-sworn translation.";
            string weWillContactYou = "We will be in Contact with you about a delivery date";
            string includingVat = "(incl. VAT)";
            string excludingVat = "(excl. VAT)";
            string cultureInfo = "en-US";
            
            if (language == "swe")
            {
                filledRelativePath = "~/usercontrols/filledemailtemplate_sv.html";
                ultimateRuleMessage = "Minimidebitering SEK 690,00";
                weWillContactYou = "Vi kommer att kontakta dig angående leveransdatum";
                includingVat = "(inkl. moms)";
                excludingVat = "(exkl. moms)";
                cultureInfo = "sv-SE";
            }
            //var headerImageBase64 = ImageToBase64()
            var filledPhysicalPath = HttpContext.Current.Server.MapPath(filledRelativePath);
            var templateContents = File.ReadAllText(filledPhysicalPath);

            var documentNames = new StringBuilder("");
            var documentNamesSubject = "";
            var fileTypes = new StringBuilder("");
            foreach (var documentItgem in order.DocumentItems)
            {
                documentNames.AppendFormat("<div>{0}</div>", documentItgem.Name);
                documentNamesSubject += string.Format("{0}, ", documentItgem.Name);
                fileTypes.AppendFormat("<div>{0}</div>", 
                    language == "swe" && documentItgem.Filetype == "Typed text" ? 
                    "Fritext" : 
                    documentItgem.Filetype);
            }
            //documentNames.Append("</div>");
            //fileTypes.Append("</div>");
            documentNamesSubject = documentNamesSubject.Remove(documentNamesSubject.LastIndexOf(","));

            var destinationLangs = new StringBuilder("");
            foreach (var destinationLanguage in order.DestinationLanguages)
            {
                destinationLangs.AppendFormat("<div>{0}</div>", destinationLanguage.Name);
            }
            //destinationLangs.AppendFormat("</ul>");

            var ultimateRuleAplies = order.TotalCost <= 690;
            
            templateContents = templateContents.Replace("UMBTMP:ULTIMATERULE", ultimateRuleAplies ? ultimateRuleMessage : "");
            
            templateContents = templateContents.Replace("UMBTMP:DATE", DateTime.Now.ToShortDateString());
            string estimatedDate = "";

            if (order.DeliveryDeadline.Days > 1)
            {
                var deliveryDate = AddBusinessDays(DateTime.Now.Date, order.DeliveryDeadline.Days.Value);
                estimatedDate = deliveryDate.ToShortDateString();
            }
            else
            {
                estimatedDate = weWillContactYou;
            }

            templateContents = templateContents.Replace("UMBTMP:ORDERID", order.Id.ToString());
            templateContents = templateContents.Replace("UMBTMP:QUALITYPACKAGE", order.DeliveryPlan.Name);
            templateContents = templateContents.Replace("UMBTMP:DOCUMENTNAMESSUBJECT", documentNamesSubject);
            templateContents = templateContents.Replace("UMBTMP:DOCUMENTNAMES", documentNames.ToString());
            templateContents = templateContents.Replace("UMBTMP:FILETYPES", fileTypes.ToString());
            templateContents = templateContents.Replace("UMBTMP:WORDCOUNT", order.WordCount.ToString(new CultureInfo(cultureInfo)));
            templateContents = templateContents.Replace("UMBTMP:SOURCELANGUAGE", order.SourceLanguage.Name);
            templateContents = templateContents.Replace("UMBTMP:DESTLANGUAGES", destinationLangs.ToString());
            //templateContents = templateContents.Replace("UMBTMP:COSTPERWORD", string.Format(new CultureInfo(cultureInfo), "{0:N3}", order.CostPerWord));

            templateContents = templateContents.Replace("UMBTMP:DELIVERYDATE", estimatedDate);
            if (recepient != null)
            {
                //templateContents = templateContents.Replace(GlobalSettings.Path + "/plugins/hsenseiq/headerlogo.png", "cid:headerimage");
                //templateContents = templateContents.Replace(GlobalSettings.Path + "/plugins/hsenseiq/footerlogo.png", "cid:footerimage");

                templateContents = templateContents.Replace("UMBTMP:RECEPIENTFIRSTNAME", recepient.ContactPerson.FirstName);
                templateContents = templateContents.Replace("UMBTMP:RECEPIENTLASTNAME", recepient.ContactPerson.LastName);
                templateContents = templateContents.Replace("UMBTMP:DELIVERYADDRESS", recepient.BillingInformation.Address);
                //templateContents = templateContents.Replace("UMBTMP:RECEPIENTCOMPANYNAME", recepient.Subscription.CompanyName);
                templateContents = templateContents.Replace("UMBTMP:RECEPIENTPHONENUMBER", recepient.ContactPerson.PhoneNumber);
                templateContents = templateContents.Replace("UMBTMP:RECEPIENTEMAIL", recepient.ContactPerson.Email);
                templateContents = templateContents.Replace("UMBTMP:CUSTOMERTYPE", recepient.CustomerType.GetType(language));
                templateContents = templateContents.Replace("UMBTMP:IDENTIFICATIONNUMBER", recepient.CustomerType.GetNumberType(language));
                templateContents = templateContents.Replace("UMBTMP:CUSTOMERIDENTIFICATION", 
                    recepient.CustomerType.IsPrivate
                        ? ""//recepient.ContactPerson.PersonalNumber
                        : recepient.Subscription.CustomerNumber);

                var totalCost = "";

                if (recepient.CustomerType.IsPrivate)
                {
                    totalCost = ultimateRuleAplies
                        ? string.Format(new CultureInfo(cultureInfo), "{0:N2}", 690 + (690 * OrderViewModel.Vat/100u))
                        : string.Format(new CultureInfo(cultureInfo), "{0:N2}", order.TotalCostWithVat);
                    totalCost += " SEK " + includingVat;
                }
                else
                {
                    totalCost = ultimateRuleAplies
                        ? string.Format(new CultureInfo(cultureInfo), "{0:N2}", 690)
                        : string.Format(new CultureInfo(cultureInfo), "{0:N2}", order.TotalCost);

                    totalCost += " SEK " + excludingVat;
                }

                templateContents = templateContents.Replace("UMBTMP:TOTALCOST", totalCost);
            }
            else // This means that it is displaying the order for preview
            {
                var totalCost = ultimateRuleAplies
                        ? string.Format(new CultureInfo(cultureInfo), "{0:N2}", 690)
                        : string.Format(new CultureInfo(cultureInfo), "{0:N2}", order.TotalCost);
                totalCost += " SEK " + excludingVat;
                totalCost += "<br />";
                totalCost += ultimateRuleAplies
                        ? string.Format(new CultureInfo(cultureInfo), "{0:N2}", 690 + (690 * OrderViewModel.Vat / 100u))
                        : string.Format(new CultureInfo(cultureInfo), "{0:N2}", order.TotalCostWithVat);
                totalCost += " SEK " + includingVat;
                
                templateContents = templateContents.Replace("UMBTMP:TOTALCOST", totalCost);
                templateContents = templateContents.Replace("UMBTMP:ORDERID", "");
                templateContents = templateContents.Replace("UMBTMP:DELIVERYADDRESS", "");
                templateContents = templateContents.Replace("UMBTMP:CUSTOMERTYPE", ""); 
                templateContents = templateContents.Replace("UMBTMP:IDENTIFICATIONNUMBER:", "");
                templateContents = templateContents.Replace("UMBTMP:CUSTOMERIDENTIFICATION", "");
                templateContents = templateContents.Replace("UMBTMP:RECEPIENTFIRSTNAME", "");
                templateContents = templateContents.Replace("UMBTMP:RECEPIENTLASTNAME", "");
                templateContents = templateContents.Replace("UMBTMP:RECEPIENTCOMPANYNAME", "");
                templateContents = templateContents.Replace("UMBTMP:RECEPIENTPHONENUMBER", "");
                templateContents = templateContents.Replace("UMBTMP:RECEPIENTEMAIL", "");
                templateContents = templateContents.Replace("Till:", "");
                templateContents = templateContents.Replace("To:", "");
                templateContents = templateContents.Replace("Typ av kund:", "");
                templateContents = templateContents.Replace("Customer type:", "");
            }

            return templateContents;
        }

        public static DateTime AddBusinessDays(DateTime date, int days)
        {
            if (days < 0)
            {
                throw new ArgumentException("days cannot be negative", "days");
            }

            if (days == 0) return date;

            if (date.DayOfWeek == DayOfWeek.Saturday)
            {
                date = date.AddDays(2);
                days -= 1;
            }
            else if (date.DayOfWeek == DayOfWeek.Sunday)
            {
                date = date.AddDays(1);
                days -= 1;
            }

            date = date.AddDays(days / 5 * 7);
            int extraDays = days % 5;

            if ((int)date.DayOfWeek + extraDays > 5)
            {
                extraDays += 2;
            }

            return date.AddDays(extraDays);
        }
    }
}