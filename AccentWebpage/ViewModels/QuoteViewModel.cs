﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccentWebpage.ViewModels
{
    public class QuoteViewModel
    {
        public List<DocumentItem> DocumentItems { get; set; }
        public int SourceLangId { get; set; }
        public List<int> DestinationIds { get; set; }
        public string DeliveryPlanName { get; set; }
        //public int DeliveryDeadlineId { get; set; }
        public string DeliveryDeadlineName { get; set; }
        public int Days { get; set; }
    }

    public class DocumentItem
    {
        public string Name { get; set; }
        public int WordCount { get; set; }
        public string Filetype { get; set; }
        public int Id { get; set; }
        public string Contents { get; set; }
    }

}