﻿using System;

namespace AccentWebpage.ViewModels
{
    [Serializable]
    public class EmailRecepient : IEntity
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        public int Save()
        {
            throw new NotImplementedException();
        }

        public void Delete()
        {
            throw new NotImplementedException();
        }

        //public int Save()
        //{
        //    int newId;
        //    Application.SqlHelper.CreateParameter("@Name", Name);
        //    var idParam = Application.SqlHelper.CreateParameter("@Id", Id);
        //    var firstNameParam = Application.SqlHelper.CreateParameter("@FirstName", FirstName);
        //    var lastNameParam = Application.SqlHelper.CreateParameter("@LastName", LastName);
        //    var emailParam = Application.SqlHelper.CreateParameter("@Email", Email);
        //    if (Id <= 0)
        //    {
        //        const string sql = "Insert into hsenseIqEmailRecepients (FirstName, LastName, Email) OUTPUT Inserted.Id values (@FirstName, @LastName, @Email)";
        //        newId = Application.SqlHelper.ExecuteScalar<int>(sql, firstNameParam, lastNameParam, emailParam);
        //    }
        //    else
        //    {
        //        const string sql = "Update hsenseIqEmailRecepients set FirstName = @FirstName, LastName = @LastName, Email = @Email where Id = @Id";
        //        Application.SqlHelper.ExecuteNonQuery(sql, idParam, firstNameParam, lastNameParam, emailParam);
        //        newId = Id;
        //    }
        //    return newId;
        //}

        //public void Delete()
        //{
        //    const string sql = "Delete from hsenseIqEmailRecepients where Id = @Id";
        //    var idParam = Application.SqlHelper.CreateParameter("@Id", Id);

        //    Application.SqlHelper.ExecuteNonQuery(sql, idParam);
        //}

        //public static List<EmailRecepient> GetRecepients()
        //{
        //    var result = new List<EmailRecepient>();
        //    var reader = Application.SqlHelper.ExecuteReader("Select * from hsenseIqEmailRecepients");
        //    while (reader.Read())
        //    {
        //        var recepient = Read(reader);
        //        result.Add(recepient);
        //    }

        //    return result;
        //}

        //public static EmailRecepient GetRecepient(int id)
        //{
        //    const string sql = "Select Id, Name, Value from hsenseIqEmailRecepients where Id = @Id";
        //    var idParam = Application.SqlHelper.CreateParameter("@Id", id);
        //    var reader = Application.SqlHelper.ExecuteReader(sql, idParam);
        //    reader.Read();
        //    return Read(reader);
        //}

        //private static EmailRecepient Read(IRecordsReader reader)
        //{
        //    return new EmailRecepient
        //    {
        //        Id = reader.Get<int>("Id"),
        //        Email = reader.Get<string>("Email"),
        //        FirstName = reader.Get<string>("FirstName"),
        //        LastName = reader.Get<string>("LastName")
        //    };
        //}
    }
}